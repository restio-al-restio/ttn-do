#!/bin/sh
if [ x$1 = x-f ] ; then
    rm -rf config.cache autom4te.cache
    set -- -r
fi
if [ x$1 = x-r ] ; then
    ./config.status --recheck && ./config.status
else
    ./config.status "$@"
fi
