#!/bin/sh
exec ${GUILE-guile} -e '(ttn-do bump-version)' -s $0 "$@" # -*-scheme-*-
!#
;;; bump-version

;; Copyright (C) 2008, 2009, 2011, 2012 Thien-Thi Nguyen
;;
;; This file is part of ttn-do, released under the terms of the
;; GNU General Public License as published by the Free Software
;; Foundation; either version 3, or (at your option) any later
;; version.  There is NO WARRANTY.  See file COPYING for details.

;;; Commentary:

;; Usage: bump-version [--cool] [--more MORE] [EXPLICIT | --auto]
;;
;; normal operation
;; ----------------
;; In configure.ac (or configure.in), set second ‘AC_INIT’ arg to
;; VERSION, computed as EXPLICIT if specified, otherwise the value
;; "1+ the current one" (increment the last numerical component)
;; if given option ‘--auto’ (short form: ‘-a’).
;;
;; Normally it is an error if there is no configure.{ac,in} file.
;; Option ‘--cool’ (short form ‘-c’) relaxes this.
;;
;; Next, use "git config" ‘user.name’ and ‘user.email’ values and
;; the first byte of .last-release to update top-level ChangeLog
;; w/ the (properly-formatted) entry:
;;
;;  YYYY-MM-DD  USER-NAME  <USER-EMAIL>
;;  Release: VERSION (tag TAG)
;;  * configure.ac (AC_INIT): Bump version to "VERSION" for release.
;;
;; If TAG is identical to VERSION (no "v" prefix), omit " (tag TAG)".
;; Next, do "git commit -a" w/ the commit message comprising the
;; (properly-formatted) last two lines of the above ChangeLog entry.
;; NB: This may pull in other changes.
;;
;; If other changes are to be documented, MORE names a file whose
;; contents are appended (properly-formatted) to the ChangeLog and
;; commit message entries.  Short form for ‘--more’ is ‘-m’.
;;
;; Files named "ChangeLog" in subdirectories are similarly modified,
;; except that the "* configure.ac" and subsequent lines are omitted,
;; and only if the latest entry is NOT a "Release:" entry.
;;
;; Lastly, do "git tag TAG" w/ annotation "YYYY-MM-DD HH:MM:SS"
;; followed by the second and subsequent lines of .last-release;
;; and update the first line of .last-release to be TAG.
;;
;;
;; operation when using git-version-gen
;; ------------------------------------
;; If configure.ac (or .in) uses git-version-gen (from gnulib),
;; the operation is like the normal operation, described above,
;; except that configure.ac (or .in) is not modified, and the
;; (change-)log entries subsequently do not include that notice.

;;; Code:

(define-module (ttn-do bump-version)
  #:export (main)
  #:use-module ((ttn-do zzz banalities) #:select (check-hv
                                                  qop<-args))
  #:use-module ((ttn-do zzz personally) #:select (fs fso fse FE))
  #:use-module ((ttn-do zzz filesystem) #:select (dir-exists?))
  #:use-module ((ttn-do zzz subprocess) #:select (shell-command->string
                                                  fshell-command->string
                                                  shell-command->list
                                                  call-process
                                                  sysfmt))
  #:use-module ((ttn-do mogrify) #:select (filename:
                                           editing-buffer
                                           find-file
                                           find-file-read-only))
  #:use-module ((srfi srfi-13) #:select (string-trim-both
                                         string-take
                                         substring/shared
                                         string-skip-right))
  #:use-module ((srfi srfi-14) #:select (char-set:digit)))

(define (dne s . args)
  (fse "ERROR: Could not find ~A~%" (apply fs s args))
  (exit #f))

(define (check-sanity-find-c cool?)
  (define (buf-if-file-exists filename)
    (and (file-exists? filename)
         (find-file filename)))
  (or (dir-exists? ".git")
      (dne "subdir .git"))
  (or (file-exists? ".last-release")
      (dne "file .last-release"))
  ;; rv
  (or (buf-if-file-exists "configure.ac")
      (buf-if-file-exists "configure.in")
      (and (not cool?)
           (dne "file configure.ac (or .in)"))))

(define (gvg? c)                        ; using git-version-gen(1)?
  (editing-buffer c
    (goto-char (point-min))
    (search-forward "git-version-gen" #f #t)))

(define (line<- command)
  (string-trim-both (shell-command->string command)))

(define lr-info
  (let ((v? #f)
        (version #f))
    ;; lr-info
    (lambda (sel)
      (or version (editing-buffer (find-file-read-only ".last-release")
                    (set! v? (char=? #\v (char-after)))
                    (end-of-line)
                    (set! version (buffer-substring
                                   (+ (if v? 1 0) (point-min))
                                   (point)))))
      (case sel
        ((v?) v?)
        ((version) version)))))

(define (filename/nodir: c)
  (basename (filename: c)))

(define (oldv gvg? c)
  (let ((s (if gvg?
               (lr-info 'version)
               (line<- "autoconf -t 'AC_INIT:$2'"))))
    (and (string-null? s) (dne "well-formed AC_INIT (in ~A)"
                               (filename/nodir: c)))
    (fso "oldv: ~A~%" s)
    s))

(define (newv<-oldv oldv)
  (let* ((last-n (cond ((string-skip-right oldv char-set:digit) => 1+)
                       (else 0)))
         (s (fs "~A~A"
                (string-take oldv last-n)
                (1+ (string->number (substring/shared oldv last-n))))))
    (fso "newv: ~A~%" s)
    s))

(define (tag newv)
  (let ((s (if (lr-info 'v?)
               (fs "v~A" newv)
               newv)))
    (fso "tag: ~A~%" s)
    (or (string-null? (fshell-command->string "git tag -l ~A" s))
        (begin (fso "WARNING: Reseating tag ~A~%" s)
               (sysfmt "git tag -d ~A" s)))
    s))

(define (zero)
  (fs "~A  ~A  <~A>"
      (strftime "%F" (gmtime (current-time)))
      (line<- "git config user.name")
      (line<- "git config user.email")))

(define (one newv tag)
  (fs "Release: ~A~A" newv (if (string=? tag newv)
                               ""
                               (fs " (tag ~A)" tag))))

(define (two c newv)
  (fs "* ~A (AC_INIT): Bump version to ~S for release.~%"
      (filename/nodir: c) newv))

(define (more filename)
  (or (access? filename R_OK)
      (dne "readable file ~A" filename))
  (editing-buffer (find-file filename)
    (goto-char (point-min))
    (while (< (point) (point-max))
      (insert "\t")
      (forward-line 1))
    (buffer-string)))

(define (ensure-writable! buf)
  (chmod (filename: buf) #o644))

;; TODO: Make ‘editing-buffer’ composable w/ this.
;;
;; (define-macro (save-when-done . body)
;;   `(begin
;;      (toggle-read-only -1)
;;      (goto-char (point-min))
;;      ,@body
;;      (ensure-writable! (current-buffer))
;;      (save-buffer)))

(define (update-c! buf oldv newv)
  (editing-buffer buf
    (toggle-read-only -1)
    (goto-char (point-min))
    (re-search-forward "^AC_INIT")
    (search-forward oldv)
    (delete-char (- (match-beginning 0) (match-end 0)))
    (insert newv)
    (ensure-writable! (current-buffer))
    (save-buffer)))

(define (munge-ChangeLog-files! zero one two more)
  (FE (shell-command->list "find . -depth -name ChangeLog")
      (lambda (filename)
        (editing-buffer (find-file filename)
          ;; If latest entry is a release, skip this one.
          ;; However, never skip top-level ChangeLog.
          (or (and (not (string=? "./ChangeLog" filename))
                   (begin (goto-char (point-min))
                          (forward-line 2)
                          (looking-at "\tRelease: ")))
              (begin
                (toggle-read-only -1)
                (goto-char (point-min))
                (insert zero "\n\n\t" one "\n")
                (and (string=? "." (dirname filename))
                     (let ((add (string-append
                                 (if two (fs "\t~A" two) "")
                                 more)))
                       (or (string-null? add)
                           (insert "\n" add))))
                (insert "\n")
                (ensure-writable! (current-buffer))
                (save-buffer)))))))

(define (call-git command . args)
  (call-process "git" #:args (cons (symbol->string command) args)))

(define (commit! one two more)
  (editing-buffer one
    (insert "\n\n")
    (and two (insert two))
    (let ((p (point)))
      (insert more)
      (goto-char p)
      (while (< (point) (point-max))
        (delete-char 1)
        (forward-line 1)))
    (call-git 'commit "-a" "-m" (buffer-string))))

(define (tag! tag)
  (editing-buffer (find-file ".last-release")
    (end-of-line)
    (delete-region (point-min) (point))
    (insert (strftime "%F %T" (gmtime (current-time))))
    (call-git 'tag "-m" (buffer-string) tag)
    (delete-region (point-min) (point))
    (insert tag)
    (save-buffer)))

(define (main/qop qop)
  (let* ((auto? (qop 'auto))
         (rest (qop '()))
         (c (check-sanity-find-c (qop 'cool)))
         (gvg? (or (not c) (gvg? c)))
         (oldv (oldv gvg? c))
         (newv (if auto?
                   (newv<-oldv oldv)
                   (cond ((pair? rest) (car rest))
                         (else (fse "ERROR: Missing argument~%")
                               (exit #f)))))
         (tag (tag newv))
         (zero (zero))
         (one (one newv tag))
         (two (and (not gvg?) (two c newv)))
         (more (or (qop 'more more) "")))
    (or gvg? (update-c! c oldv newv))
    (munge-ChangeLog-files! zero one two more)
    (commit! one two more)
    (tag! tag))
  #t)

(define (main args)
  (check-hv args '((package . "ttn-do")
                   (version . "2.3")
                   ;; 2.3 -- new option: --cool
                   ;; 2.2 -- inhibit "Release:" skipping for top-level
                   ;; 2.1 -- bugfix
                   ;; 2.0 -- similar to 1.6, but w/ ‘-m MORE’
                   ;;        (change in command-line syntax)
                   (help . commentary)))
  (main/qop (qop<-args args '((more (single-char #\m) (value #t))
                              (cool (single-char #\c))
                              (auto (single-char #\a))))))

;;; bump-version ends here
