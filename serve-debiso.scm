#!/bin/sh
exec ${GUILE-guile} -e '(ttn-do serve-debiso)' -s $0 "$@" # -*- scheme -*-
!#
;;; serve-debiso --- serve Debian ISO images on port 42424

;; Copyright (C) 2007, 2009, 2010, 2011, 2012, 2021 Thien-Thi Nguyen
;;
;; This file is part of ttn-do, released under the terms of the
;; GNU General Public License as published by the Free Software
;; Foundation; either version 3, or (at your option) any later
;; version.  There is NO WARRANTY.  See file COPYING for details.

;;; Commentary:

;; Usage: serve-debiso
;;
;; Mount *.iso files under /mnt/debiso
;; and then serve them via http on port 42424.
;;
;; This relies on proper autofs/automount setup.
;; (If things aren't working, see /etc/auto.master et al.)

;;; Code:

(define-module (ttn-do serve-debiso)
  #:export (main)
  #:use-module ((ttn-do zzz banalities) #:select (check-hv
                                                  decry))
  #:use-module ((ttn-do zzz personally) #:select (FE fso))
  #:use-module ((ttn-do zzz filesystem) #:select (filtered-files
                                                  not-dot-not-dotdot
                                                  dir-exists?
                                                  mkdir-p))
  #:use-module ((ttn-do zzz subprocess) #:select (call-process))
  #:use-module ((srfi srfi-13) #:select (string-concatenate
                                         string-join)))

(define MROOT "/mnt/debiso")

(define (available)
  (filtered-files not-dot-not-dotdot (in-vicinity MROOT "available")))

(define UDIR "/tmp/mnt/.serve-debiso.d")

(define FORMS
  '((define-module (ttn-do sizzweb)
      #:use-module ((www utcsec) #:prefix UTCS:
                    ;; Sigh, Guile 1.8 can't handle this list being
                    ;; different from the one in sizzweb.scm.
                    ;;- #:select (<-rfc1123-date
                    ;;-           <-mtime)
                    ;;
                    ;; TODO: Have (ttn-do sizzweb) export a stable
                    ;; "plugin API".
                    )
      #:use-module ((www url-coding) #:select ((url-coding:decode . urldecode))))

    ;; This block is (mis?)named ‘Φ’ in the ChangeLog file.
    ;; See: <https://en.wikipedia.org/wiki/Static_single_assignment_form>.
    (add-dynamic-handler!
     "^/[0-9][0-9]r[0-9]"               ; e.g.: "/31r2/01/..."
     (lambda (M in-port upath headers)
       (define (reply-with-status n)
         (add-standard-headers M n (http-status-string n))
         (list 'close))
       (define (transmit-file M filename)
         (prep-file-transfer M filename #f))
       (define (hack s)
         ;; debian uses "%7e" for '~', but leaves '+' alone (sigh).
         (string-concatenate
          (map (lambda (c)
                 (if (memq c '(#\+))
                     (fs "%~A" (number->string (char->integer c) 16))
                     (make-string 1 c)))
               (string->list s))))
       (let* ((filename (fs-name (urldecode (hack upath))))
              (since (and=> (assq-ref headers 'If-Modified-Since)
                            UTCS:<-rfc1123-date)))
         (cond ((not filename)
                (reply-with-status 404))
               ((not since)
                (transmit-file M filename))
               ((< since (UTCS:<-mtime filename))
                (transmit-file M filename))
               (else
                (reply-with-status 304))))))))

(define (main args)
  (check-hv args '((package . "ttn-do")
                   (version . "2.3")
                   (help . commentary)))

  (or (null? (cdr args))
      (decry "spurious arguments, try --help"))

  (fso "available: ~A~%" (string-join (sort (available) string<?) ", "))
  (cond ((dir-exists? UDIR))
        (else
         (mkdir-p UDIR)
         (with-output-to-file (in-vicinity UDIR "servlets.scm")
           (lambda ()
             (FE FORMS (lambda (form)
                         (fso "~S~%" form)))))))

  (call-process "ttn-do" #:args `("sizzweb"
                                  "-r" ,MROOT
                                  "-p" "42424"
                                  "-u" ,UDIR)))

;;; serve-debiso ends here
