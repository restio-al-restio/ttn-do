#!/bin/sh
exec ${GUILE-guile} -e '(ttn-do browse-grumi)' -s $0 "$@" # -*- scheme -*-
!#
;;; browse-grumi --- show what each package supports

;; Copyright (C) 2006, 2007, 2011 Thien-Thi Nguyen
;;
;; This file is part of ttn-do, released under the terms of the
;; GNU General Public License as published by the Free Software
;; Foundation; either version 3, or (at your option) any later
;; version.  There is NO WARRANTY.  See file COPYING for details.

;;; Commentary:

;; Usage: browse-grumi [-nw]
;;
;; Kick off "ttn-do grumi" on localhost:8001, and start w3m on it.
;; After w3m exits, kill the grumi and clean up.  If DISPLAY is set,
;; do everything in a new rxvt session.  Otherwise, or if given
;; optional arg "-nw", use the current session.
;;
;; Normally, the grumi's pidfile is written in browse-grumi.d/
;; (under cwd).  Env var TMPDIR, if set, overrides cwd.

;;; Code:

(define-module (ttn-do browse-grumi)
  #:export (main)
  #:use-module ((ttn-do zzz banalities) #:select (check-hv))
  #:use-module ((ttn-do zzz personally) #:select (fs fso))
  #:use-module ((ttn-do zzz filesystem) #:select (directory-vicinity
                                                  dir-exists?))
  #:use-module ((ttn-do zzz subprocess) #:select (call-process))
  #:use-module ((ttn-do zzz 0gx forms-from) #:select (forms<-file))
  #:use-module ((ttn-do rm-rf) #:select (rm-rf!)))

(define (hey s . args)
  (display "browse-grumi: ")
  (apply fso s args)
  (newline))

(define (kick command)
  (hey "doing: ~A" command)
  (call-process command #:norm #t))

(define (main args)
  (check-hv args '((package . "ttn-do")
                   ;; 1.0 -- initial release
                   ;; 1.1 -- update temporary-file handling
                   ;; 1.2 -- use [TMPDIR/]PIDDIR/pid as pidfile
                   (version . "1.2")
                   (help . commentary)))
  (let* ((nw? (member "-nw" args))
         (piddir ((cond ((getenv "TMPDIR") => directory-vicinity)
                        (else identity))
                  "browse-grumi.d"))
         (piddir-exists? (dir-exists? piddir))
         (pidfile (in-vicinity piddir "pid"))
         (grumi (fs "ttn-do grumi -p 8001 --daemon ~A" pidfile))
         (browse "w3m http://localhost:8001"))

    (define (leave-piddir)
      (hey "not deleting ~A/" piddir))

    (define (zonk-piddir)
      (rm-rf! #f piddir)
      (hey "~A ~A/"
           (if (dir-exists? piddir)
               "tried (but failed) to delete"
               "deleted")
           piddir))

    (cond (piddir-exists?)
          (else (hey "creating ~A/" piddir)
                (mkdir piddir)))
    (kick grumi)
    (hey "exit value: ~A" (kick (if (or nw? (not (getenv "DISPLAY")))
                                    browse
                                    (fs "rxvt -e ~A" browse))))
    (kill (car (forms<-file pidfile)) SIGKILL)
    ((if piddir-exists?
         leave-piddir
         zonk-piddir)))
  #t)

;;; browse-grumi ends here
