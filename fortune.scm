#!/bin/sh
exec ${GUILE-guile} -e "(ttn-do fortune)" -s $0 "$@" # -*-scheme-*-
!#
;; Copyright (C) 2004, 2005, 2007, 2009-2013, 2017, 2019-2021 Thien-Thi Nguyen
;;
;; This file is part of ttn-do, released under the terms of the
;; GNU General Public License as published by the Free Software
;; Foundation; either version 3, or (at your option) any later
;; version.  There is NO WARRANTY.  See file COPYING for details.

;;; Commentary:

;; Usage: fortune -l    (like "ls -l -I '*.dat' <standard-cookie-dir>")
;;        fortune
;;        fortune literature   [or other filename in standard cookie dir]
;;        fortune DIR/FILENAME   [.dat assumed in same dir]
;;
;; Display fortune cookie to stdout.  Option ‘--list’ (or ‘-l’ for short)
;; means instead to list the contents of the <standard-cookie-dir>, which
;; is either "$HOME/local/share/games/fortunes" or the contents of file
;; $HOME/.fortune-cookie-dir if the first directory is not found.  If the
;; cookie dir cannot be determined, signal error.
;;
;; Usage: fortune --create-index [options] [FILENAME...]
;;
;; Invoked with ‘--create-index’ (or ‘-c’ for short), scan each FILENAME,
;; which should be in fortune cookie format, and write FILENAME.dat, its
;; index.
;;
;; The fortune cookie format is text containing groups of lines separated
;; by "delim lines", a specially chosen character (traditionally the percent
;; ‘%’ character) on a line by itself.  Use ‘--delim’ (or ‘-d’ for short)
;; to specify another character.  The first and last lines may also be
;; delim lines.  Each group of lines thus delimited is a "cookie".
;; Thus, a file with N cookies may have N-1, N or N+1 delim lines.
;;
;; Use ‘--flag FLAG’ (or ‘-f’ for short) to set a bit corresponding
;; to FLAG in the index.  This option may be specified multiple times,
;; in which case the value is a list, accumulating left to right.
;; Valid values for FLAG are:
;;
;;  random   -- This does not do anything at the moment.
;;  ordered  -- Sort the cookies lexically, ignoring non-alphanumeric
;;              leading characters.
;;  rotated  -- Note that the cookies are ROT13.

;;; Code:

(define-module (ttn-do fortune)
  #:export (main
            create-index-file!
            fortune-cookie)
  #:use-module ((ice-9 rdelim) #:select (read-line write-line))
  #:use-module ((srfi srfi-11) #:select (let-values))
  #:use-module ((srfi srfi-13) #:select (substring/shared
                                         string-concatenate-reverse
                                         string-contains
                                         string-trim
                                         string-map!
                                         string-take))
  #:use-module ((srfi srfi-14) #:select (char-set-complement
                                         char-set:letter+digit))
  #:use-module ((ttn-do zzz 0gx string-io) #:select (string-in-proc))
  #:use-module ((ttn-do zzz 0gx uve) #:select (make-uve
                                               uve-ref
                                               uve-set!
                                               read-network-uve-proc))
  #:use-module ((ttn-do zzz banalities) #:select (check-hv
                                                  decry
                                                  qop<-args))
  #:use-module ((ttn-do zzz filesystem) #:select (directory-vicinity
                                                  dir-exists?))
  #:use-module ((ttn-do zzz personally) #:select (FE)))

;; A fortune cookie index file is a binary file consisting of a six-word
;; header followed by the offset table (each entry a word) for each cookie,
;; followed by a word to hold the size of the cookie file.  A word is four
;; bytes in network (big-endian) order.  The header is:
;;
;;   version     -- typically 1 for old files and 2 for newer ones
;;   count       -- number of cookies
;;   longest     -- number of bytes of longest cookie
;;   shortest    -- number of bytes of shortest cookie
;;   flags       -- logior of #x1 (random)
;;                            #x2 (ordered)
;;                            #x4 (rotated)
;;   delim-char  -- this is shifted to the MSB position
;;                  (the remaining bytes are #\nul)
;;
;; Traditionally, for cookie file foo, the index file is named foo.dat, but
;; that is not required.

(define (k name)
  (case name
    ((#:format-version)  2)             ; hmmm
    ((#:sizeof-word)     4)
    ((#:all-flags)      '(#:random #:ordered #:rotated))
    ((#:random)        #x1)
    ((#:ordered)       #x2)
    ((#:rotated)       #x4)
    ((#:bits-per-byte)   8)             ; you never know...
    ((#:delim+newline)   2)))

(define (read-word-proc port)
  (let ((r! (read-network-uve-proc 'u32)))
    ;; retval
    (lambda ()
      (r! port))))

(define (write-word-proc port)
  (let ((uve (make-uve 'u32 1 1)))
    (lambda (n)
      (uve-set! uve 0 (htonl n))
      (uniform-vector-write uve port))))

(define trimmed-to-first-alphanumeric
  (let ((non-alnum (char-set-complement char-set:letter+digit)))
    ;; trimmed-to-first-alphanumeric
    (lambda (full)
      (string-trim full non-alnum))))

(define (words<-cookie-file filename delim flags)

  (let* ((p (open-input-file filename))
         (count -1) (shortest #f) (longest #f)
         (box (list #f))
         (tp box)
         (order? (memq #:ordered flags)))

    (define (hang! x)
      (set-cdr! tp (list x))
      (set! tp (cdr tp)))

    (define (another! whence acc)

      (define prev (car tp))
      (define ofs (seek p 0 whence))

      (let ((len (- ofs
                    (cond ((not prev) 0)
                          (order? (car prev))
                          (else prev))
                    (k #:delim+newline))))
        (cond ((not shortest)
               (set! shortest len)
               (set! longest len))
              (else
               (set! shortest (min shortest len))
               (set! longest (max longest len)))))
      (hang! (if order?
                 (begin
                   (and prev
                        (set-cdr! prev (trimmed-to-first-alphanumeric
                                        (string-concatenate-reverse
                                         acc))))
                   (cons ofs ""))
                 ofs))
      (set! count (1+ count)))

    (define (refine ls)
      (if (null? ls)
          ;; Explicitly add a "word to hold the size of the cookie file".
          (list 0)
          ;; The "size of the cookie file" is (in) the last element of ‘ls’.
          (if order?
              (map car
                   (append
                    ;; After sorting, the first element "is" ‘(car tp)’
                    ;; because ‘tp’ has the value ‘((EOF-OFFSET . ""))’
                    ;; and the empty string is ‘string<?’ everything.
                    ;; So, discard that (via ‘cdr’) from here, but...
                    (cdr (sort ls (lambda (a b)
                                    (string<? (cdr a)
                                              (cdr b)))))
                    ;; ...make sure it ends up here, for EOF-OFFSET.
                    tp))
              ls)))

    ;; If ‘order?’, accumulate (OFFSET . "") and hang on the previous
    ;; entry (i.e., in its ‘cdr’) COOKIE-TRIMMED-TO-FIRST-ALPHANUMERIC,
    ;; then sort ascending.  Otherwise, accumulate OFFSET only.

    (let loop ((acc '()))

      (define line (read-line p 'concat))

      (cond ((eof-object? line)
             (or (null? acc)
                 (another! SEEK_END acc))
             (close-port p)
             ;; kludge
             (cond ((= 1 count)
                    (set! longest (+ (k #:delim+newline) longest))
                    (set! shortest (+ (k #:delim+newline) shortest))))
             (cons*                     ; rv
              ;; header
              (k #:format-version)
              (max 0 count)
              (or longest 0)
              (or shortest 0)
              (apply logior (map (lambda (flag)
                                   (if (memq flag flags)
                                       (k flag)
                                       0))
                                 (k #:all-flags)))
              (ash (char->integer delim)
                   (* (1- (k #:sizeof-word))
                      (k #:bits-per-byte)))
              ;; offset table
              (refine (cdr box))))

            ((and (= (k #:delim+newline) (string-length line))
                  (char=? delim (string-ref line 0)))
             (another! SEEK_CUR acc)
             (loop '()))

            ((negative? count)
             (hang! (if order? (cons 0 "") 0))
             (set! count (1+ count))
             (loop (cons line acc)))

            (else
             (loop (cons line acc)))))))

;; Create index file @var{out-name} from cookie file @var{in-name}, separating
;; cookies by looking for char @var{delim} on a line by itself.  Optional
;; @var{flags} are keywords:
;;
;; @table @code
;; @item #:random
;; Set bit 0 (corresponding to a mask of #x1) in the flags word in the header,
;; but do nothing else at the moment (FIXME).
;;
;; @item #:ordered
;; Set bit 1 (corresponding to a mask of #x2) in the flags word in the header,
;; and order the offsets by sorting the cookies with @code{string<?}, ignoring
;; non-alphanumeric leading characters.
;;
;; @item #:rotated
;; Set bit 2 (corresponding to a mask of #x4) in the flags word in the header,
;; to note that the cookies are @dfn{ROT13}.
;; @end table
;;
;; Return #t on success.
;;
(define (create-index-file! out-name in-name delim . flags)
  (FE flags (let ((ok (k #:all-flags)))
              (lambda (flag)
                (or (memq flag ok)
                    (error "Bad flag:" flag)))))
  (call-with-output-file out-name
    (lambda (port)
      (FE (words<-cookie-file in-name delim flags)
          (write-word-proc port)))))

(define (get-cookie cookie-file dat-file)

  (define (grok-header)
    (let* ((port (open-input-file dat-file))
           (readw (read-word-proc port))
           (info '()))

      (define (shrug v)
        #f)

      (define (push! v)
        (set! info (cons v info)))

      ;; Do it this way to avoid potential evaluation-order weirdness.
      (shrug (readw))                   ; [0] version: discard
      (push! (readw))                   ; [1] count
      (push! (readw))                   ; [2] longest
      (shrug (readw))                   ; [3] shortest: discard
      (push! (let ((w (readw)))         ; [4] flags
               (let loop ((ls (k #:all-flags))
                          (acc '()))
                 (if (null? ls)
                     acc
                     (let ((flag (car ls)))
                       (loop (cdr ls)
                             (if (zero? (logand (k flag) w))
                                 acc
                                 (cons flag acc))))))))
      (push! (integer->char
              (bit-extract (readw)      ; [5] delim
                           24 32)))

      (apply values                     ; rv
             port readw
             ;; (delim flags longest count)
             info)))

  (define (raw-extraction)
    (let-values (((port readw delim flags longest count)
                  (grok-header)))

      (define readw-from
        (let ((idx-eof-pos (stat:size (stat port)))
              (wsize (k #:sizeof-word)))
          ;; readw-from
          (lambda (whence word-offset)
            (and (> idx-eof-pos
                    (seek port (* wsize word-offset)
                          whence))
                 (readw)))))

      (let* ((degenerate (zero? count))
             (selection (if degenerate
                            count
                            (random count)))
             (start (readw-from SEEK_CUR selection))
             (need-scan (or (memq #:ordered flags)
                            degenerate
                            (= count (1+ selection))))
             (len (if need-scan
                      (min longest
                           (- (readw-from SEEK_END -1)
                              start))
                      (- (readw)        ; this is at [1 + selection]
                         start
                         (k #:delim+newline)))))
        (close-port port)

        (values                         ; rv
         (call-with-input-file cookie-file
           (lambda (p)
             (seek p start SEEK_SET)
             ((string-in-proc (make-string len))
              p)))
         (and need-scan (string delim #\newline))
         (memq #:rotated flags)))))

  (let-values (((cookie delim-line rotated)
                (raw-extraction)))

    (cond ((not delim-line))
          ((string-contains cookie delim-line)
           => (lambda (pos)
                (set! cookie (string-take cookie pos)))))

    (and rotated
         (let ((a-n (char->integer #\a))
               (A-n (char->integer #\A)))

           (define (rot base c)
             (integer->char (+ base
                               (modulo (+ 13 (- (char->integer c)
                                                base))
                                       26))))

           (define (c->c c)
             (cond ((char<=? #\a c #\z) (rot a-n c))
                   ((char<=? #\A c #\Z) (rot A-n c))
                   (else                c)))

           (string-map! c->c cookie)))

    cookie))

;; Return a randomly-chosen string extracted from @var{cookie-file},
;; using the index file named by appending @file{.dat} to @var{cookie-file}.
;; Optional arg @var{dat-file} specifies the index file to use instead of the
;; default.
;;
;;-args: (- 1 0)
;;
(define (fortune-cookie cookie-file . dat-file)
  (get-cookie cookie-file (if (null? dat-file)
                              (string-append cookie-file ".dat")
                              (car dat-file))))

(define (dir-ok? name)
  (and (dir-exists? name) name))

(define (fortune-select/qop qop)
  (let* ((under-home (directory-vicinity (getenv "HOME")))
         (cdir (or (dir-ok? (under-home "local/share/games/fortunes"))
                   (let ((clue (under-home ".fortune-cookie-dir")))
                     (and (file-exists? clue)
                          (dir-ok? (call-with-input-file clue read-line))))
                   (decry "could not determine fortune cookie dir"))))
    (set! *random-state* (seed->random-state
                          (let ((pair (gettimeofday)))
                            (* (car pair) (cdr pair)))))
    (if (qop 'list)
        (let ((cmd (string-append "ls -l -I '*.dat' " cdir)))
          (write-line cmd)
          (system cmd))
        (display (fortune-cookie
                  (let* ((args (qop '()))
                         (file (if (null? args)
                                   "fortunes"
                                   (car args))))
                    (if (string=? file (basename file))
                        (in-vicinity cdir file)
                        file)))))))

(define (fortune-create/qop qop)
  (FE (qop '())
      (lambda (filename)
        (apply create-index-file!
               (string-append filename ".dat")
               filename
               (or (qop 'delim (lambda (s)
                                 (string-ref s 0)))
                   #\%)
               (or (qop 'flag (lambda (ls)
                                (map (lambda (s)
                                       (symbol->keyword
                                        (string->symbol
                                         s)))
                                     ls)))
                   '())))))

(define (fortune/qop qop)
  ((if (qop 'create-index)
       fortune-create/qop
       fortune-select/qop)
   qop))

(define (main args)
  (check-hv args '((package . "ttn-do")
                   (version . "2.1")
                   ;; 2.1  -- Guile 2 slog
                   ;; 2.0  -- bugfixes for ‘create-index-file!’ and ilk
                   ;;         new option: --create-index
                   ;;         new option: --delim CHAR
                   ;;         new option: --flag FLAG
                   ;;         file selection relaxed from abs to "w/ DIR"
                   ;; 1.1  -- avoid potential order-of-evaluation gotcha
                   ;; 1.0  -- original release
                   (help . commentary)))
  (fortune/qop
   (qop<-args
    args '((create-index (single-char #\c))
           (delim (single-char #\d) (value #t))
           (flag (single-char #\f) (value #t) (merge-multiple? #t))
           (list (single-char #\l))))))

;;; fortune ends here
