#!/bin/sh
exec ${GUILE-guile} -e '(ttn-do xearth-mapdata-to-text-db-table)' -s $0 "$@" # -*-scheme-*-
!#
;;; xearth-mapdata-to-text-db-table --- mine xearth-1.1/mapdata.c

;; Copyright (C) 2005, 2007, 2009, 2010, 2011 Thien-Thi Nguyen
;;
;; This file is part of ttn-do, released under the terms of the
;; GNU General Public License as published by the Free Software
;; Foundation; either version 3, or (at your option) any later
;; version.  There is NO WARRANTY.  See file COPYING for details.

;;; Commentary:

;; Usage: xearth-mapdata-to-text-db-table [--no-body] MAPDATA
;;
;; Write to stdout a series of sexps suitable for reading using
;; proc ‘read-text-db-table’, representing the data mined from
;; MAPDATA, the mapdata.c file (from xearth-1.1 or similar).
;;
;; Option ‘--no-body’ (‘-n’ for short) means to stop output after
;; displaying the header (text-db-table configuration) sexp.  For
;; xearth-mapdata-to-text-db-table 1.0, this results in the output:
;;
;;  (text-db-table-config
;;    (delim . "\n")
;;    (fields (type sexp) (init sexp) (path sexp)))
;;
;; The ‘type’ value is a keyword, either #:land or #:water.
;;
;; The ‘init’ value is a vector #(x y z) that indicates a point on the
;; unit sphere (each of x, y, and z has been scaled by 30000), where the
;; x axis points "to the right" (towards 0 N 90 E), the y axis points
;; "up" (towards the north pole), and the z axis points "out of the
;; screen" (towards 0 N 0 E).  This is the starting point of the curve.
;;
;; The ‘path’ value is a vector of triples #(dx dy dz); the #(x y z)
;; triple for each successive point in the curve is obtained by adding
;; #(dx dy dz) onto the previous #(x y z) values.

;;; Code:

(define-module (ttn-do xearth-mapdata-to-text-db-table)
  #:export (main)
  #:use-module ((ttn-do zzz banalities) #:select (check-hv
                                                  qop<-args))
  #:use-module ((ttn-do mogrify) #:select (find-file-read-only
                                           editing-buffer))
  #:use-module ((ttn-do pp) #:select (pp)))

(define (>>head)
  (pp '(text-db-table-config
        (delim . "\n")
        (fields (type sexp)
                (init sexp)
                (path sexp)))))

(define (>>body filename)
  (editing-buffer (find-file-read-only filename)
    (re-search-forward "map_data.*=.*\\{")
    (let* ((integer-start (make-regexp "[-0-9]"))
           (comment (make-regexp "/[*][^*][^*]*[*]/")))

      (define (rnttc)                   ; read-number-through-trailing-comma
        (re-search-forward integer-start)
        (let ((p (1- (point))))
          (search-forward ",")
          (string->number (buffer-substring p (1- (point))))))

      (let loop ()
        (and (re-search-forward comment (point-max) #t)
             (let ((num (1- (rnttc))))
               (or (= -1 num)
                   (let ((vec (make-vector num))
                         (l/w (if (= 1 (rnttc))
                                  #:land
                                  #:water)))
                     (display l/w)
                     (display " ")
                     (display (vector (rnttc) (rnttc) (rnttc)))
                     (display " ")
                     (do ((i 0 (1+ i)))
                         ((= num i))
                       (vector-set! vec i (vector (rnttc) (rnttc) (rnttc))))
                     (display vec)
                     (newline)
                     (loop)))))))))

(define (main args)
  (check-hv args '((package . "ttn-do")
                   (version . "1.0")
                   (help . commentary)))
  (let ((qop (qop<-args args '((no-body (single-char #\n))))))
    (>>head)
    (or (qop 'no-body)
        (>>body (car (qop '()))))
    #t))

;;; xearth-mapdata-to-text-db-table ends here
