#! /bin/sh
exec ${GUILE-guile} -e "(ttn-do sizzweb)" -s $0 "$@" # -*-scheme-*-
!#
;;; sizzweb --- refinement of Martin Grabmueller's web server

;; Copyright (C) 2004-2013, 2017, 2019, 2020 Thien-Thi Nguyen
;; Copyright (C) 2000, 2001 Martin Grabmueller <mgrabmue@cs.tu-berlin.de>
;;
;; This file is part of ttn-do, released under the terms of the
;; GNU General Public License as published by the Free Software
;; Foundation; either version 3, or (at your option) any later
;; version.  There is NO WARRANTY.  See file COPYING for details.

;;; Description: Simple web server.

;;; Commentary:

;; Usage: sizzweb -r DIR [options...]
;;
;;   -p, --port=PORT     -- Listen on PORT [8001]; to specify a UNIX
;;                          domain socket, use "-p unix:FILENAME"
;;   -r, --docroot DIR   -- Specify DIR as filesystem root (required)
;;   -u, --ulibdir DIR   -- Look in DIR for servlets.scm [$HOME/.sizzweb.d]
;;   -d, --daemon FILE   -- Write pid and tcp port to FILE, go into background
;;   -l, --logfile FILE  -- Append logs to FILE [stdout if not --daemon]
;;       --funky         -- Don't use this unless you have read the source
;;   -b, --boredom N     -- Hang up after N inactive seconds (minimum 15)
;;
;; To stop sizzweb when invoked with -d FILE, use: kill -1 `head -1 FILE`
;; (note the backticks).  To reload servlets.scm, use kill -2.

;;; Code:

(define-module (ttn-do sizzweb)
  #:export (main)
  #:use-module ((ttn-do zzz banalities) #:select (check-hv
                                                  qop<-args))
  #:use-module (ice-9 optargs)
  #:use-module ((ice-9 regex) #:select (regexp-quote))
  #:use-module ((www utcsec) #:prefix UTCS: #:select (rfc1123-date<-
                                                      <-mtime
                                                      rfc1123-now))
  #:use-module ((www server-utils parse-request)
                #:select (request-method
                          request-upath
                          request-protocol-version
                          request-headers
                          request-body
                          receive-request))
  #:use-module ((www server-utils answer)
                #:select (compose-response))
  #:use-module ((www server-utils big-dishing-loop)
                #:select (named-socket))
  #:use-module ((www server-utils filesystem)
                #:select (access-forbidden?-proc
                          upath->filename-proc
                          default-text-charset
                          fully-specified
                          filename->content-type))
  #:use-module ((www server-utils log) #:select (string<-sockaddr
                                                 log-http-response-proc))
  #:use-module ((www data http-status) #:select (http-status-string))
  #:use-module ((www data mime-types) #:select (reset-mime-types!
                                                put-mime-types-from-file!))
  #:use-module ((ttn-do zz sys linux-gnu) #:select ((sendfile . ttn-sendfile)))
  #:use-module ((ttn-do zzz xhtml-tree) #:select (~simple-strict-xhtml
                                                  ~head ~title ~body
                                                  ~h2 ~hr
                                                  ~p ~a ~br ~b
                                                  ~table ~tr ~td))
  #:use-module ((ttn-do zzz personally) #:select (FE fs fso fse make-fso))
  #:use-module ((ttn-do zzz senz-altro) #:select (daemonize))
  #:use-module ((ttn-do zzz filesystem) #:select (directory-vicinity
                                                  filtered-files-in-vicinity
                                                  not-dot-not-dotdot
                                                  expand-file-name))
  #:autoload (ttn-do zzz publishing) (parent-directories)
  #:use-module ((srfi srfi-1) #:select (car+cdr))
  #:use-module ((srfi srfi-11) #:select (let-values))
  #:use-module ((srfi srfi-13) #:select (string-prefix?
                                         string-suffix?
                                         string-drop-right)))

(define *sizzweb-version*
  ;; 1.0 -- initial release
  ;; 1.1 -- ‘Date’ response header bugfix
  ;;        can reload servlets.scm w/ ‘kill -2’
  ;;        handles *.gz for Content-Encoding
  ;;        prettier internal filesystem directory display
  ;;        dynamic handlers updated in place
  ;;        new proc: remove-dynamic-handler!
  ;; 1.2 -- single-process operation unless given option --funky
  ;;        can override ‘send-generated-directory-index’
  ;;        can die of boredom
  ;; 1.3 -- finish cleanly on SIGQUIT and SIGTERM
  ;; 1.4 -- do "zero-copy TCP" for file transfers
  ;; 1.5 -- bugfix: handle SIGTERM when daemon
  ;;        log port accessible to servlets
  ;; 1.6 -- transmit-file includes Last-Modified header
  ;; 1.7 -- when --daemon, close stdin, stdout, stderr
  ;; 1.8 -- bugfix: shutdown socket after sending error message
  ;;        loop on ‘shutdown 2’ check
  ;;        log restart/shutdown
  ;; 1.9 -- output UTF-8 XHTML
  ;;        support UNIX-domain sockets
  ;;        handle HTTP HEAD request
  ;; 1.10 - use SO_LINGER
  ;;        do ‘shutdown 2’ without check/loop
  ;; 1.11 - ignore ENOTCONN on socket ‘shutdown’
  ;;        manage mime-types db directly
  ;;        use "/" in Server header http response
  ;; 1.12 - bugfix: unbotch ‘cond’ to ‘or’ conversion (ugh)
  ;; 1.13 - bugfix: also shutdown for "400 Bad Request"
  ;;        do ‘close-port’ after ‘shutdown’
  ;;        send HTTP/1.1 response for HTTP/1.1 request
  ;;        recognize .xz files
  ;;        handle ‘~’ in Unix-domain "port" command-line arg
  ;;        generate "XHTML Strict"
  ;; 1.14 - bugfix: DON'T ‘close-port’ after shutdown (1.13 regression)
  ;; 1.15 - add some exception handling; avoid ‘builtin-variable’
  ;; 1.16 - use precise Content-Type for generated pages
  ;;        include type info in generated directory index anchors
  ;;        include charset info for ‘text/*’ content/anchors
  ;;        add index.xhtml as dir upath to filename candidate
  ;;        recognize .lz files
  ;; 1.17 - don't try to load servlets if not available.
  ;;        Guile 2 slog
  ;; 1.18 - use ‘lstat’ for directory listing
  "1.18")

(define *system-mime-types* "/etc/mime.types")

(define fs-name #f)
(define load-servlets! #f)
(define *log-port* #f)


;; Support

(define (guilty sep)
  (string-append "SizzWeb" sep *sizzweb-version*))

(define (tlist<-filename lead filename)
  (fully-specified lead (filename->content-type filename "text/plain")))

(define (~td/right x)
  (~td 'align "right" x))

(define ~page/titled
  (let ((responsible (guilty " ")))
    (lambda (title . x)
      (~simple-strict-xhtml
       (~head (~title title))
       (~body x (~hr) (~p responsible))))))

(define (ge-1.1? protocol-version)
  (let-values (((major minor) (car+cdr protocol-version)))
    (not (or (zero? major)
             (and (= 1 major)
                  (zero? minor))))))

(define (q/r top bot)
  (values (quotient top bot)
          (remainder top bot)))

(define (Content-Type-XHTML M)
  (M #:add-header 'Content-Type "application/xhtml+xml"))

(define (end-response M chunk . good-bye)
  (and chunk (M #:rechunk-content chunk))
  good-bye)


;; Standard responses ================================================

(define add-standard-headers
  (let ((responsible (guilty "/")))
    (lambda (M code text)
      (M #:set-reply-status code text)
      (M #:add-header 'Date (UTCS:rfc1123-now))
      (M #:add-header 'Server responsible))))

(define aok!
  (let ((msg (http-status-string 200)))
    (lambda (M)
      (add-standard-headers M 200 msg))))

(define (prep-error M number . body)
  (let ((msg (http-status-string number))
        (nstr (number->string number)))
    (add-standard-headers M number msg)
    (Content-Type-XHTML M)
    (M #:add-content (let ((title (list nstr " " msg)))
                       (~page/titled title (~h2 title) (~p body))))
    (end-response
     M #t
     'close)))

(define (prep-not-found M upath)
  (prep-error M 404
              "The requested URL:" (~br)
              (~b upath) (~br)
              "was not found on this server."))

(define (prep-bad-request M)
  (prep-error M 400
              "Your browser sent a request that"
              " this server could not understand."))

(define (prep-unknown-method M method upath)
  (prep-error M 501
              (~b (symbol->string method))
              " to "
              (~b upath)
              " not supported."))

(define (prep-forbidden M upath)
  (prep-error M 403
              "You do not have permission to access:" (~br)
              (~b upath)))


;; Special responses =================================================

(define prep-generated-directory-index-box
  (let ((actual #f))
    (lambda args
      (if (null? args)
          actual
          (set! actual (car args))))))

;; Construct and send a directory index.
;;
(define (prep-generated-directory-index M upath dir)
  (define (upath-sub b e)
    (substring upath b e))
  (aok! M)
  (Content-Type-XHTML M)
  (let ((last-char-idx (1- (string-length upath))))
    (and (< 0 last-char-idx)
         (char=? #\/ (string-ref upath last-char-idx))
         (set! upath (upath-sub 0 last-char-idx))))
  (M #:add-content
     (~page/titled
      (list "Directory " upath)
      (~h2 "Parent Directories")
      (cond ((parent-directories upath)
             => (lambda (dirs)
                  (~table
                   (map (lambda (ref/name)
                          (~tr (~td "")
                               (~td (apply ~a 'href ref/name))))
                        dirs))))
            (else '()))
      (~hr)
      (~h2 "Directory " (basename upath))
      (let* ((? vector-ref)
             (subd "(subdir)")
             (under-dir (directory-vicinity dir))
             (raw (filtered-files-in-vicinity
                   dir (lambda (fn)
                         (and (not-dot-not-dotdot fn)
                              (let* ((si (lstat (under-dir fn)))
                                     (mt (strftime "%F %T"
                                                   (localtime (stat:mtime si)))))
                                (if (eq? 'directory (stat:type si))
                                    (vector
                                     subd
                                     ""
                                     (string-append fn "/")
                                     mt)
                                    (vector
                                     (tlist<-filename
                                      'type
                                      (if (or (string-suffix? ".xz" fn)
                                              (string-suffix? ".lz" fn)
                                              (string-suffix? ".gz" fn))
                                          (string-drop-right fn 3)
                                          fn))
                                     (number->string (stat:size si))
                                     fn mt)))))))
             (all (sort raw (lambda (a b)
                              (cond ((let ((a-dir? (eq? subd (? a 0)))
                                           (b-dir? (eq? subd (? b 0))))
                                       (or (and a-dir?
                                                b-dir?)
                                           (and (not a-dir?)
                                                (not b-dir?))))
                                     (string<? (? a 2) (? b 2)))
                                    (else
                                     (eq? subd (? a 0))))))))
        (~table (map (let ((under-upath (directory-vicinity upath)))
                       (lambda (type size fn mtime)
                         ;; ‘type’ can be either ‘subd’ (string) or
                         ;; a plist of the form ‘(type STRING [...])’.
                         (define subd? (eq? subd type))
                         (~tr (~td "")
                              (~td/right size)
                              (~td mtime)
                              (~td (if subd?
                                       type
                                       (cadr type)))
                              (~td (apply ~a 'href (under-upath fn)
                                          (append (if subd?
                                                      '()
                                                      type)
                                                  (list fn)))))))
                     (map (lambda (x) (? x 0)) all)
                     (map (lambda (x) (? x 1)) all)
                     (map (lambda (x) (? x 2)) all)
                     (map (lambda (x) (? x 3)) all))))))
  (end-response
   M (* 16 1024)
   'chunked
   'close))

;; Transfer a file.
;;
(define (prep-file-transfer M filename chunkable?)

  (define (c-meta! enc len)
    (and enc (M #:add-header 'Content-Encoding enc))
    (apply M #:add-header
           (tlist<-filename
            'Content-Type (if len
                              (string-drop-right filename len)
                              filename))))

  (aok! M)
  (cond ((string-suffix? ".xz" filename) (c-meta! "x-xz" 3))
        ((string-suffix? ".lz" filename) (c-meta! "x-lzip" 3))
        ((string-suffix? ".gz" filename) (c-meta! "gzip" 3))
        (else                            (c-meta! #f #f)))
  (let* ((p (open-input-file filename))
         (si (stat p))
         (in (fileno p))
         (len (stat:size si)))

    (define (reasonable-chunk-size)
      ;; TODO: Compute some intelligent value.
      2048)

    (define (sched! count)
      (M #:add-direct-writer
         count (lambda (out-port)
                 (let loop ((count count))
                   (or (zero? count)
                       (loop (- count (ttn-sendfile (fileno out-port)
                                                    in #f count))))))))

    (M #:add-header 'Last-Modified (UTCS:rfc1123-date<- #f (UTCS:<-mtime p)))
    (if chunkable?
        (let ((sz (reasonable-chunk-size)))
          (let-values (((q r) (q/r len sz)))
            (let loop ((q q))
              (cond ((zero? q)
                     (or (zero? r) (sched! r)))
                    (else
                     (sched! sz)
                     (loop (1- q)))))))
        (sched! len))
    (end-response
     #f #f
     (and chunkable?
          'chunked)
     (lambda ()
       (close-port p))
     'close)))


;; Dynamic URLs ======================================================

;; This is the list of registered dynamic handlers.  The car of the
;; association is a compiled regexp, the cdr a vector in the form
;; #(RE-STR HANDLER).  We keep RE-STR (uncompiled regexp) around in
;; order to support dynamic reloading of the config file.

(define *dynamic-url-handlers* '())

(define (add-dynamic-handler! re-str handler)
  ;; If RE-STR is already registered, update it w/ HANDLER.  Otherwise,
  ;; append a new pair (RX . #(RE-STR HANDLER)) to the global alist.
  (let loop ((ls *dynamic-url-handlers*))
    (cond ((null? ls)
           (set! *dynamic-url-handlers*
                 (append!               ; maintain order
                  *dynamic-url-handlers*
                  (acons (make-regexp re-str)
                         (vector re-str handler)
                         '()))))
          ((string=? (vector-ref (cdar ls) 0) re-str)
           (vector-set! (cdar ls) 1 handler))
          (else
           (loop (cdr ls))))))

(define (remove-dynamic-handler! re-str)
  (let loop ((ls *dynamic-url-handlers*))
    (cond ((null? ls))
          ((string=? (vector-ref (cdar ls) 0) re-str)
           (set! *dynamic-url-handlers*
                 (delq (car ls) *dynamic-url-handlers*)))
          (else
           (loop (cdr ls))))))

;; Return a dynamic handler suitable for ‘upath’, or #f if non found.
;;
(define (find-dynamic-url-handler upath)
  (let loop ((ls *dynamic-url-handlers*))
    (cond ((null? ls) #f)
          ((regexp-exec (car (car ls)) upath) (vector-ref (cdr (car ls)) 1))
          (else (loop (cdr ls))))))

(define (now-timestamp-string)
  (strftime "[%F %T]: " (localtime (current-time))))


;; Main program ======================================================

(define (somewhat-more-slackful-shutdown sock how)
  (catch 'system-error
         (lambda ()
           (shutdown sock how))
         (lambda (key who fmt args errno)
           ;; Ignore ENOTCONN "Transport endpoint is not connected".
           (or (= ENOTCONN (car errno))
               (let ((s (fs "~AInternal error on ‘~A’: ~A~%"
                            (now-timestamp-string) who
                            (apply fs fmt args))))
                 (display s *log-port*)
                 (fse "~A" s)
                 (raise SIGTERM))))))

(define (make-server-loop no-access? concurrency boredom)
  (define known '(HEAD GET))
  (define host (gethostname))

  (define log (log-http-response-proc *log-port*))

  (define (spew M req in-port)
    (let ((upath (request-upath req))
          (pvers (request-protocol-version req)))
      (cond ((find-dynamic-url-handler upath)
             => (lambda (handle)
                  (handle M in-port upath (request-headers req))))
            (else
             (and (eq? 'HEAD (request-method req))
                  (M #:inhibit-content! #t))
             (let ((filename (fs-name upath)))
               (cond ((not filename)
                      (prep-not-found M upath))
                     ((no-access? filename)
                      (prep-forbidden M upath))
                     ((file-is-directory? filename)
                      ((prep-generated-directory-index-box)
                       M upath filename))
                     (else
                      (prep-file-transfer M filename (ge-1.1? pvers)))))))))

  (define (parse port)
    (catch 'parse-error (lambda ()
                          (receive-request port))
           (lambda (key . args)
             (fse "~S: ~S~%" key args)
             #f)))

  (define (prepare pv)
    (compose-response host #:protocol-version pv))

  (define (take conn)

    (define (so-it-goes method upath)
      (lambda (status/count)
        (log (string<-sockaddr (cdr conn))
             method upath status/count)))

    (and boredom (alarm boredom))
    (let ((sock (car conn)))

      (define (out! M pvers disposition method upath)

        (define (try symbol)
          (memq symbol disposition))

        (cond ((try 'close) (M #:add-header 'Connection "close"))
              ((ge-1.1? pvers))
              (else (M #:add-header 'Connection "keep-alive")))
        (and=> (M #:send! sock (if (and (try 'chunked)
                                        (ge-1.1? pvers))
                                   '(chunked)
                                   '()))
               (so-it-goes method upath))
        (force-output sock))

      (define (seems-ok req)
        (let* ((method (request-method req))
               (upath (request-upath req))
               (pvers (request-protocol-version req))
               (M (prepare pvers))
               (disposition (if (memq method known)
                                (spew M req sock)
                                (prep-unknown-method M method upath))))
          (out! M pvers disposition method upath)
          disposition))

      (define (wtf!)
        (let* ((pvers '(1 . 0))
               (M (prepare pvers))
               (disposition (prep-bad-request M)))
          (out! M pvers disposition #f #f)
          disposition))

      (define (dispose x)
        (cond ((thunk? x)
               (x))
              ((eq? 'close x)
               (somewhat-more-slackful-shutdown sock 2))))

      (FE (cond ((parse sock) => seems-ok)
                (else (wtf!)))
          dispose)))

  (define (handle-request conn)
    (let ((sock (car conn)))

      (define (child)
        (take conn))

      (define (kick non-blocking?)
        (let ((pid (primitive-fork)))
          (cond ((zero? pid)
                 (exit (child)))
                (else
                 (close-port sock)
                 (set! sock #f)
                 (or non-blocking?
                     (zero? (status:exit-val (cdr (waitpid pid)))))))))

      (case concurrency
        ((#:new-process)        (kick #f))
        ((#:new-process/nowait) (kick #t))
        (else                   (child)))))

  (prep-generated-directory-index-box prep-generated-directory-index)
  (and boredom (alarm boredom))

  ;; rv
  (lambda (ear)
    (let loop ()
      (handle-request (accept ear))
      (loop))))

(define (log-timestamped s . args)
  (or (eq? (current-output-port) *log-port*)
      (let ((flog (make-fso *log-port*)))
        (flog "~A" (now-timestamp-string))
        (apply flog s args)
        (flog "~%")
        (force-output *log-port*))))

(define (close-socket sig listening-port)
  (log-timestamped "Shutdown (~A)" sig)
  (and (= PF_UNIX (car listening-port))
       (delete-file (list-ref listening-port 2))))

(define (numerically sig)               ; symbol -> integer
  ;; Apparently, ‘builtin-variable’ is deprecated.
  ;;- (variable-ref (builtin-variable sig))
  (module-ref the-scm-module sig))

(define (open-logfile filename)
  (open-file filename "al"))

(define (as-daemon pidfile dish! listening-port)
  (daemonize ->bool (lambda (bye)
                      (define (finish sig)
                        (sigaction (numerically sig)
                                   (lambda ignored
                                     (close-socket sig listening-port)
                                     (bye sig))))
                      ;; re-open log port
                      (let ((fn (port-filename *log-port*)))
                        (and (file-exists? fn)
                             (set! *log-port* (open-logfile fn))))
                      (finish 'SIGTERM)
                      (finish 'SIGHUP)
                      (log-timestamped "Restart (pid ~A pidfile ~S)"
                                       (getpid) pidfile)
                      (dish!))
             pidfile
             listening-port))

(define (robustly sig)

  (define hlog
    (let ((inner (make-fso *log-port*)))
      ;; hlog
      (lambda (level s . args)
        (inner "h~A, " (make-string level #\m))
        (apply inner s args)
        (newline *log-port*)
        (force-output *log-port*))))

  (define (hl3 s . args)
    (apply hlog 3 s args))

  (define (done!)
    (raise sig))

  (define (handle-system-error key who fmt args errno)
    (cond ((equal? '(98) errno)
           (let ((patience (+ 23 (random 11))))
             (apply hl3 fmt args)
             (hl3 "waiting ~S seconds ..." patience)
             (sleep patience)
             (hl3 "and trying again")))
          (else
           (done!))))

  (define (handle-unexpected-eof key . more)

    (define (two sock so-far)
      (and=> (false-if-exception (getpeername sock))
             (lambda (avec)
               (let ((family (vector-ref avec 0)))
                 (and (memq family (list AF_INET AF_INET6))
                      (hl3 "peer ~A:~A"
                           (inet-ntop family (vector-ref avec 1))
                           (vector-ref avec 2))))))
      (false-if-exception (close-port sock)))

    (case (length more)
      ((1)   #t)
      ((2)  (apply two more))
      (else (done!))))

  ;; rv
  (lambda (key . args)
    (hlog 1 "got ~S ~S" key args)
    (case key
      ((system-error)
       (apply handle-system-error key args))
      ((unexpected-eof)
       (apply handle-unexpected-eof key args))
      (else
       (done!)))
    (hlog 1 "continuing...")))

(define (main/qop qop)

  (define (inet-args port)
    (list PF_INET
          AF_INET
          INADDR_ANY
          port))

  ;; TODO: (a) rationalize configuration; (b) make configurable.
  (fluid-set! default-text-charset "UTF-8")

  (let ((parent-pid (getpid))
        (docroot (qop 'docroot (lambda (dir) (in-vicinity dir ""))))
        (listening-port (or (qop 'port (lambda (s)
                                         (cond ((string-prefix? "unix:" s)
                                                (list PF_UNIX
                                                      AF_UNIX
                                                      (expand-file-name
                                                       (substring s 5))))
                                               (else
                                                (inet-args
                                                 (string->number s))))))
                            (inet-args 8001))))

    (set! fs-name (upath->filename-proc
                   docroot (map (lambda (ext)
                                  (fs "index.~A" ext))
                                '(shtml
                                  xhtml
                                  html))))
    (let ((fn (in-vicinity (or (qop 'ulibdir)
                               (in-vicinity (getenv "HOME") ".sizzweb.d"))
                           "servlets.scm")))
      (sigaction SIGINT (lambda ignored
                          (and (file-exists? fn)
                               (false-if-exception (load fn)))))
      (set! load-servlets! (lambda () (kill parent-pid SIGINT))))
    (set! *log-port* (cond ((qop 'logfile)
                            => (lambda (logfile)
                                 (open-logfile logfile)))
                           ((qop 'daemon)
                            (open-output-file *null-device*))
                           (else
                            (current-output-port))))
    (cond ((file-exists? *system-mime-types*)
           (reset-mime-types! 491)
           (put-mime-types-from-file! 'quail *system-mime-types*)))
    (load-servlets!)
    (let ((dish (make-server-loop (access-forbidden?-proc
                                   docroot (regexp-quote "/../"))
                                  (and (qop 'funky) #:new-process)
                                  (and=> (and=> (qop 'boredom) string->number)
                                         (lambda (b)
                                           (sigaction SIGALRM (lambda ignored
                                                                (raise SIGHUP)))
                                           (max 15 b))))))

      (define (dish!)
        (let ((sock (named-socket (car listening-port)
                                  (cdr listening-port)
                                  #:socket-setup `((,SO_REUSEADDR . 1)
                                                   (,SO_LINGER 1 . 60)))))
          (listen sock 0)               ; queue length
          (let loop ()
            (catch #t (lambda ()
                        (dish sock))
                   (robustly SIGQUIT))
            (loop))))

      (cond ((qop 'daemon)
             => (lambda (pidfile)
                  (as-daemon pidfile dish! listening-port)))
            (else
             (log-timestamped "Restart (pid ~A)" parent-pid)
             (fso "Starting pid ~A on port ~A, with docroot ~S.~%"
                  parent-pid listening-port docroot)
             (call-with-current-continuation
              (lambda (done)
                (define (finish sig)
                  (sigaction (numerically sig)
                             (lambda ignored
                               (fso "(got ~A)~%" sig)
                               (close-socket sig listening-port)
                               (done #t))))
                (finish 'SIGQUIT)
                (finish 'SIGTERM)
                (dish!)))
             (fso "Shutting down~%")
             #t)))))

(define (main args)

  (define (valid-dir file)
    (define (bad msg)
      (fso "~A: ~A: ~A~%" (car args) msg file)
      #f)
    (and (or (access? file R_OK)
             (bad "cannot read"))
         (or (file-is-directory? file)
             (bad "not a directory"))))

  (check-hv args `((package . "ttn-do")
                   (version . ,*sizzweb-version*)
                   (help . commentary)))
  (main/qop
   (qop<-args
    args `((funky)
           (boredom (single-char #\b) (value #t))
           (port    (single-char #\p) (value #t))
           (docroot (single-char #\r) (value #t) (required? #t)
                    (predicate ,valid-dir))
           (ulibdir (single-char #\u) (value #t)
                    (predicate ,valid-dir))
           (daemon  (single-char #\d) (value #t))
           (logfile (single-char #\l) (value #t))))))

;;; sizzweb.scm ends here
