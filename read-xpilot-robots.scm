#!/bin/sh
exec ${GUILE-guile} -e "(ttn-do read-xpilot-robots)" -s $0 "$@" # -*-scheme-*-
!#
;;; read-xpilot-robots --- Read xpilot "robots" file

;; Copyright (C) 2002, 2003, 2005, 2006, 2007, 2008, 2009,
;;   2010, 2011, 2013 Thien-Thi Nguyen
;;
;; This file is part of ttn-do, released under the terms of the
;; GNU General Public License as published by the Free Software
;; Foundation; either version 3, or (at your option) any later
;; version.  There is NO WARRANTY.  See file COPYING for details.

;;; Commentary:

;; Usage: read-xpilot-robots FILE
;;
;; Read xpilot robots file FILE into a list, each element of which
;; having the form:
;;
;;     (NAME TYPE SHIP PARA)
;;
;; where NAME and TYPE are strings, SHIP is a list of sublists of
;; integers, and PARA is a list of integers.  When invoked from the
;; shell, display each form to stdout, one per line.

;;; Code:

(define-module (ttn-do read-xpilot-robots)
  #:export (read-xpilot-robots
            main)
  #:use-module ((ttn-do zzz banalities) #:select (check-hv))
  #:use-module ((ice-9 rdelim) #:select (read-line))
  #:use-module ((ice-9 regex) #:select (match:substring
                                        match:suffix))
  #:use-module ((srfi srfi-13) #:select (string-map))
  #:use-module ((ttn-do zzz 0gx read-string) #:select (read-string))
  #:use-module ((ttn-do zzz personally) #:select (FE fs fso)))

(define trigger-rx (make-regexp "^type:[ \t]*([^ \t]+)"))
(define next-rx    (make-regexp "^[a-z]+:[ \t]*"))

(define (make-next port)
  (lambda ()
    (match:suffix (regexp-exec next-rx (read-line port)))))

;; Read input in xpilot robots file format from @var{port}.
;; Return a list of sexps.
;;
(define (read-xpilot-robots port)
  (let ((next (make-next port))
        (acc '()))
    (let loop ((line (read-line port)))
      (cond ((eof-object? line))
            ((regexp-exec trigger-rx line)
             => (lambda (m)
                  (let ((type (match:substring m 1))
                        (para (read-string (fs "(~A)" (next))))
                        (ship (read-string
                               (fs "(~A)" (string-map
                                           (lambda (c)
                                             (if (char=? #\, c)
                                                 #\space
                                                 c))
                                           (next)))))
                        (name (next)))
                    (set! acc (cons (list name type ship para)
                                    acc))
                    (loop (read-line port)))))
            (else (loop (read-line port)))))
    (reverse! acc)))

(define (read-xpilot-robots/main args)
  (let ((file (cadr args)))
    (FE (call-with-input-file file read-xpilot-robots)
        (lambda (robot)
          (fso "~S~%" robot))))
  #t)

(define (main args)
  (check-hv args '((package . "ttn-do")
                   (version . "1.1")
                   (help . commentary)))
  (read-xpilot-robots/main args))

;;; read-xpilot-robots ends here
