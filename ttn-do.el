;;; ttn-do.el --- emacs interface to ttn-do

;; Copyright (C) 2001, 2003, 2006, 2007 Thien-Thi Nguyen
;;
;; This file is part of ttn-do, released under the terms of the
;; GNU General Public License as published by the Free Software
;; Foundation; either version 3, or (at your option) any later
;; version.  There is NO WARRANTY.  See file COPYING for details.

;;; Code:

(defvar ttn-do-commands nil
  "*List of commands for ttn-do(1).
Set to nil to force update on next invocation of `ttn-do'.")

(defun ttn-do (command &optional args)
  "Do `(compile \"ttn-do COMMAND ARGS\")'.
See also variable `ttn-do-commands'."
  (interactive
   (list (completing-read
          "ttn-do " (or ttn-do-commands
                        (setq ttn-do-commands
                              (split-string (shell-command-to-string
                                             "ttn-do | sed 1d")))))
         (read-string "args: ")))
  (compile (concat "ttn-do " command " " args))
  (switch-to-buffer "*compilation*")
  (delete-other-windows))

;; this requires support in ~/.xmodmaprc
;; keycode 117 = Hyper_R
;; add Mod3 = Hyper_R

(global-set-key [?\H-p] 'ttn-do)
(define-key minibuffer-local-map [?\H-p] 'previous-history-element)
(define-key minibuffer-local-map [?\H-n] 'next-history-element)

(provide 'ttn-do)

;;; ttn-do.el ends here
