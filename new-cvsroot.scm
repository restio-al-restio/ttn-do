#!/bin/sh
exec ${GUILE-guile} -e '(ttn-do new-cvsroot)' -s $0 "$@" # -*- scheme -*-
!#
;;; new-cvsroot --- change CVS/Root files under cwd

;; Copyright (C) 2001, 2006, 2007, 2010, 2011, 2013, 2021 Thien-Thi Nguyen
;;
;; This file is part of ttn-do, released under the terms of the
;; GNU General Public License as published by the Free Software
;; Foundation; either version 3, or (at your option) any later
;; version.  There is NO WARRANTY.  See file COPYING for details.

;;; Commentary:

;; Usage: new-cvsroot NEW-ROOT
;;
;; Replace contents of all CVS/Root files under cwd w/ NEW-ROOT.

;;; Code:

(define-module (ttn-do new-cvsroot)
  #:export (main)
  #:use-module ((ice-9 ftw) #:select (nftw))
  #:use-module ((srfi srfi-13) #:select (string-suffix?))
  #:use-module ((ttn-do zzz banalities) #:select (check-hv
                                                  decry))
  #:use-module ((ttn-do zzz personally) #:select (FE fs fso)))

(define (main args)
  (check-hv args '((package . "ttn-do")
                   (version . "1.1")
                   (help . commentary)))

  (and (null? (cdr args))
       (decry "Missing argument, try --help"))

  (let ((acc '()))

    (define (pick filename st flag base level)
      (and (eq? 'directory flag)
           (string-suffix? "CVS" filename 0 3 base)
           (set! acc (cons filename acc)))
      #t)

    (define spew!
      (let ((new-root (fs "~A~%" (cadr args))))
        (lambda ()
          (display new-root))))

    (nftw "." pick 'physical)
    (FE (sort acc string<?)
        (lambda (filename)
          (fso "~A~%" filename)
          (with-output-to-file (fs "~A/Root" filename)
            spew!)))))

;;; new-cvsroot ends here
