#!/bin/sh
exec ${GUILE-guile} -e '(ttn-do make-gnu-pkg-homepage)' -s $0 "$@" # -*- scheme -*-
!#
;;; make-gnu-pkg-homepage

;; Copyright (C) 2013, 2014, 2020, 2021 Thien-Thi Nguyen
;;
;; This file is part of ttn-do, released under the terms of the
;; GNU General Public License as published by the Free Software
;; Foundation; either version 3, or (at your option) any later
;; version.  There is NO WARRANTY.  See file COPYING for details.

;;; Commentary:

;; Usage: make-gnu-pkg-homepage [options] INFILE
;;
;; Write to stdout some XHTML suitable for a GNU project
;; homepage, using the contents of INFILE.  Options are:
;;
;;  -o, --output FILE   -- write output to FILE
;;  -D, --debug         -- keep temporary html-data file;
;;                         write its name to stderr at finish
;;
;; The format was adapted from boilerplate.html w/ Parent-Version: 1.92.
;; INFILE is a series of alternating keywords and sexps:
;;
;; #:name -- STRING
;;   the package name; in the output ‘tarname’ is downcased ‘name’
;;   and ‘bug-report-email’ is "bug-" prefixed to ‘tarname’
;;
;; #:license -- SYMBOL
;;   typically ‘GPLv3+’
;;
;; #:style -- STRING
;;   text/css to be inserted right after the <title> (in <head>)
;;   w/ surrounding whitespace trimmed; formatted as follows:
;;     <style type="text/css"><!--
;;     STRING
;;     --></style>
;;
;; #:intro -- list of XHTML-TREE
;;   introduction to the package (what is it, why use it, etc);
;;   typically this is a series of ‘(~p ...)’ (paragraph) forms
;;   comprising strings and (properly nested) XHTML "~FOO" forms
;;
;; #:mailing-lists -- (BUG-REPORT-ROLE [(NAME ROLE) ...])
;;   a list whose car is a XHTML-TREE describing the role of
;;   ‘bug-report-email’, followed by sub-lists, naming other
;;   mailing lists and their respective roles
;;
;; #:helping-out -- list of SYMBOL
;;   flags for the "Getting involved" section; valid SYMBOLs:
;;     no-test        -- don't mention test releases
;;     no-translation -- don't mention translation efforts
;;
;; #:maintainers -- STRING
;;   real names of maintainers, comma-separated
;;
;; #:donation -- STRING  (optional)
;;   URL for the "tip jar" link appended after the list of
;;   maintainers, in a pair of parens
;;
;; #:more-source -- list of STRING
;;   other filenames (in cwd) to link to in the "source" footer;
;;   no need to specify ‘NEWS-excerpt’

;;; Code:

(define-module (ttn-do make-gnu-pkg-homepage)
  #:export (main
            ~~standard-homepage)
  #:use-module ((ice-9 regex) #:select (string-match
                                        match:substring))
  #:use-module ((ttn-do zzz personally) #:select (fs
                                                  fse
                                                  make-fso))
  #:use-module ((ttn-do zzz banalities) #:select (check-hv
                                                  decry
                                                  qop<-args))
  #:use-module (ttn-do zzz xhtml-tree)
  #:use-module ((ttn-do mark-up-news-excerpt) #:select (tree<-news-excerpt))
  #:use-module ((ttn-do zzz publishing) #:select (:LF
                                                  w/o-<&>
                                                  flatten-to
                                                  html-data<-file))
  #:use-module ((ttn-do zzz 0gx forms-from) #:select (forms<-file))
  #:use-module ((ttn-do zzz 0gx temporary-file) #:select (unique-i/o-file-port
                                                          unlink-port-filename))
  #:use-module ((srfi srfi-1) #:select (car+cdr))
  #:use-module ((srfi srfi-11) #:select (let-values))
  #:use-module ((srfi srfi-13) #:select (string-trim-both)))

(define CONFIG #f)

(define (configure! filename)
  (let loop ((acc (acons 'donation "" '()))
             (ls (forms<-file filename)))
    (if (pair? ls)
        (loop
         (let ((k (keyword->symbol (car ls)))
               (v (cadr ls)))
           (append (case k
                     ((name)
                      (let ((tarname (string-downcase v)))
                        `((name . ,v)
                          (tarname . ,tarname)
                          (bug-report-email . ,(fs "bug-~A" tarname)))))
                     (else
                      (list (cons k v))))
                   acc))
         (cddr ls))
        (let* ((port (unique-i/o-file-port (fs "~AT" filename)))
               (fso (make-fso port)))
          (define (ref k)
            (or (assq-ref acc k)
                (decry "missing ~S" k)))
          (define (Q! obj)
            `(quote ,obj))
          (define (L! ls)
            `(list ,@ls))
          (define (ml spec)
            (L! spec))
          (fso "~S~%~S~%"
               '(use-modules
                 (ttn-do make-gnu-pkg-homepage)
                 (ttn-do zzz xhtml-tree))
               `(~~standard-homepage
                 ,(L! (map (lambda (k)
                             `(cons ,(Q! k)
                                    ,(let ((v (ref k)))
                                       (case k
                                         ((name tarname maintainers
                                                donation style
                                                bug-report-email)
                                          v)
                                         ((more-source intro)
                                          (L! v))
                                         ((license)
                                          (Q! v))
                                         ((helping-out)
                                          (L! (map Q! v)))
                                         ((mailing-lists)
                                          (L! (map L! v)))))))
                           '(name
                             tarname
                             maintainers
                             donation
                             style
                             intro
                             bug-report-email
                             more-source
                             license
                             helping-out
                             mailing-lists)))))
          (seek port 0 SEEK_SET)
          port))))

(define (package key)
  (assq-ref CONFIG key))

(define (~~ssi filename)
  (list :LF
        "<!--#include virtual=\"" filename "\" -->"
        :LF))

(define (~~/// . elements)
  (define (closing elem)
    (list :LF "</" (symbol->string elem) ">"))
  (map closing elements))

(define (~~a/mailto addr)
  (let ((full (fs "~A@gnu.org" addr)))
    (~a 'href (list "mailto:" full)
        (w/o-<&> (fs "<~A>" full)))))

(define (~~sources)

  (define (ref x)
    (~a 'href x (~samp x)))

  (list "(source: "
        (ref "NEWS-excerpt")
        (map (lambda (filename)
               (list ", " (ref filename)))
             (package 'more-source))
        ")"))

(define (~~news-excerpt)
  (tree<-news-excerpt
   "NEWS-excerpt"
   (fs "https://ftpmirror.gnu.org/~A/" (package 'tarname))))

(define (~~download/news)
  (list

   (~h3 'id "download" "Download / News")

   (~p "(" (let ((full (fs "://ftp.gnu.org/gnu/~A" (package 'tarname))))
             (define (item scheme)
               (~a 'href (string-append scheme full)
                   (string-upcase scheme)))
             (list
              (item "https") ",  "
              (item "http") ",  "
              (item "ftp") ",  "
              (~a 'href "/prep/ftp.html"
                  "mirrors")))
       ")")

   (~~news-excerpt)

   (~p "("
       (~a 'title "plain text NEWS excerpt"
           'href "NEWS-excerpt"
           "unadorned")
       ")")))

(define (~~documentation)
  (list

   (~h3 'id "documentation" "Documentation")

   (let ((name (package 'name))
         (tarname (package 'tarname)))
     (~p (~a 'href "manual/"
             "Documentation for " name)
         " is available online, as is"
         " " (~a 'href "/manual/"
                 "documentation for most GNU software")
         "."
         " You may also find information about " name " by running"
         " " (~kbd "info " tarname)
         " or by looking at"
         " " (~code (fs "/usr/share/doc/~A/" tarname))
         ", " (~code (fs "/usr/local/share/doc/~A/" tarname))
         ", or similar directories on your system."))))

(define (~~mailing-lists)

  (define (name)
    (package 'name))

  (define (listinfo-ref addr)
    (~a 'href (fs "https://lists.gnu.org/mailman/listinfo/~A" addr)
        addr))

  (list

   (~h3 'id "mail" "Mailing Lists")

   (~p (name) " has the following mailing lists:")

   (apply ~ul (map (lambda (ent)
                     (let-values (((addr role) (car+cdr ent)))
                       (~li (listinfo-ref addr)
                            role)))
                   (let-values (((bug-report-role etc)
                                 (car+cdr (package 'mailing-lists))))
                     (acons (package 'bug-report-email)
                            bug-report-role
                            etc))))

   (~p "Announcements about " (name)
       " and most other GNU software are made on the"
       " " (~a 'href "https://lists.gnu.org/mailman/listinfo/info-gnu"
               "info-gnu")
       " mailing list"
       " (" (~a 'href "https://lists.gnu.org/archive/html/info-gnu/"
                "archive")
       ").")

   (~p "Security reports that should not be made immediately"
       " public can be sent directly to the maintainer."
       " If there is no response to an urgent"
       " issue, you can escalate to the general"
       " " (~a 'href "https://lists.gnu.org/mailman/listinfo/security"
               "security")
       " mailing list for advice.")))

(define (~~helping-out)

  (define (name)
    (package 'name))

  (define (tarname)
    (package 'tarname))

  (define (opt? sym)
    (memq sym (package 'helping-out)))

  (list

   (~h3 'id "contribute" "Getting involved")

   (~p "Development of " (name)
       ", and GNU in general, is"
       " a volunteer effort, and you can contribute."
       " For information, please read"
       " " (~a 'href "/help/"
               "How to help GNU")
       "."
       " If you'd like to get involved, it's a good idea to join the"
       " discussion mailing list (see above).")

   (let ((all '()))

     (define (push! term definition)
       (set! all (acons term definition all)))

     (or (opt? 'no-test)
         (push! "Test releases"
                (let ((common (fs "//alpha.gnu.org/gnu/~A/" (tarname))))

                  (define (via proto)
                    (let ((full (fs "~A:~A" proto common)))
                      (~a 'href full full)))

                  "Trying the latest test release (when available)"
                  " is always appreciated. Test releases of " (name)
                  " can be found at"
                  " " (via 'http) " (via HTTP) and"
                  " " (via 'ftp)  " (via FTP).")))

     (push! "Development"
            (list
             "For development sources, issue trackers, and other"
             " information, please see the"
             " " (~a 'href (fs "https://savannah.gnu.org/projects/~A/"
                               (tarname))
                     (name) " project page")
             " at"
             " " (~a 'href "https://savannah.gnu.org"
                     "savannah.gnu.org")
             "."))

     (or (opt? 'no-translation)
         (push! (fs "Translating ~A" (name))
                (list
                 "To translate " (name) "'s messages into other"
                 " languages, please see the"
                 " " (~a 'href (fs "http://translationproject.org/domain/~A.html"
                                   (tarname))
                         "Translation Project page for " (name))
                 ". If you have a new translation of the message strings,"
                 " or updates to the existing strings, please have the"
                 " changes made in this repository. Only translations"
                 " from this site will  be incorporated into " (name)
                 ". For more information, see the"
                 " " (~a 'href "http://translationproject.org/html/welcome.html"
                         "Translation Project")
                 ".")))

     (push! "Maintainer"
            (list
             (name) " is currently maintained by"
             " " (package 'maintainers)
             (let ((donation (package 'donation)))
               (if (string-null? donation)
                   donation             ; i-run-me! :-D
                   (list " (" (~a 'href donation
                                  "tip jar")
                         ")")))
             ". Please use the mailing lists for contact."))

     (set! all (reverse! all))
     (if (> 2 (length all))
         ;; fancy
         (~dl (map (lambda (term definition)
                     (list (~dt term)
                           (~dd definition)))
                   (map car all)
                   (map cdr all)))
         ;; basic
         (map ~p (map cdr all))))))

(define (~~licensing)
  (let* ((nick (package 'license))
         (m (string-match "^(.+)v([0-9.]+)([+]?)"
                          (symbol->string nick)))
         (base (match:substring m 1))
         (vers (match:substring m 2))
         (plus (not (string-null? (match:substring m 3)))))
    (list

     (~h3 'id "license" "Licensing")

     (~p (package 'name) " is free software; you can redistribute"
         " it and/or modify it under the terms of the"
         " " (~a 'href (fs "/licenses/~A.html"
                           (string-downcase base))
                 'rel "license"
                 (case (string->symbol base)
                   ((GPL) "GNU General Public License")
                   ((LGPL) "GNU Lesser General Public License")
                   (else (decry "bad license: ~S" nick))))
         " as published by the Free Software Foundation;"
         (let ((v (fs "version ~A of the License" vers)))
           (if plus
               (list (fs " either ~A," v)
                     " or (at your option) any later version")
               v))
         "."))))

(define (~~boilerplate-id version)
  (list "<!-- Parent-Version: " version " -->" :LF
        "<!-- This page is derived from"
        " /server/standards/boilerplate.html -->" :LF))

(define (~~style-block opaque-string)
  ;; This differs from ~style in that it includes <!-- and -->.
  ;; Also, it doesn't handle anything but OPAQUE-STRING -- lame!
  (if opaque-string
      (list :LF "<style type=\"text/css\"><!--"
            :LF (string-trim-both opaque-string)
            :LF "--></style>")
      ""))

;; Expand to a tree comprising several @dfn{ssi}
;; (or ``server-side include'') directives and other
;; boilerplate, properly formatted, based on the values
;; of @var{alist}.  This is an ``internal'' procedure.
;;
(define (~~standard-homepage alist)
  (set! CONFIG alist)
  (list
   (~~ssi "/server/header.html")
   (~~boilerplate-id "1.92")
   (~title (list (package 'name)
                 " - GNU Project"
                 " - Free Software Foundation"))
   (~~style-block (package 'style))
   (~~ssi "/server/gnun/initial-translations-list.html")
   (~~ssi "/server/banner.html")
   (~div
    'class "reduced-width"
    (~h2 (list "GNU " (package 'name)))
    (package 'intro)
    (~~download/news)
    (~~documentation)
    (~~mailing-lists)
    (~~helping-out)
    (~~licensing)
    (~~///
     ;; to close <div id="content"> started in the ‘~~ssi’ above
     'div))
   (~~ssi "/server/footer.html")
   (~div
    'id "footer"

    (~div
     'class "unprintable"
     (~p "Please send general FSF &amp; GNU inquiries to"
         " " (~~a/mailto "gnu")
         ". There are also"
         " " (~a 'href "/contact/"
                 "other ways to contact")
         " the FSF. Broken links and other corrections or suggestions"
         " can be sent to"
         " " (~~a/mailto (package 'bug-report-email))
         ".")
     (~p
      "Please see the"
      " " (~a 'href "/server/standards/README.translations.html"
              "Translations README")
      " for information on coordinating and contributing translations"
      " of this article."))

    (~p (strftime "Copyright &copy; %Y Free Software Foundation, Inc."
                  (gmtime (current-time))))

    (~p "This page is licensed under a"
        " " (~a 'rel "license"
                'href "http://creativecommons.org/licenses/by-nd/4.0/"
                "Creative Commons Attribution-NoDerivatives 4.0 International License")
        ".")

    (~~ssi "/server/bottom-notes.html")

    (~p 'class "unprintable"
        (strftime "Updated: %F %R UTC" (gmtime (current-time)))
        " " (~~sources)))
   (~~///
    'div
    'body
    'html)))

(define (main args)
  (check-hv args `((package . "ttn-do")
                   (version . "2.0")
                   ;; 2.0  -- revamp to sync w/ Parent-Version 1.92
                   ;;         make year in copyright notice dynamic
                   ;;         add #:style support
                   ;;         no longer use non-breaking spaces
                   ;;         make #:donation optional
                   ;;         add "basic" mode for "Getting involved"
                   ;;         various minor bugfixes
                   ;; 1.1  -- use non-breaking spaces
                   ;;         add "tip jar" support
                   ;; 1.0  -- initial release
                   (help . commentary)))
  (let* ((qop (qop<-args args '((output (single-char #\o) (value #t))
                                (debug (single-char #\D)))))
         (inp (configure! (car (qop '()))))
         (port (or (qop 'output open-output-file)
                   (current-output-port))))
    (flatten-to port (html-data<-file (port-filename inp)))
    (cond ((qop 'debug)
           (or (qop 'output) (newline))
           (fse "~A: DEBUG: not deleting temp file ‘~A’~%"
                (basename (car (command-line)))
                (port-filename inp)))
          (else
           (unlink-port-filename inp))))
  #t)

;;; make-gnu-pkg-homepage ends here
