# find-exports.sed
#
# Copyright (C) 2011, 2013 Thien-Thi Nguyen
#
# This file is part of ttn-do, released under the terms of the
# GNU General Public License as published by the Free Software
# Foundation; either version 3, or (at your option) any later
# version.  There is NO WARRANTY.  See file COPYING for details.

# Find ‘#:export[-syntax] LIST’, where LIST is "(" SYMBOL* ")",
# possibly expressed over multiple lines with spaces here and there.
/^  .:export/!d
:more
/)/! {
    N
    s/\n/ /
    b more
}

# Remove #:export[-syntax], leaving only LIST.
s/#:export-*[a-z]*//

# Strip parens from LIST, leaving SYMBOL*.
y/()/  /

# Ignore symbol ‘main’.
s/ main //g

# Squeeze whitespace.
s/  */ /g

# Add begin and end markers (for debugging).
s/^//
s/ *$//

# If there are no symbols, we are done.
//d

# Format each symbol with a tsin directive.
s/ /\n\n@tsin i /g

# Get rid of the end marker.
s///

# Replace the begin marker with a texinfo sectioning command.
//s//@section Usage from Scheme/

# find-exports.sed ends here
