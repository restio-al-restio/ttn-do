#!/bin/sh
exec ${GUILE-guile} -e '(ttn-do wcat)' -s $0 "$@" # -*- scheme -*-
!#
;;; wcat

;; Copyright (C) 2001, 2003-2012, 2017, 2019-2021 Thien-Thi Nguyen
;;
;; This file is part of ttn-do, released under the terms of the
;; GNU General Public License as published by the Free Software
;; Foundation; either version 3, or (at your option) any later
;; version.  There is NO WARRANTY.  See file COPYING for details.

;;; Commentary:

;; Usage: wcat [options] [URL ...]
;;
;; Options:
;;  -h, --headers   -- output only headers (unless --both)
;;  -z, --zcat      -- pass output through "gzip -d -c -f"
;;  -b, --both      -- output headers, blank line, body
;;  -n, --newline   -- make sure each output starts on a new line
;;  -B, --bad ELEM  -- send a non-well formed ELEM (for server testing)
;;
;; Cat out web arguments.  When headers are output, the format is:
;;
;;   PROTOCOL/VERSION RESPONSE-CODE RESPONSE-MESSAGE
;;   HEADER-NAME: HEADER-VALUE
;;   ...
;;
;; URL can have a "standard" form (e.g., "http://HOST:PORT/PATH"),
;; or SOCKET-NAME,PATH (note comma separator), where SOCKET-NAME is
;; a Unix-domain filename and PATH is the same as for a standard URL.
;; For a standard URL, the "scheme" MUST be "http" (i.e., "https", "ftp",
;; etc. are not supported and results in an "Unknown URL scheme" error).
;;
;; ELEM is one of:
;; * header-no-colon
;;   Send a "header" line "WRONG".
;; * header-no-name
;;   Send a "header" line ":WRONG".

;;; Code:

(define-module (ttn-do wcat)
  #:export (main)
  #:use-module ((ttn-do zzz banalities) #:select (check-hv
                                                  decry
                                                  qop<-args))
  #:use-module ((www url) #:select (url:parse
                                    url:make-http
                                    url:scheme
                                    url:host
                                    url:port))
  #:use-module ((www http) #:select (http:connect
                                     http:open
                                     send-request
                                     receive-response
                                     http:message-version
                                     http:message-status-code
                                     http:message-status-text
                                     http:message-header
                                     http:message-body
                                     http:message-headers))
  #:use-module (ice-9 optargs)
  #:use-module ((ice-9 popen) #:select (open-output-pipe
                                        close-pipe))
  #:use-module ((ice-9 regex) #:select (string-match
                                        match:substring
                                        match:suffix))
  #:use-module ((ttn-do zzz personally) #:select (FE fso)))

(define (parse-ref string)              ; => url object
  (cond ((string-match "^(/[^,]+)," string)
         => (lambda (m)
              (url:make-http AF_UNIX (match:substring m 1) (match:suffix m))))
        (else
         (url:parse string))))

(define cmgr
  (let ((pool '()))
    (define (szonk! ent)
      (close-port (cdr ent))
      (set-cdr! ent #f)
      (set! pool (delq! ent pool)))
    ;; cmgr
    (lambda (command targ)
      (case command
        ((open!) (or (assoc-ref pool targ)
                     (let ((sock (if (eq? AF_UNIX (car targ))
                                     (apply http:connect PF_UNIX targ)
                                     (apply http:open targ))))
                       (set! pool (acons targ sock pool))
                       sock)))
        ((close!) (and=> (assoc targ pool) szonk!))
        ((finish!) (FE pool szonk!))))))

(define* (get-one weirdness url #:optional head-only?)
  (case (url:scheme url)
    ((unknown) (decry "Unknown URL scheme")))
  (let* ((targ (list (url:host url) (or (url:port url) 80)))
         (pending (send-request (cmgr 'open! targ)
                                (if head-only? 'HEAD 'GET)
                                url
                                #:headers (case weirdness
                                            ((header-no-colon)
                                             '("WRONG"))
                                            ((header-no-name)
                                             '(":WRONG"))
                                            (else
                                             '()))))
         (answer (receive-response pending))
         (ch (and=> (http:message-header 'Connection answer)
                    (lambda (s)
                      (string->symbol (string-downcase s))))))
    (and (cond ((string=? "HTTP/1.0" (http:message-version answer))
                (not (eq? 'keep-alive ch)))
               (else
                (eq? 'close ch)))
         (cmgr 'close! targ))
    answer))

(define (process-proc weirdness newline? head? body?)
  (lambda (string)
    (and newline? (or (zero? (port-column (current-output-port)))
                      (newline)))
    (let ((ans (get-one weirdness (parse-ref string) (not body?))))
      (cond (head?
             (fso "~A ~A ~A~%"
                  (http:message-version ans)
                  (http:message-status-code ans)
                  (http:message-status-text ans))
             (FE (http:message-headers ans)
                 (lambda (pair)
                   (fso "~A: ~A~%" (car pair) (cdr pair))))))
      (and head? body? (newline))
      (and body? (display (http:message-body ans))))))

(define (main args)
  (check-hv args '((package . "ttn-do")
                   (version . "1.8")
                   ;; 1.8  -- error out on unrecognized URL scheme
                   ;; 1.7  -- Guile 2 slog
                   ;; 1.6  -- fix ‘--help’ output
                   ;; 1.5  -- connection pool; slog; three new options
                   ;; 1.4  -- option ‘zcat’
                   ;; 1.3  -- UNIX-domain support
                   ;; 1.2  -- option ‘headers’
                   ;; 1.1  -- handle multiple URLs
                   ;; 1.0  -- initial release
                   (help . commentary)))
  (let* ((qop (qop<-args args '((headers (single-char #\h))
                                (newline (single-char #\n))
                                (both (single-char #\b))
                                (bad (single-char #\B) (value #t))
                                (zcat (single-char #\z)))))
         (urls (qop '())))
    (if (null? urls)
        (decry "no url specified")
        (let ((zp (and (qop 'zcat) (open-output-pipe "gzip -d -c -f"))))
          (cond (zp (set-current-output-port zp)))
          (FE urls (process-proc (qop 'bad string->symbol)
                                 (qop 'newline)
                                 (or (qop 'both)
                                     (qop 'headers))
                                 (or (qop 'both)
                                     (not (qop 'headers)))))
          (cmgr 'finish! #f)
          (cond (zp (force-output zp)
                    (close-pipe zp)))))))

;;; wcat ends here
