#!/bin/sh
exec ${GUILE-guile} -e '(ttn-do local-build)' -s $0 "$@" # -*-scheme-*-
!#
;;; local-build

;; Copyright (C) 2001, 2004, 2007, 2011, 2012, 2017 Thien-Thi Nguyen
;;
;; This file is part of ttn-do, released under the terms of the
;; GNU General Public License as published by the Free Software
;; Foundation; either version 3, or (at your option) any later
;; version.  There is NO WARRANTY.  See file COPYING for details.

;;; Commentary:

;; Usage: local-build [options] TARBALL[...] [-- configure-options...]
;;
;; Do the standard build and install process for GNU programs,
;; namely "configure ; make ; make install".  For each TARBALL
;; (filename of the form ‘STEM.tgz’ or ‘STEM.tar.[glx]z’),
;; unpack the package in the current directory and build in place.
;; The prefix is ‘$HOME/local’.
;;
;; Options:
;;   --make-cmd CMD    -- Use CMD to build instead of simply "make".
;;   --no-install      -- Skip "make install".
;;   --check           -- Do "make check" before "make install".
;;   --clean           -- Do "make clean" after "make install".
;;
;; Additional options to the configure script can be specified
;; by appending them after the argument "--".
;;
;; Examples:
;;   $ local-build rcs-5.7.tar.gz cvs-1.10.tar.gz
;;   $ local-build --no-install \
;;                 --make-cmd=/home/gnutex/bin/make-and-install-gcc \
;;                 gcc-2.95.2.tar.gz \
;;                 -- --with-gnu-as

;;; Code:

(define-module (ttn-do local-build)
  #:export (main)
  #:use-module ((ttn-do zzz banalities) #:select (check-hv
                                                  qop<-args))
  #:use-module ((ttn-do zzz personally) #:select (fs fso FE))
  #:use-module ((ttn-do zzz filesystem) #:select (dir-exists?
                                                  with-cwd))
  #:use-module ((ttn-do zzz subprocess) #:select (sysfmt))
  #:use-module ((ttn-do zzz 0gx a-dash-dash-b) #:select (a-dash-dash-b))
  #:use-module ((srfi srfi-2) #:select (and-let*))
  #:use-module ((srfi srfi-11) #:select (let-values))
  #:use-module ((srfi srfi-13) #:select (string-suffix?
                                         string-drop-right
                                         string-prefix?
                                         string-join)))

(define LOCAL (in-vicinity (getenv "HOME") "local"))

(define (fln s . args)
  (apply fso s args)
  (newline))

(define (line)
  (fln (make-string 55 #\=)))

(define (ign s . args)
  (apply fso s args)
  (fln " ... ignoring"))

(define (child s . args)
  (zero? (apply sysfmt s args)))

(define (maybe s)
  (or (symbol? s)
      (child s)))

(define (suffix?/without tail filename)
  (and (string-suffix? tail filename)
       (string-drop-right filename (string-length tail))))

(define (build do-make do-check do-install do-clean configure-opts)
  (lambda (tarball)
    (line)
    (fln "Building ~A" tarball)
    (or (string=? "" configure-opts)
        (fln "  with ~S" configure-opts))
    (and-let* ((f (basename tarball))
               (base (cond ((suffix?/without ".tar.gz" f))
                           ((suffix?/without ".tgz" f))
                           ((suffix?/without ".tar.lz" f))
                           ((suffix?/without ".tar.xz" f))
                           (else (ign "Cannot handle ~A" tarball)
                                 #f))))
      (or (dir-exists? base)
          ;; GNU tar automagically DTRT: (info "(tar) gzip")
          (sysfmt "tar xvf ~A" tarball))
      (if (not (dir-exists? base))
          (ign "Cannot find base dir ~A" base)
          (with-cwd base
            (fln "In directory ~A" (getcwd))
            (if (not (access? "configure" X_OK))
                (ign "Could not find ./configure")
                (and (child "./configure --prefix ~A ~A"
                            LOCAL configure-opts)
                     (maybe do-make)
                     (maybe do-check)
                     (maybe do-install)
                     (maybe do-clean)
                     (fln "Successful build: ~A" base))))))))

(define (main/qop me qop configure-opts)
  (setenv "PATH" (fs "~A:~A"
                     (in-vicinity LOCAL "bin")
                     (getenv "PATH")))
  (let ((do-clean (let ((yes "make clean"))
                    (cond ((qop 'clean)
                           (fln "Will do ~S after \"make install\"..."
                                yes)
                           yes)
                          (else 'skip))))
        (do-install (let ((yes "make install"))
                      (cond ((qop 'no-install)
                             (fln "Will skip ~S..." yes)
                             'skip)
                            (else yes))))
        (do-check (let ((yes "make check"))
                    (cond ((qop 'check)
                           (fln "Will do ~S before \"make install\"..."
                                yes)
                           yes)
                          (else 'skip))))
        (do-make (or (qop 'make-cmd) "make")))
    (FE (qop '()) (build do-make do-check do-install do-clean
                         (string-join configure-opts)))
    (line)
    (fln "~A done" me)
    #t))

(define (main args)
  (check-hv args '((package . "ttn-do")
                   (version . "2.2")
                   ;; 2.2 -- handle .tar.lz
                   ;; 2.1 -- handle .tar.xz
                   ;; 2.0 -- redesign "configure options" spec/support
                   ;; 1.3 -- bail if no configure script
                   ;; 1.2 -- add --check support
                   ;; 1.1 -- fix topdir bug
                   ;;        add --version support
                   ;;        enhance --help output
                   ;; 1.0 -- original release
                   (help . commentary)))
  (fln ">>>")
  (fln ">>> ~A" (string-join args))
  (fln ">>>")
  (let-values (((args configure-opts) (a-dash-dash-b args)))
    (main/qop (car args)
              (qop<-args args '((make-cmd (value #t))
                                (no-install)
                                (clean)
                                (check)))
              configure-opts)))

;;; local-build ends here
