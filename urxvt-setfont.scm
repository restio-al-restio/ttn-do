#!/bin/sh
exec ${GUILE-guile} -e '(ttn-do urxvt-setfont)' -s $0 "$@" # -*- scheme -*-
!#
;;; urxvt-setfont --- set the (current) urxvt's font

;; Copyright (C) 2008, 2010, 2011, 2021 Thien-Thi Nguyen
;;
;; This file is part of ttn-do, released under the terms of the
;; GNU General Public License as published by the Free Software
;; Foundation; either version 3, or (at your option) any later
;; version.  There is NO WARRANTY.  See file COPYING for details.

;;; Commentary:

;; Usage: urxvt-setfont TTF-FONT-SPEC
;;
;; Set the (current) urxvt's font to TTF-FONT-SPEC.
;; For example:
;;  urxvt-setfont "DejaVu Sans Mono-12"
;;
;; See also: urxvt(1), urxvt(7).

;;; Code:

(define-module (ttn-do urxvt-setfont)
  #:export (main)
  #:use-module ((ttn-do zzz banalities) #:select (check-hv
                                                  decry))
  #:use-module ((ttn-do zzz personally) #:select (fso)))

(define (main args)
  (check-hv args '((package . "ttn-do")
                   (version . "1.1")
                   (help . commentary)))

  (and (null? (cdr args))
       (decry "Missing argument, try --help"))

  (fso "~A]710;xft:~A~A"
       (integer->char 27)               ; ESC
       (cadr args)                      ; font name
       (integer->char 7)))              ; C-g

;;; urxvt-setfont ends here
