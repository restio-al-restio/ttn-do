#!/bin/sh
exec ${GUILE-guile} -e '(ttn-do primes)' -s $0 "$@" # -*- scheme -*-
!#
;;; primes

;; Copyright (C) 2009, 2010, 2011, 2012 Thien-Thi Nguyen
;;
;; This file is part of ttn-do, released under the terms of the
;; GNU General Public License as published by the Free Software
;; Foundation; either version 3, or (at your option) any later
;; version.  There is NO WARRANTY.  See file COPYING for details.

;;; Commentary:

;; Usage: primes LIMIT
;;
;; Display prime numbers in the range [2,LIMIT], one per line.
;; If LIMIT is less than two or not a number, do nothing.

;;; Code:

(define-module (ttn-do primes)
  #:export (primes
            main)
  #:use-module ((ice-9 rdelim) #:select (write-line))
  #:use-module ((ttn-do zzz 0gx bv) #:select (make-bv bv-ref bv-set!))
  #:use-module ((ttn-do zzz banalities) #:select (check-hv))
  #:use-module ((ttn-do zzz personally) #:select (FE)))

;; Return a list of prime numbers in the range [2,@var{limit}].
;;
(define (primes limit)

  (define (p2 n)
    (+ 2 n))

  (let* ((sz (1+ limit))
         (bv (make-bv sz #f))
         (half (ash sz -1)))
    (let loop ((acc (if (<= 2 limit)
                        (list 2)
                        '()))
               (n 3))
      (cond (n (if (< half n)
                   (bv-set! bv n #t)
                   (do ((i (1- (quotient limit n)) (1- i)))
                       ((zero? i))
                     (bv-set! bv (* n (1+ i)) #t)))
               (loop (cons n acc)
                     (let next ((n (p2 n)))
                       (and (> sz n)
                            (if (bv-ref bv n)
                                (next (p2 n))
                                n)))))
            (else (reverse! acc))))))

(define (main args)
  (check-hv args '((package . "ttn-do")
                   (version . "1.1")
                   ;; 1.1  -- performance bump
                   ;; 1.0  -- initial release
                   (help . commentary)))
  (or (null? (cdr args))
      (FE (primes (or (string->number (cadr args)) ; slack
                      0))
          write-line)))

;;; primes ends here
