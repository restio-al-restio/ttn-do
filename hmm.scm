#!/bin/sh
exec ${GUILE-guile} -e '(ttn-do hmm)' -s $0 "$@" ;# -*- scheme -*-
!#
;;; hmm

;; Copyright (C) 2017 Thien-Thi Nguyen
;;
;; This file is part of ttn-do, released under the terms of the
;; GNU General Public License as published by the Free Software
;; Foundation; either version 3, or (at your option) any later
;; version.  There is NO WARRANTY.  See file COPYING for details.

;;; Commentary:

;; Usage: hmm [REPOS-LIST]
;;
;; Read file REPOS-LIST which contains the relative filenames
;; of .git directories under the directory of REPOS-LIST.
;; This file is ‘read’; Scheme comments (semicolon to eol)
;; and arbitrary intervening whitespace are permitted.
;;
;; If unspecified, REPOS-LIST defaults to .USER.hmm.list in the
;; current directory, where USER is the value of env var ‘USER’
;; or env var ‘LOGIN’ or simply "nobody".
;;
;; Sort the dirs by modification time of the HEAD file,
;; most recent first.
;;
;; For each related working dir WDIR (i.e., parent of .git dir),
;; display to stdout the line:
;;
;;  * YYYY-MM-DD HH:MM:SS  [[file:WDIR][WDIR]]  DESC
;;
;; followed by the output of shell command:
;;
;;  git branch && git status
;;
;; (in WDIR) indented by two spaces (nice for Emacs Org mode).
;; YYYY et al represent the modification time (localtime).  DESC
;; is the output of "git describe", if applicable, or "-" otherwise.

;;; Code:

(define-module (ttn-do hmm)
  #:export (main)
  #:use-module ((ttn-do zzz banalities) #:select (check-hv
                                                  qop<-args))
  #:use-module ((ttn-do zzz filesystem) #:select (expand-file-name
                                                  with-cwd))
  #:use-module ((ttn-do zzz personally) #:select (FE fs fso
                                                     forms<-port))
  #:use-module ((srfi srfi-13) #:select (string-drop-right)))

(define org-ref (make-object-property))

(define (wd<- filename)
  (string-drop-right filename 5))

(define (repos-list filename)
  (let ((root (expand-file-name (dirname filename))))

    (define (norm symbol)
      (let* ((reldir (symbol->string symbol))
             (dir (expand-file-name reldir root)))
        (set! (org-ref dir)
              (let ((wd (wd<- reldir)))
                (fs "[[file:~A][~A]]"
                    wd wd)))
        (cons (stat:mtime (stat (in-vicinity dir "HEAD")))
              dir)))

    (map norm (call-with-input-file filename
                forms<-port))))

(define (expound mtime dir)
  ;; This mixes port I/O and ‘system’, hence the ‘force-output’.
  ;; TODO: Use ‘(ttn-do zzz subprocess)’ facilities.
  (with-cwd (wd<- dir)
    ;; NB: no trailing ‘~%’; leave that to "git describe"
    (fso "~%* ~A  ~A  "
         (strftime "%F %T" (localtime mtime))
         (org-ref dir))
    (force-output)
    (system "git describe 2>/dev/null || echo '-'")
    (system "{ git branch && git status ; } | sed 's/^/  /'")
    (force-output)))

(define (main/qop qop)
  (let ((ls (sort (repos-list
                   (or (false-if-exception (car (qop '())))
                       (fs ".~A.hmm.list"
                           (or (getenv "USER")
                               (getenv "LOGNAME")
                               'nobody))))
                  (lambda (a b)
                    (> (car a)
                       (car b))))))
    (FE (map car ls)
        (map cdr ls)
        expound)))

(define (main args)
  (check-hv args '((package . "ttn-do")
                   (version . "1.0")
                   ;; 1.0  -- initial release
                   (help . commentary)))
  (main/qop
   (qop<-args
    args '())))

;;; hmm ends here
