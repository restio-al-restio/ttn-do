#!/bin/sh
exec ${GUILE-guile} -e '(ttn-do xml2sexp)' -s $0 "$@" # -*-scheme-*-
!#
;;; xml2sexp

;; Copyright (C) 2007, 2008, 2009, 2010, 2011,
;;   2012, 2013, 2014, 2021 Thien-Thi Nguyen
;;
;; This file is part of ttn-do, released under the terms of the
;; GNU General Public License as published by the Free Software
;; Foundation; either version 3, or (at your option) any later
;; version.  There is NO WARRANTY.  See file COPYING for details.

;;; Commentary:

;; Usage: xml2sexp [options] [XMLFILE...]
;;
;; Pretty-print contents of XMLFILE... to stdout.
;; If XMLFILE is "-" or omitted, read from standard input.
;; If XMLFILE ends with ".gz", filter it through "gzip -dc" first.
;; The contents are read as xml and written as a sexp.
;;
;; Options:
;; -f, --name-format [FMT] -- do (simple-format cep FMT filename) and
;;                            (newline cep), where cep is the current
;;                            error port, prior to the normal output for
;;                            each XMLFILE; disabled if from stdin
;;
;; -c, --combine-chardata  -- combine contiguous ‘character-data’ nodes;
;;                            done prior to ‘-w’ processing
;;
;; -w, --keep-whitespace   -- do NOT ignore ‘character-data’ nodes where
;;                            all characters are in ‘char-set:whitespace’
;;
;; -e, --empty STRING      -- suppress "XML_ERROR_NO_ELEMENTS" error
;;                            on empty files; display STRING followed
;;                            by newline, instead
;;
;; -x, --omit NODETYPE     -- ignore NODETYPE, one of:
;;                              character-data
;;                              comment
;;                            this option may be specified multiply
;;
;; -m, --map MAPFILE       -- Read external entity reference map from
;;                            MAPFILE [~/.ttn-do/xml2sexp.d/map]
;;
;; -s, --sxml N            -- output SXML N normal form, where N
;;                            is one of 0, 1, 2, 3 (EXPERIMENTAL)
;;
;; External entity references are resolved using the ‘system-id’
;; portion only.  If ‘system-id’ starts with "http://", then fail.
;; (Support for download / caching these is not yet implemented.)
;; If ‘system-id’ names a file in the current directory, open it.
;; If MAPFILE exists, ‘read’ it to map the ‘system-id’ to a local
;; filename; open it.  Each entry in MAPFILE looks something like:
;;
;; ("http://www.gnu.org/software/texinfo/dtd/4.13/texinfo.dtd"
;;  . "~/tmp/texinfo.dtd")
;;
;; In other words: (SYSTEM-ID . LOCAL-FILENAME), where both
;; SYSTEM-ID and LOCAL-FILENAME are normal Scheme strings.
;;
;; On failure, display message w/ all unresolved reference portions
;; (context, base, system-id ,public-id) to stderr and exit failurefully.

;;; Code:

(define-module (ttn-do xml2sexp)
  #:export (main xml2sexp)
  #:use-module ((ttn-do zzz banalities) #:select (check-hv
                                                  decry
                                                  qop<-args))
  #:use-module ((srfi srfi-13) #:select (string-suffix?
                                         string-prefix?
                                         string-concatenate-reverse
                                         string-every))
  #:use-module ((srfi srfi-14) #:select (char-set:whitespace))
  #:use-module ((ice-9 rdelim) #:select (write-line))
  #:use-module ((ice-9 popen) #:select (open-input-pipe))
  #:use-module ((ttn-do pp) #:select (pp))
  #:use-module ((ttn-do zzz personally) #:select (FE fs fse))
  #:use-module ((ttn-do zzz filesystem) #:select (expand-file-name))
  #:use-module ((ttn-do zzz 0gx forms-from) #:select (forms<-port))
  #:use-module ((mixp expat) #:select (parser-create
                                       set-param-entity-parsing
                                       hset!))
  #:use-module ((mixp utils) #:select (xml->tree)))

(define (open-input-file-if-exists filename)
  (let ((filename (expand-file-name filename)))
    (and (file-exists? filename)
         (open-input-file filename))))

(define BOSS (basename (car (command-line))))

(define (parser qop)
  (let ((eemap (or (qop 'map)
                   (and (string-prefix? "xml2sexp" BOSS)
                        "~/.ttn-do/xml2sexp.d/map")))
        (p (parser-create)))

    (define (external-entity-ref context base system-id public-id)
      (and (string? eemap)
           (set! eemap (false-if-exception
                        (forms<-port (open-input-file-if-exists
                                      (expand-file-name eemap))))))
      (cond ((and (not (string-prefix? "http://" system-id))
                  (open-input-file-if-exists system-id)))
            ((and (pair? eemap)
                  (assoc-ref eemap system-id))
             => open-input-file-if-exists)
            (else
             (fse "~A: cannot open external entity ref~%" BOSS)
             (FE `((context ,context)
                   (base ,base)
                   (system-id ,system-id)
                   (public-id ,public-id))
                 (lambda (x)
                   (apply fse " (~A) ~S~%" x)))
             (exit #f))))

    (cond (eemap
           (set-param-entity-parsing p 'XML_PARAM_ENTITY_PARSING_ALWAYS)
           (hset! p `((external-entity-ref . ,external-entity-ref)))))
    p))

(define (combine-character-data-children node)
  (let ((results '()))

    (define (result x)
      (set! results (cons x results)))

    (let loop ((parts #f) (ls (cddr node)))

      (define (squeeze!)
        (and parts
             (result (list 'character-data (string-concatenate-reverse
                                            parts)))))

      (if (null? ls)
          (begin (squeeze!)
                 (reverse! results))
          (let ((one (car ls)))
            (case (car one)
              ((character-data)
               (loop (cons (cadr one) (or parts '()))
                     (cdr ls)))
              (else
               (squeeze!)
               (result one)
               (loop #f (cdr ls)))))))))

(define (crunch qop port)
  (let* ((omit (or (qop 'omit (lambda (v) (map string->symbol v)))
                   '()))
         (sxml (qop 'sxml string->number))
         (attribute-association (if sxml list cons))
         (attribute-collect (case sxml
                              ((#f) (lambda (acc attr)
                                      (acc attr)))
                              ((0 1) (lambda (acc attr)
                                       (or (null? attr)
                                           (acc (cons '@ attr)))))
                              ((2 3) (lambda (acc attr)
                                       (acc (cons '@ attr))))
                              (else (decry "bad sxml normal form: ~S" sxml))))
         (no-comment (or (memq 'comment omit)
                         (and sxml (<= 2 sxml))))
         ;; TODO: SXML 2NF also specifies ‘no-entity’.
         (no-character-data (memq 'character-data omit))
         (no-s (if (qop 'keep-whitespace)
                   (lambda (s)
                     #f)
                   (lambda (s)
                     (string-every char-set:whitespace s)))))

    (define smooth
      (if (or (qop 'combine-chardata)
              (eq? 3 sxml))
          combine-character-data-children
          cddr))

    (define (crunch-1 form)
      (let ((so-far '()))

        (define (acc x)
          (set! so-far (cons x so-far)))

        (let loop ((ls form))
          (if (null? ls)
              (reverse! so-far)         ; rv
              (let ((head (car ls))
                    (tail (cdr ls)))
                (or (pair? head)
                    (decry "(not (pair? head)) !!! ~S" head))
                (and (pair? (car head))
                     (decry "(pair? (car head)) !!! ~S" head))
                (case (car head)
                  ((element)
                   (let* ((x (cadr head))
                          (name (string->symbol (car x)))
                          (attrs (map (lambda (pair)
                                        (attribute-association
                                         (string->symbol (car pair))
                                         (cdr pair)))
                                      (cadr x))))
                     (acc name)
                     (attribute-collect acc attrs)
                     (FE (map crunch-1 (map list (smooth head)))
                         (lambda (res)
                           (cond ((null? res))
                                 ((and-map string? res)
                                  (FE res acc))
                                 (else
                                  (acc res)))))))
                  ((character-data)
                   (or no-character-data
                       (let ((s (cadr head)))
                         (or (no-s s)
                             (acc s)))))
                  ((comment)
                   (or no-comment
                       (acc head)))
                  (else
                   (acc head)))
                (loop tail))))))

    (crunch-1 (xml->tree port (parser qop)))))

;; Read XML from @var{port} and return a sexp.
;;
;; @var{options} is an alist with symbolic keys @code{keep-whitespace},
;; @code{empty}, @code{omit}; these control behavior identically to the
;; command-line options of the same name described above, with one
;; exception: there is no default mapfile (in which case handling for
;; external entity references is not enabled).  Also, the value for
;; @code{keep-whitespace} is a boolean, whereas that for @code{omit} is a
;; list of symbols.
;;
(define (xml2sexp options port)
  ;; FIXME: ‘crunch’ takes ‘qop’, which expects strings as the alist
  ;; values, requiring (suboptimal) normalization of ‘options’.

  (define (norm x)
    (case (car x)
      ((sxml) (cons (car x) (number->string (cdr x))))
      ((omit) (cons (car x) (map symbol->string (cdr x))))
      (else x)))

  (let ((normalized (map norm options)))

    (define (fake-qop k . proc)
      (cond ((assq-ref normalized k)
             => (lambda (v)
                  (if (not (null? proc))
                      ((car proc) v)
                      v)))
            (else #f)))

    (crunch fake-qop port)))

(define (main/qop qop)
  (let ((pre (or (qop 'name-format
                      (lambda (fmt)
                        (set! fmt (string-append fmt "~%"))
                        (lambda (filename)
                          (fse fmt filename))))
                 identity))
        (all (qop '())))

    (define (one filename)
      (let* ((stdin? (string=? "-" filename))
             (port (cond (stdin?
                          (current-input-port))
                         ((string-suffix? ".gz" filename)
                          (open-input-pipe (fs "gzip -dc ~S" filename)))
                         (else
                          (open-input-file filename)))))
        (or stdin? (pre filename))
        (pp (crunch qop port))
        (force-output)
        (or stdin? (close-port port))))

    (or (pair? all)
        (set! all (list "-")))
    (or (qop 'empty
             (lambda (empty-string)
               (FE all (lambda (filename)
                         (catch 'XML_ERROR_NO_ELEMENTS
                                (lambda () (one filename))
                                (lambda ignored
                                  (write-line empty-string)
                                  (force-output)))))))
        (FE all one))))

(define (main args)
  (check-hv args '((package . "ttn-do")
                   (version . "1.8")
                   ;; 1.8  -- read from stdin if no XMLFILE
                   ;; 1.7  -- default MAPFILE based on argv[0]
                   ;; 1.6  -- add option ‘--map MAPFILE’;
                   ;;         add option ‘--combine-chardata’
                   ;;           (auto enabled for SXML NF3);
                   ;; 1.5  -- add EXPERIMENTAL ‘--sxml N’
                   ;; 1.4  -- bugfix: maintain elem attr order
                   ;; 1.3  -- export ‘xml2sexp’
                   ;; 1.2  -- handle stdin, gzipped input
                   ;; 1.1  -- add more callbacks; add ‘--omit X’
                   ;; 1.0  -- initial revision
                   (help . commentary)))
  (main/qop
   (qop<-args
    args '((name-format (single-char #\f) (value #t))
           (map (single-char #\m) (value #t))
           (combine-chardata (single-char #\c))
           (keep-whitespace (single-char #\w))
           (omit (single-char #\x) (value #t) (merge-multiple? #t))
           (empty (single-char #\e) (value #t))
           (sxml (single-char #\s) (value #t))))))

;;; xml2sexp ends here
