#!/bin/sh
exec ${GUILE-guile} -e '(ttn-do phone)' -s $0 "$@" # -*-scheme-*-
!#
;;; phone

;; Copyright (C) 2001, 2003, 2004, 2005, 2007, 2009, 2010,
;;   2011, 2021 Thien-Thi Nguyen
;;
;; This file is part of ttn-do, released under the terms of the
;; GNU General Public License as published by the Free Software
;; Foundation; either version 3, or (at your option) any later
;; version.  There is NO WARRANTY.  See file COPYING for details.

;;; Commentary:

;; Usage: phone REGEXP
;;
;; Grep for REGEXP in ~/.phone database.

;;; Code:

(define-module (ttn-do phone)
  #:export (main)
  #:use-module ((ttn-do zzz banalities) #:select (check-hv
                                                  decry))
  #:use-module ((ttn-do mogrify) #:select (editing-buffer)))

(define (main args)
  (check-hv args '((package . "ttn-do")
                   (version . "2.0")
                   ;; 2.0  -- fail if ~/.phone cannot be read
                   ;; 1.0  -- initial release
                   (help . commentary)))
  (and (null? (cdr args))
       (decry "No regexp specified"))
  (editing-buffer #t
    (insert-file-contents "~/.phone")
    (keep-lines (cadr args))
    ;; Unfortunately, ‘keep-lines’ does not preserve eob ‘#\newline’.
    (and (< (point-min) (goto-char (point-max)))
         (not (char=? #\newline (char-before)))
         (insert #\newline))
    (write-to-port #t))
  #t)

;;; phone ends here
