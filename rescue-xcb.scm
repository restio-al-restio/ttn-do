#!/bin/sh
exec ${GUILE-guile} -e '(ttn-do rescue-xcb)' -s $0 "$@" # -*- scheme -*-
!#
;;; rescue-xcb --- convert X protocol descriptions from .xml to .eaab

;; Copyright (C) 2007, 2010, 2011, 2019-2021 Thien-Thi Nguyen
;;
;; This file is part of ttn-do, released under the terms of the
;; GNU General Public License as published by the Free Software
;; Foundation; either version 3, or (at your option) any later
;; version.  There is NO WARRANTY.  See file COPYING for details.

;;; Commentary:

;; Usage: rescue-xcb INPUT-DIR OUTPUT-DIR
;;
;; Read INPUT-DIR/*.xml and write OUTPUT-DIR/*.eaab.
;; Filenames undergo s/_/-/g.

;;; Code:

(define-module (ttn-do rescue-xcb)
  #:export (main)
  #:use-module ((ttn-do zzz 0gx read-string) #:select (read-string))
  #:use-module ((ttn-do xml2sexp) #:select (xml2sexp))
  #:use-module ((ttn-do zzz banalities) #:select (check-hv
                                                  decry))
  #:use-module ((ttn-do zzz filesystem) #:select (directory-vicinity
                                                  dir-exists?
                                                  filtered-files-in-vicinity))
  #:use-module ((ttn-do zzz personally) #:select (FE fs fso))
  #:use-module ((srfi srfi-1) #:select (filter-map))
  #:use-module ((srfi srfi-13) #:select (string-prefix?
                                         string-suffix?
                                         string-map))
  #:use-module ((ttn-do pp) #:select (pp))
  #:use-module ((ttn-do mogrify) #:select (filename:
                                           editing-buffer)))

(cond-expand
 (guile-2
  (use-modules
   (ice-9 curried-definitions)))
 (else #f))

(define (under-to-hyphen string)
  (string-map (lambda (c)
                (if (char=? #\_ c)
                    #\-
                    c))
              string))

(define (filter-eaab undesirable eaab)

  (define (eaab? x)
    (and (pair? x)
         (symbol? (car x))
         (pair? (cdr x))))

  (define (one eaab)
    (cons* (car eaab) (cadr eaab)
           (filter-map (lambda (child)
                         (if (eaab? child)
                             (and (not (memq (car child) undesirable))
                                  (one child))
                             child))
                       (cddr eaab))))

  (one eaab))

(define ((process outdir) filename)
  (let ((under-outdir (directory-vicinity outdir))
        (eaab (fs "~A.eaab" (under-to-hyphen (basename filename ".xml")))))
    (editing-buffer #t
      (insert-file-contents filename)
      (search-forward "<!--\n")
      (delete-region (point-min) (point))
      (insert ";;; " eaab " (from " filename ")\n\n"
              ";; Copyright (C) "
              (strftime "%Y" (gmtime (current-time)))
              " Thien-Thi Nguyen\n")
      (while (not (looking-at "-->"))
        (insert ";; ")
        (forward-line 1))
      (delete-region (point) (point-max))
      (insert "\n")
      (let ((sexp (call-with-input-file filename
                    (lambda (port)
                      (xml2sexp '((combine-chardata)
                                  (omit comment))
                                port)))))
        (define (walk x)
          (cond ((null? x)
                 x)
                ((pair? x)
                 (cons (walk (car x))
                       (walk (cdr x))))
                ((string? x)
                 (if (string-prefix? "0x" x)
                     (string->number (substring x 2) 16)
                     ;; Don't use ‘string->symbol’; Guile 1.8.7 does:
                     ;;   (string->symbol "0") => #{0}#
                     (read-string (under-to-hyphen x))))
                (else x)))
        (pp (walk (filter-eaab '(doc description) sexp))
            (buffer-port)))
      (insert "\n")
      (insert ";;; " eaab " ends here\n")
      (set! (filename: (current-buffer)) (under-outdir eaab))
      (save-buffer)
      (fso "wrote ~A~%" (filename: (current-buffer))))))

(define (check dir)
  (if (dir-exists? dir)
      dir
      (decry "not a directory: ~A" dir)))

(define (main args)
  (check-hv args '((package . "ttn-do")
                   (version . "1.4")
                   ;; 1.4  -- start to adapt for newer XCB versions
                   ;; 1.3  -- Guile 2 slog
                   ;; 1.2  -- handle read-only input files
                   ;; 1.1  -- rewrite in Scheme
                   ;; 1.0  -- initial release
                   (help . commentary)))
  (or (= 3 (length args))
      (decry "missing args (try --help)"))
  (let ((idir (check (cadr args)))
        (odir (check (caddr args))))
    (FE (filtered-files-in-vicinity idir (lambda (filename)
                                           (string-suffix? ".xml" filename))
                                    #:filter-prefixed)
        (process odir))))

;;; rescue-xcb ends here
