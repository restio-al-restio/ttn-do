#!/bin/sh
exec ${GUILE-guile} -e '(ttn-do utf8-dump)' -s $0 "$@" # -*- scheme -*-
!#
;;; utf8-dump.scm --- excercise ‘(ttn-do zzz emacsdream)’

;; Copyright (C) 2017 Thien-Thi Nguyen
;;
;; This file is part of ttn-do, released under the terms of the
;; GNU General Public License as published by the Free Software
;; Foundation; either version 3, or (at your option) any later
;; version.  There is NO WARRANTY.  See file COPYING for details.

;;; Commentary:

;; Usage: utf8-dump [options] [FILE | -]
;;
;; Display a running analysis of the (presumed) UTF-8 byte stream from
;; FILE (or stdin if FILE is "-" or omitted), and a summary when done.
;;
;; Options:
;;  -u, --uni-data FILENAME  -- Consult FILENAME for Unicode names
;;  -A, --omit-ascii         -- Don't display values < 128
;;
;; Output lines have form:
;;
;;   V-COUNT  B-OFS  B-COUNT  U+N  (BYTE...)  [CHAR  [- NAME]]
;;
;; Columns are separated by one or more space (U+20) characaters.
;;
;; - V-COUNT is the decoded value count, starting with 1.
;;
;; - B-OFS is the byte stream offset, starting with 0.
;;
;; - B-COUNT is how many bytes the encoded value occupies
;;   (thus, this line B-OFS + B-COUNT => next line B-OFS).
;;
;; - In ‘U+N’, the ‘U+’ is literal, while N is four (or more)
;;   hex digits that indicate the decoded value.
;;
;; - BYTE... is one or more two-hex-digit, space-separated
;;   values in the range [0,255] (i.e., ‘00’ through ‘ff’).
;;
;; - If N is less than 128, CHAR is its Scheme character object as
;;   produced by ‘integer->char’, otherwise this column is omitted.
;;
;; - If given ‘--uni-data FILENAME’ -- typically UnicodeData.txt:
;;
;;      0000;<control>;Cc;0;BN;;;;;N;NULL;;;;
;;      0001;<control>;Cc;0;BN;;;;;N;START OF HEADING;;;;
;;      0002;<control>;Cc;0;BN;;;;;N;START OF TEXT;;;;
;;      ...
;;
;;   then NAME is the Unicode name, e.g., "LATIN SMALL LETTER U".
;;   If NAME is "<control>" it is appended with a parenthetical
;;   "second name".  If no second name is defined, use U+N instead.
;;
;; On EOF, display two lines:
;;
;;   eof: B-OFS
;;   -----
;;
;; On invalid bytes, scan forward until a valid lead byte is found and
;; display the line:
;;
;;   B-OFS   - (BAD...) [(IGNORED...)]
;;
;; where B-OFS is aligned with the normal output B-OFS, the "-" is
;; literal, and (BAD...) is aligned with the normal output (BYTE...).
;; Each BAD is a byte that failed validation.  If stream resync ignored
;; some bytes, those are in (IGNORED...), otherwise that info is omitted.
;;
;; Finally, display the line:
;;
;;   eof: (B-OFS . V-COUNT)
;;
;; where B-OFS and V-COUNT are the finishing values.  If the stream is
;; from a file, B-OFS should be identical to the file size (in bytes).
;;
;; Example:
;;  $ printf '\342\200\230u\342\200\230\n' \
;;     | utf8-dump -u ~/tmp/UnicodeData-9.0.0.txt
;;         1       0  3  U+2018 (e2 80 98) - LEFT SINGLE QUOTATION MARK
;;         2       3  1  U+0075 (75) #\u - LATIN SMALL LETTER U
;;         3       4  3  U+2019 (e2 80 99) - RIGHT SINGLE QUOTATION MARK
;;         4       7  1  U+000a (0a) #\newline - <control> (LINE FEED (LF))
;;  eof: 8
;;  -----
;;  eof: (8 . 4)

;;; Code:

(define-module (ttn-do utf8-dump)
  #:export (main)
  ;; the autoloads are for ‘set-up-UNICODE-NAMES!’
  #:autoload (ice-9 rdelim) (read-line)
  #:autoload (srfi srfi-13) (string-index
                             string-suffix?)
  #:use-module (ice-9 format)
  #:use-module ((srfi srfi-1) #:select (car+cdr))
  #:use-module ((srfi srfi-11) #:select (let-values))
  #:use-module ((ttn-do zzz banalities) #:select (check-hv
                                                  qop<-args))
  #:use-module ((ttn-do zzz personally) #:select (fs fso))
  #:use-module ((ttn-do zzz emacsdream) #:select (utf8-reader)))

(define UNICODE-NAMES #f)

(define (FS  s . args) (apply format #f s args))
(define (FSO s . args) (apply format #t s args))

(define (hex2 n) (FS "~2,'0X" n))
(define (Uhex n) (FS "U+~4,'0X" n))

(define (set-up-UNICODE-NAMES! filename)
  (let ((port (open-input-file filename))
        (ht (make-hash-table)))

    (define (parse-line line)
      (let ((positions
             (let loop ((rv (list -1)))
               (cond ((string-index line #\; (1+ (car rv)))
                      => (lambda (pos)
                           (loop (cons pos rv))))
                     (else
                      (reverse! (cons (string-length line)
                                      rv)))))))

        (define (lref idx)
          (let* ((at (list-tail positions idx))
                 (beg (1+ (car at)))
                 (end (cadr at)))
            (and (not (= beg end))
                 (substring line beg end))))

        (values
         (string->number (lref 0) 16)
         (lref 1)
         (lref 10))))

    (let loop ()
      (let ((line (read-line port)))
        (or (eof-object? line)
            (let-values (((uval name n2) (parse-line line)))
              (or (string-suffix? ", First>" name)
                  (string-suffix? ", Last>" name)
                  (hashq-set! ht uval
                              (if (string=? "<control>" name)
                                  (fs "~A (~A)"
                                      name
                                      (or n2 (Uhex uval)))
                                  name)))
              (loop)))))
    (close-port port)
    (set! UNICODE-NAMES ht)))

(define N-RAW-BYTES 6) ;; "length six" (info "(ttn-do) zzz emacsdream")

(define pretty-proc
  (let ((idx (apply vector (map iota (iota N-RAW-BYTES))))
        (cache (make-hash-table)))

    ;; pretty-proc
    (lambda (raw-bytes)

      (define (compute+jam! uval b-count)
        (let ((s (string-append
                  (fs "~A" (map (lambda (i)
                                  (hex2 (vector-ref raw-bytes i)))
                                (vector-ref idx b-count)))
                  (if (> 128 uval)
                      (fs " ~S" (integer->char uval))
                      "")
                  (cond ((and UNICODE-NAMES
                              (hashq-ref UNICODE-NAMES
                                         uval))
                         => (lambda (uni-name)
                              (fs " - ~A" uni-name)))
                        (else "")))))
          (hashq-set! cache uval s)
          s))

      ;; This is probably little gain (as well as uncouth -- see "ugh").
      ;;
      ;;- ;; prime the cache (once)
      ;;- (or (hashq-ref cache 0)
      ;;-     (do ((i 0 (1+ i)))
      ;;-         ((= 256 i))
      ;;-       (vector-set! y 0 i)         ; ugh
      ;;-       (compute+jam! i 1)))

      ;; rv
      (lambda (uval b-count)
        (or (hashq-ref cache uval)
            (compute+jam! uval b-count))))))

(define (scan u8rd ignorable)
  (let ((w (u8rd #:posbox))
        (x (u8rd #:rvbox))
        (y (u8rd #:raw-bytes)))

    (define pretty (pretty-proc y))

    (let loop ()
      (let ((starting-b-ofs (car w)))
        (and (catch 'invalid-utf8 u8rd
                    (lambda (key . bad-bytes)
                      (let ((ign (cdr (u8rd #:sync!))))
                        (set-car! x #f)
                        (set-cdr! x (FS "~16D ~10,@A ~A ~A~%"
                                        starting-b-ofs
                                        '-
                                        (map hex2 bad-bytes)
                                        (if (null? ign)
                                            ""
                                            (map hex2 ign)))))
                      #t))
             (let-values (((b-count uval) (car+cdr x)))
               (if b-count
                   (or (ignorable uval)
                       (FSO "~8D ~7D  ~A  ~A ~A~%"
                            (cdr w)     ; i.e., ‘v-count’
                            starting-b-ofs
                            b-count
                            (Uhex uval)
                            (pretty uval b-count)))
                   (display uval))
               (loop)))))))

(define (main/qop qop)
  (qop 'uni-data set-up-UNICODE-NAMES!)
  (let* ((args (qop '()))
         (filename (and (pair? args)
                        (car args)))
         (prod (utf8-reader (if (and filename (not (string=? "-" filename)))
                                (open-input-file filename)
                                (current-input-port))))
         (ignorable (if (qop 'omit-ascii)
                        (lambda (uval)
                          (> 128 uval))
                        (lambda (uval)
                          #f))))
    (scan prod ignorable)
    (fso "eof: ~A~%" (car (prod #:posbox)))
    (fso "-----~%")
    (let ((last (utf8-reader (prod #:ubs) (prod #:posbox))))
      (scan last ignorable)
      (fso "eof: ~A~%" (last #:posbox)))))

(define (main args)
  (check-hv args '((package . "ttn-do")
                   (version . "1.0")
                   ;; 1.0  -- initial release
                   (help . commentary)))
  (main/qop
   (qop<-args
    args '((uni-data (single-char #\u) (value #t))
           (omit-ascii (single-char #\A))))))

;;; utf8-dump.scm ends here
