#!/bin/sh
exec ${GUILE-guile} -e '(ttn-do xdpyinfo)' -s $0 "$@" # -*- scheme -*-
!#
;;; xdpyinfo --- describe the current X display

;; Copyright (C) 2020 Thien-Thi Nguyen
;;
;; This file is part of ttn-do, released under the terms of the
;; GNU General Public License as published by the Free Software
;; Foundation; either version 3, or (at your option) any later
;; version.  There is NO WARRANTY.  See file COPYING for details.

;;; Commentary:

;; Usage: xdpyinfo
;;
;; Display information about the current display to stdout.
;; For extensions, see "ttn-do x-list-extensions", or the
;; original xdpyinfo(1) (implemented in C), part of X.Org.

;;; Code:

(define-module (ttn-do xdpyinfo)
  #:export (main)
  #:use-module ((srfi srfi-13) #:select (string-join))
  #:use-module ((ttn-do zzz banalities) #:select (check-hv
                                                  qop<-args))
  #:use-module ((ttn-do zzz personally) #:select (FE fs fso))
  #:use-module ((ttn-do zzz x-protocol) #:select ((-internal-data . zxi)
                                                  (-x . zx-x)))
  #:use-module ((ttn-do zzz x-umbrages) #:select (connection)))

(define ENUMS (zxi 'ENUMS))

(define (rassq-ref-proc alist)
  (and (or (symbol? alist)
           (symbol? (car alist)))
       (set! alist (hash-ref ENUMS alist)))
  (let ((rev (map cons
                  (map cdr alist)
                  (map car alist))))
    ;; rv
    (lambda (number)
      (or (assq-ref rev number)
          number))))

(define InputFocus   (rassq-ref-proc 'InputFocus))
(define BackingStore (rassq-ref-proc 'BackingStore))
(define VisualClass  (rassq-ref-proc 'VisualClass))
(define RevertTo     (rassq-ref-proc '((RevertToNone . 0)
                                       (RevertToPointerRoot . 1)
                                       (RevertToParent . 2))))

(define (getter alist)
  (lambda (key)
    (assq-ref alist key)))

(define (int/ num den)
  (inexact->exact (truncate (/ (* 1.0 num) den))))

(define (hex n)
  (fs "0x~A" (number->string n 16)))

(define (describe-visual visual depth)

  (define v (getter visual))

  (fso "  visual:~%")
  (fso "    visual id:    ~A~%" (hex (v 'visual-id)))
  (fso "    class:    ~A~%" (VisualClass (v 'class)))
  (let ((depth (zx-x 'depth depth)))
    (fso "    depth:    ~A plane~A~%"
         depth (if (= 1 depth)
                   "" "s")))
  (fso "    available colormap entries:    ~A per subfield~%"
       (v 'colormap-entries))
  (fso "    red, green, blue masks:    ~A, ~A, ~A~%"
       (hex (v 'red-mask))
       (hex (v 'green-mask))
       (hex (v 'blue-mask)))
  (fso "    significant bits in color specification:    ~A bits~%"
       (v 'bits-per-rgb-value)))

(define (describe q setup extensions)

  (define top (getter setup))

  (fso "version number:    ~A.~A~%"
       (top 'protocol-major-version)
       (top 'protocol-minor-version))
  (top 'length)                         ; ?
  (fso "vendor string:    ~A~%" (top 'vendor))
  (let ((relno (top 'release-number)))
    (fso "vendor release number:    ~A~%" relno)
    ;; NB: xdpyinfo.c also groks versioning for XFree86, DMX.
    (fso "X.Org version: ~A.~A.~A~%"
         (int/ relno 10000000)
         (modulo (int/ relno 100000) 100)
         (modulo (int/ relno 1000) 100)))
  (fso "maximum request size:  ~A bytes~%"
       (* 256 (top 'maximum-request-length)))
  (fso "motion buffer size:  ~A~%" (top 'motion-buffer-size))
  (fso "bitmap unit, bit order, padding:    ~A, ~A, ~A~%"
       (top 'bitmap-format-scanline-unit)
       (if (zero? (top 'bitmap-format-bit-order))
           'LSBFirst 'MSBFirst)
       (top 'bitmap-format-scanline-pad))
  (fso "image byte order:    ~A~%"
       (if (zero? (top 'image-byte-order))
           'LSBFirst 'MSBFirst))
  (let ((pixmap-formats (top 'pixmap-formats)))
    (fso "number of supported pixmap formats:    ~A~%"
         (vector-length pixmap-formats))
    (fso "supported pixmap formats:~%")
    (FE (vector->list pixmap-formats)
        (lambda (pmf)

          (define pm (getter pmf))

          (fso "    depth ~A, bits_per_pixel ~A, scanline_pad ~A~%"
               (pm 'depth)
               (pm 'bits-per-pixel)
               (pm 'scanline-pad)))))
  (fso "keycode range:    minimum ~A, maximum ~A~%"
       (top 'min-keycode)
       (top 'max-keycode))
  (let* ((resp (q 'GetInputFocus))
         (win (InputFocus (zx-x 'focus resp)))
         (revert-to (RevertTo (zx-x 'revert-to resp))))
    (fso "focus:  ~A~%"
         (case win
           ((None PointerRoot) win)
           (else (fs "window ~A, revert to ~A"
                     (hex win)
                     (case revert-to
                       ((RevertToNone RevertToPointerRoot
                                      RevertToParent)
                        (substring (symbol->string revert-to)
                                   (string-length "RevertTo")))
                       (else "<ERROR>")))))))
  (fso "number of extensions:    ~A~%" (vector-length extensions))
  (FE (sort (vector->list extensions) string<?)
      (lambda (name)
        (fso "    ~A~%" name)))
  (let* ((roots (top 'roots))
         (count (vector-length roots)))
    (fso "default screen number:    ~A~%"
         (if (= 1 count)
             0
             "???"))
    (fso "number of screens:    ~A~%" count)
    (FE (vector->list roots) (iota count)
        (lambda (root idx)

          (define r (getter root))

          (fso "~%screen #~A:~%" idx)
          (let ((wp (r 'width-in-pixels))
                (hp (r 'height-in-pixels))
                (wm (r 'width-in-millimeters))
                (hm (r 'height-in-millimeters))
                (depths (vector->list (r 'allowed-depths))))
            (fso "  dimensions:    ~Ax~A pixels (~Ax~A millimeters)~%"
                 wp hp wm hm)
            (fso "  resolution:    ~Ax~A dots per inch~%"
                 (int/ wp (/ wm 25.4))
                 (int/ hp (/ hm 25.4)))
            (fso "  depths (~A):    ~A~%"
                 (r 'allowed-depths-len)
                 (string-join (map (lambda (depth)
                                     (number->string (zx-x 'depth depth)))
                                   depths)
                              ", "))
            (fso "  root window id:    ~A~%"
                 (hex (r 'root)))
            (let ((root-depth (r 'root-depth)))
              (fso "  depth of root window:    ~A plane~A~%"
                   root-depth (if (= 1 root-depth)
                                  "" "s")))
            (fso "  number of colormaps:    minimum ~A, maximum ~A~%"
                 (r 'min-installed-maps) (r 'max-installed-maps))
            (fso "  default colormap:    ~A~%"
                 (hex (r 'default-colormap)))
            (fso "  default number of colormap cells:    ~A~%"
                 "???")
            (fso "  preallocated pixels:    black ~A, white ~A~%"
                 (r 'black-pixel) (r 'white-pixel))
            (fso "  options:    backing-store ~A, save-unders ~A~%"
                 (case (BackingStore (r 'backing-stores))
                   ((NotUseful) "NO")
                   ((WhenMapped) "WHEN MAPPED")
                   ((Always) "YES")
                   (else "<ERROR>"))
                 (if (zero? (r 'save-unders))
                     "NO" "YES"))
            (let* ((CursorShape 0)      ; X.h line 632
                   (resp (q 'QueryBestSize
                            #:class    CursorShape
                            #:drawable (r 'root)
                            #:width    65535
                            #:height   65535))
                   (w (zx-x 'width  resp))
                   (h (zx-x 'height resp)))
              (fso "  largest cursor:    ~A~%"
                   (if (= 65535 w h)
                       "unlimited"
                       (fs "~Ax~A" w h))))
            (fso "  current input event mask:    ~A~%" 'TODO)
            (fso "  number of visuals:    ~A~%"
                 (apply + (map (lambda (depth)
                                 (zx-x 'visuals-len depth))
                               depths)))
            (fso "  default visual id:  ~A~%"
                 (hex (zx-x '(visuals 0 visual-id) (car depths))))
            (FE depths
                (lambda (depth)
                  (FE (vector->list (zx-x 'visuals depth))
                      (lambda (visual)
                        (describe-visual visual depth))))))))))

(define (main args)
  (check-hv args '((package . "ttn-do")
                   (version . "1.0")
                   (help . commentary)))
  (let ((qop (qop<-args args '())))
    (let* ((conn (connection))
           (q (conn #:q)))
      (fso "name of display:    ~A~%" (getenv "DISPLAY"))
      (describe q (conn #:setup) (zx-x 'names (q 'ListExtensions)))
      (conn #:bye))))

;;; xdpyinfo ends here
