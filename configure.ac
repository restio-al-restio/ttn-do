dnl configure.ac

dnl Copyright (C) 2000-2013, 2017, 2019-2021 Thien-Thi Nguyen
dnl
dnl This file is part of ttn-do, released under the terms of the
dnl GNU General Public License as published by the Free Software
dnl Foundation; either version 3, or (at your option) any later
dnl version.  There is NO WARRANTY.  See file COPYING for details.

AC_INIT([ttn-do],[432],[ttn@gnuvola.org])
AC_CONFIG_AUX_DIR([baux])
AC_CONFIG_MACRO_DIR([baux])
AC_CONFIG_HEADERS([zz/config.h])

# Make sure configuration doesn't auto-compile anything.
GUILE_AUTO_COMPILE=0
export GUILE_AUTO_COMPILE

dnl Don't quote the RHS of this assignment; the expansion
dnl of ‘m4_esyscmd(...)’ includes a trailing newline.
TAGISODATE=m4_esyscmd([git tag -n | sort -nr | sed 's/^[^ ]* *//;s/ .*//;q'])
AC_SUBST([TAGISODATE])

FOOL='Thien-Thi Nguyen'
AC_SUBST([FOOL])

SNUGGLE_MAINT_MODE_BASE([enable documentation-related build rules])
MAINT_MODE=$enable_maint_mode
AC_SUBST([MAINT_MODE])

SNUGGLE_SET_SOFIXFLAGS

#-----------------------------------------------------------------------------
# Checks for programs

AC_PATH_PROG([WHICH],[which],[:])

SNUGGLE_PROGS
SNUGGLE_GUILE_LIBSITE_DIR([ttndo])
SNUGGLE_GUILE_TOOLS_EXISTSP([ttndo_cv_mkmodcat],[make-module-catalog])
AS_IF([test xyes = x$ttndo_cv_mkmodcat],
      [mmc='${GUILE_TOOLS} make-module-catalog'],
      [mmc=:])
AC_SUBST([mmc])
SNUGGLE_MODULE_AVAILABLE([ice-9 optargs-kw])
AS_IF([test yes = "$guile_cv_have_mod_ice_9_optargs_kw"],
      [kludge=transform],
      [kludge=leave-be])
AC_SUBST([kludge])
SNUGGLE_CHECK_META_SWITCH_MINUS_E_STRING([guile_cv_dwms_ok])
AS_IF([test xyes = x"$guile_cv_dwms_ok"],
      [eshstyle=direct-with-meta-switch],
      [eshstyle=modern-sh-wrapper])
AC_SUBST([eshstyle])

SNUGGLE_MODULE_AVAILABLE([mixp utils])

AC_PROG_LN_S
AC_PROG_MAKE_SET
AC_PROG_MKDIR_P
AC_PROG_INSTALL

AC_CHECK_PROG([INSTALL_INFO], [install-info], [install-info],
              [@echo WARNING: NOT DOING: install-info])

#-----------------------------------------------------------------------------
# Set lispdir.

lispdir="${datadir}/emacs/site-lisp"
AC_SUBST([lispdir])

#-----------------------------------------------------------------------------
# Installation needs a GNU Make that handles $(foreach ...) and $(call ...).
AC_MSG_CHECKING([for GNU Make that handles ‘foreach’ and ‘call’])
sed 'y/DT/$\t/' > confmaketest.mk <<EOF
reverse = D(2) D(1)
all:
T@echo D(foreach x, a b c, jD(x)k) D(call reverse,a,b)
EOF
confmaketest=$(${MAKE-make} -f confmaketest.mk)
rm -f confmaketest.mk
case "$confmaketest" in
  ("jak jbk jck b a")
     AC_MSG_RESULT([yes (cool!)])
     ;;
  (*)
     AC_MSG_RESULT([no (bummer!)])
     AC_MSG_ERROR([try latest from: ftp://ftp.gnu.org/gnu/make/])
     ;;
esac

#-----------------------------------------------------------------------------
# zz

LT_PREREQ([2.2.6])
LT_INIT([dlopen disable-static])

SNUGGLE_FLAGS
SNUGGLE_CHECK_CLASSIC_HEADERS

SNUGGLE_GUILE_USER_PROVIDES([have_array_prototype],[array-prototype])

AS_IF([test xyes = "x$have_array_prototype"],[
AC_CACHE_CHECK([whether Guile's array-prototype DTRT],
  [guile_cv_array_prototype_works],[
  SNUGGLE_CHECK([guile_cv_array_prototype_works],[
    (exit (let ((proto (quote s)))
            (eq? proto (array-prototype
                        (list->uniform-vector
                         proto (list 1))))))
  ])
])
AS_IF([test xyes = "x$guile_cv_array_prototype_works"],,
 [AC_DEFINE([NEED_ARRAY_PROTOTYPE_KLUDGE], 1,
            [Define to 1 if you array-prototype does not DTRT.])])])

AS_IF([test xyes = x"$enable_maint_mode"],[
AC_CHECK_HEADERS([X11/keysymdef.h])
AS_IF([test xyes = x"$ac_cv_header_X11_keysymdef_h"],,
  [AC_MSG_ERROR([cannot build; install <X11/keysymdef.h>
 (try package ‘x11proto-dev’ or ‘x11proto-core-dev’ or ‘libx11-dev’)
 and try again])])
])

#-----------------------------------------------------------------------------
# Write it out.

AS_IF([test xno = x"$guile_cv_have_mod_mixp_utils"],
[AC_MSG_WARN([3rd-party module (mixp utils) not found])])

AC_CONFIG_FILES([
  baux/common.mk
  GNUmakefile
  ttn-do
  zz/GNUmakefile
  zzz/GNUmakefile
])

AC_CONFIG_FILES([baux/gbaux-do],[chmod +x baux/gbaux-do])

AC_OUTPUT

dnl configure.ac ends here
