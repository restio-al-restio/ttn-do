#!/bin/sh
exec ${GUILE-guile} -e '(ttn-do edit-script-header)' -s $0 "$@" # -*- scheme -*-
!#
;;; edit-script-header --- Munge the first few lines of a Scheme script

;; Copyright (C) 2003, 2004, 2007, 2008, 2011, 2012, 2017, 2021 Thien-Thi Nguyen
;;
;; This file is part of ttn-do, released under the terms of the
;; GNU General Public License as published by the Free Software
;; Foundation; either version 3, or (at your option) any later
;; version.  There is NO WARRANTY.  See file COPYING for details.

;;; Commentary:

;; Usage: edit-script-header [OPTIONS] FILE ...
;;
;; Grok and modify each FILE's header (hash-bang invocation sequence)
;; according to OPTIONS:
;;
;;  -o, --output OTHER  -- write to file OTHER instead of modifying FILE
;;  -g, --guile [GUILE] -- use GUILE as the interpreter
;;  -G, --GUILE-guile   -- use ${GUILE-guile} construct, implies -e
;;  -n, --no-act        -- do not do the edit, display parse results only
;;  -v, --verbose       -- display progress reports
;;  -b, --backup EXT    -- rename output file to fooEXT before writing
;;  -s, --style STYLE   -- convert output to use header STYLE (see below)
;;  -i, --installed     -- use installed guile as the interpreter
;;
;; By default, the name of the interpreter is constructed by combining
;; bindir (use "guile-tools guile-config info bindir" to see this value)
;; with the name "guile", for example: "/usr/local/bin/guile".  This name
;; is placed directly after the hash-bang "#!" on the first line followed
;; by a space and then the meta switch "\".  The second line has the rest
;; of the args, terminating with "-s" and a newline.  The third line has
;; a bang-hash "!#" only.  This is the ‘direct-with-meta-switch’ style.
;;
;; Simple edits (like changing the guile interpreter from /usr/bin/guile
;; to /usr/local/bin/guile, for example) are no problem.  Converting
;; between styles is somewhat more tricky.  Here is a list of the styles
;; that are recognized and supported (for -s option):
;;
;; * old-style-sh-wrapper
;;
;;   This is primarily for executable modules, and looks something like:
;;
;;   #!/bin/sh
;;   main='(module-ref (resolve-module '\''MODULE-NAME) '\'main')'
;;   exec ${GUILE-guile} -l $0 -c "(apply $main (cdr (command-line)))" "$@"
;;   !#
;;
;;   We call it "old" because there are now better ways to invoke ‘main’
;;   (see ‘modern-sh-wrapper’).  Converting scripts from and to this style
;;   requires accompanying changes to the script's invocation convention
;;   that ‘edit-script-header’ does not handle.
;;
;; * modern-sh-wrapper
;;
;;   This style looks something like:
;;
;;   #!/bin/sh
;;   exec ${GUILE-guile} -e ENTRY-POINT -s $0 "$@"
;;   !#
;;
;;   The distinguishing characteristic is ‘exec’ and ‘-s $0 "$@"’.  The
;;   ‘-e ENTRY-POINT’ is optional.  This style is fully convertable to
;;   and from the ‘direct-with-meta-switch’ style.
;;
;; * direct-with-meta-switch
;;
;;   This style looks something like:
;;
;;   #!/usr/local/bin/guile \
;;   -e ENTRY-POINT -s
;;   !#
;;
;;   The distinguishing characteristic is the guile interpreter named in
;;   the first line as well as the meta switch.  The ‘-e ENTRY-POINT’ is
;;   optional.  This style is fully convertable to and from the
;;   ‘modern-sh-wrapper’ style.
;;
;; * direct-with-minus-s
;;
;;   This style looks something like:
;;
;;   #!/usr/local/bin/guile -s
;;   !#
;;
;;   The distinguishing characteristic is the guile interpreter named
;;   in the first line as well as the ‘-s’ switch.  This style can be
;;   converted to and from the other ones with some restrictions.

;;; Code:

(define-module (ttn-do edit-script-header)
  #:export (main)
  #:use-module ((ice-9 rdelim) #:select (read-line))
  #:use-module ((srfi srfi-11) #:select (let-values))
  #:use-module ((srfi srfi-13) #:select (string-index
                                         (substring/shared . shsub)
                                         string-concatenate-reverse
                                         string-take
                                         string-drop-right
                                         string-tokenize
                                         string-join
                                         string-prefix?
                                         string-suffix?))
  #:use-module ((srfi srfi-14) #:select (char-set-complement
                                         char-set:blank))
  #:use-module ((ttn-do zzz 0gx string-io) #:select (read-string!/pud
                                                     string-out-proc))
  #:use-module ((ttn-do zzz banalities) #:select (check-hv
                                                  decry
                                                  qop<-args))
  #:use-module ((ttn-do zzz personally) #:select (fs fso FE)))

;; Implementation Note: This exercise clearly highlights the need for
;;                      lexing/parsing support in Guile, proper.
;;                      Connoisseurs of clean code, you have been warned!

;; support (todo: reimpl w/ proper lexer/parser)

(define blasted-header
  (let ((cs (char-set-complement char-set:blank)))

    (define (top-nine port)
      (let loop ((acc '())
                 (count 9))
        (let ((line (read-line port 'concat)))
          (if (or (zero? count)
                  (string-prefix? "!#" line))
              (string-concatenate-reverse acc line)
              (loop (cons line acc)
                    (1- count))))))

    ;; blasted-header
    (lambda (filename)
      (let ((t9 (call-with-input-file filename top-nine)))
        (values                         ; 2 rv:
         (string-tokenize t9 cs)        ; tokens
         (acons 'header-size            ; parse-info
                (string-length t9)
                '()))))))

(define (coalesce-matching-quotes tokens)

  (define (coalesce q ls)
    (let ((q (make-string 1 q)))
      (let loop ((ls (reverse ls)) (acc #f) (rv '()))
        (if (null? ls)
            rv
            (let* ((s (car ls))
                   (aft? (string-suffix? q s))
                   (bef? (string-prefix? q s)))
              (cond ((and bef? aft?)
                     (and acc (decry "bad quote"))
                     (loop (cdr ls)
                           #f
                           (cons (shsub s 1 (1- (string-length s)))
                                 rv)))
                    (aft?
                     (and acc (decry "bad quote"))
                     (loop (cdr ls)
                           (list (string-drop-right s 1))
                           rv))
                    (bef?
                     (or acc (decry "unterminated quote"))
                     (loop (cdr ls)
                           #f
                           (cons (string-join (cons (shsub s 1)
                                                    acc))
                                 rv)))
                    (else
                     (let ((pushed (cons (car ls) (or acc rv))))
                       (loop (cdr ls)
                             (and acc pushed)
                             (if acc rv pushed))))))))))

  ;; Do it!
  (coalesce #\' (coalesce #\" tokens)))

(define (tokenize-header file)          ; bletcherous beyond belief
  (let-values (((tokens parse-info) (blasted-header file)))

    (define (note! key value)
      (cond ((assq key parse-info)
             => (lambda (pair)
                  (set-cdr! pair value)))
            (else
             (set! parse-info (acons key value parse-info)))))

    ;; separate newlines
    (let loop ((head tokens))
      (or (null? head)
          (let ((cur (car head)))
            (cond ((string-index cur #\newline)
                   => (lambda (hit)
                        (set-car! head (string-take cur hit))
                        (set-cdr! head (append `("\n" ,(substring cur (1+ hit)))
                                               (cdr head)))
                        (loop (cddr head))))
                  (else (loop (cdr head)))))))
    ;; reduce "main=...^J" to just the module name
    (let loop ((head tokens))
      (or (null? head)
          (if (string=? "main='(module-ref" (car head))
              (let* ((new-head (cddr head))
                     (guess (car new-head))
                     ;; if we assume guess looks like:
                     ;;    '\''(SOME MODULE NAME))
                     ;; then ‘name’ will be:
                     ;;    (SOME MODULE NAME)
                     (name (substring guess 4 (1- (string-length guess)))))
                (set-car! head name)
                (set-cdr! head (cddr new-head))
                (loop (cdr head)))
              (loop (cdr head)))))
    ;; coalesce quote pairs spread out over multiple tokens (discard quote)
    (set! tokens (coalesce-matching-quotes tokens))
    ;; replace various things
    (let loop ((head tokens))
      (or (null? head)
          (let ((cur (car head)))
            (cond ((string=? "\n" cur)
                   (set-car! head 'newline))
                  ((string=? "${GUILE-guile}" cur)
                   (set-car! head 'G-g))
                  ((string=? "#!" cur)
                   (set-car! head 'hash-bang))
                  ((string=? "!#" cur)
                   (set-car! head 'bang-hash))
                  ((string-prefix? "#!/" cur)
                   (set-car! head 'hash-bang)
                   (set-cdr! head (cons (substring cur 2) (cdr head))))
                  ((string=? "$0" cur)
                   (set-car! head 'dollar-zero))
                  ((string=? "\"$@\"" cur)
                   (set-car! head 'quoted-dollar-at))
                  ((string=? "$@" cur)
                   (set-car! head 'dollar-at))
                  ((string=? "-e" cur)
                   (set-car! head 'minus-e))
                  ((string=? "-s" cur)
                   (set-car! head 'minus-s))
                  ((string=? "exec" cur)
                   (set-car! head 'exec))
                  ((string=? "guile" cur)
                   (set-car! head 'guile)))
            (loop (cdr head)))))
    ;; zonk comments (ignoring the hash-bang)
    (let loop ((head tokens))
      (or (null? (cdr head))
          (let* ((look-ahead (cdr head))
                 (first (car look-ahead)))
            (if (and (string? first)
                     (let ((n1 (string-index first #\#))
                           (n2 (string-index first #\!)))
                       (and n1 (or (not n2) (not (= (1+ n2) n1))))))
                (let search ((h2 (cdr look-ahead)))
                  (cond ((eq? 'newline (car h2))
                         (set-cdr! head h2)
                         (loop (cdr head)))
                        (else (search (cdr h2)))))
                (loop (cdr head))))))
    ;; minimize line count
    (let loop ((ls (let find-newline ((all (reverse! tokens)))
                     (if (eq? (car all) 'newline)
                         all
                         (find-newline (cdr all)))))
               (acc '())
               (count 0))
      (cond ((null? ls)
             (note! 'line-count count)
             (set! tokens acc))
            ((eq? 'newline (car ls))
             (let ((tail (cdr ls)))
               (if (and (not (null? acc)) (eq? 'newline (car acc)))
                   (loop tail acc count)
                   (loop tail (cons (car ls) acc) (1+ count)))))
            (else (loop (cdr ls) (cons (car ls) acc) count))))
    ;; note lines in order (discard newline)
    (let loop ((ls tokens) (acc '()) (lines '()))
      (if (null? ls)
          (note! 'lines (reverse! lines))
          (let ((cur (car ls)))
            (if (eq? 'newline cur)
                (loop (cdr ls) '() (cons (reverse! acc) lines))
                (loop (cdr ls) (cons cur acc) lines)))))
    ;; retval
    (note! 'tokens tokens)
    parse-info))

(define (attributes&idiom raw)          ; bombastic begotten bilge
  (let* ((attributes '())
         (lines (assq-ref raw 'lines))
         (tokens (assq-ref raw 'tokens))
         (first-line (car lines))
         (second-line (if (< 1 (length lines)) (cadr lines) '()))
         (third-line (if (< 2 (length lines)) (caddr lines) '()))
         (guile #f) (module #f) (proc #f))

    (define (attr! a)
      (set! attributes (cons a attributes)))

    (define (attr? a)
      (memq a attributes))

    (define (idiom! name . extra)
      (acons 'idiom name
             (acons 'attributes attributes
                    (append extra raw))))

    ;; set some attributes
    (or (equal? '(bang-hash) (car (last-pair lines)))
        (attr! 'missing-bang-hash))
    (or (equal? 'hash-bang (car first-line))
        (attr! 'missing-hash-bang))
    (and (< 1 (length first-line))
         (let ((shell (cadr first-line)))
           (and (or (string=? "/bin/sh" shell)
                    (string=? "/bin/bash" shell))
                (attr! 'sh-wrapper))
           (and (string-suffix? "guile" (cadr first-line))
                (attr! 'some-guile))
           (and (= 3 (length first-line))
                (attr! 'first-line-arg))))
    (and (= 1 (length second-line))
         (let ((s (car second-line)))
           (and (string? s)
                (not (zero? (string-length s)))
                (char=? #\( (string-ref s 0))
                (char=? #\) (string-ref s (1- (string-length s))))
                (attr! 'second-line-module-name)
                (set! module (car second-line)))))
    (and (< 1 (length second-line))
         (eq? 'exec (car second-line))
         (attr! 'second-line-exec)
         (set! guile (cadr second-line)))
    (and (< 1 (length third-line))
         (eq? 'exec (car third-line))
         (attr! 'third-line-exec)
         (set! guile (cadr third-line)))
    (cond ((memq 'minus-e tokens)
           => (lambda (ls)
                (attr! 'entry-point)
                (and (string? (cadr ls))
                     (let* ((ep (cadr ls))
                            (split (1+ (or (string-index ep #\)) -1))))
                       (and (char=? #\( (string-ref ep 0))
                            (attr! 'entry-point-is-module)
                            (set! module (string-take ep split)))
                       (if (char=? #\) (string-ref ep (1- (string-length ep))))
                           (attr! 'entry-point-proc-omitted)
                           (let loop ((s (substring ep split)))
                             (if (char=? #\space (string-ref s 0))
                                 (loop (substring s 1))
                                 (set! proc s)))))))))
    ;; determine idiom (and set more attributes)
    (cond ((or (attr? 'missing-bang-hash) ;;; rv
               (attr? 'missing-hash-bang))
           (idiom! 'broken))
          ((and (attr? 'sh-wrapper)
                (attr? 'second-line-module-name)
                (attr? 'third-line-exec)
                (equal? '("-l" dollar-zero
                          "-c" "(apply $main (cdr (command-line)))"
                          quoted-dollar-at)
                        (cddr third-line)))
           (attr! 'simple)
           (idiom! 'old-style-sh-wrapper
                   `(guile . ,guile)
                   `(module . ,module)))
          ((and (attr? 'sh-wrapper)
                (attr? 'second-line-exec))
           (let* ((rest (cddr second-line))
                  (len (length rest))
                  (ep? (attr? 'entry-point)))
             (and (= (if ep? 5 3) len)
                  (let ((r2 ((if ep? cddr identity) rest)))
                    (or (equal? '(minus-s dollar-zero dollar-at) r2)
                        (equal? '(minus-s dollar-zero quoted-dollar-at) r2)))
                  (attr! 'simple)))
           (idiom! 'modern-sh-wrapper
                   `(guile . ,guile)
                   `(module . ,module)
                   `(proc . ,proc)))
          ((and (attr? 'some-guile)
                (attr? 'first-line-arg)
                (string=? "\\" (caddr first-line)))
           (and (if (attr? 'entry-point)
                    (and (= 3 (length second-line))
                         (eq? 'minus-s (caddr second-line)))
                    (eq? '(minus-s) second-line))
                (attr! 'simple))
           (idiom! 'direct-with-meta-switch
                   `(guile . ,(cadr first-line))
                   `(module . ,module)
                   `(proc . ,proc)))
          ((and (attr? 'some-guile)
                (attr? 'first-line-arg)
                (eq? 'minus-s (caddr first-line)))
           (attr! 'simple)
           (idiom! 'direct-with-minus-s
                   `(guile . ,(cadr first-line))))
          (else
           (idiom! 'unrecognized)))))

;; dispatch

(define (parse-script-header file)
  (attributes&idiom (tokenize-header file)))

(define (bindir-guile)
  (in-vicinity (assq-ref %guile-build-info 'bindir) "guile"))

(define (edit-script-header options file)
  (let ((info (parse-script-header file)))

    (define (opt o)
      (assq-ref options o))

    (define (look key)
      (assq-ref info key))

    (define (attr a)
      (memq a (look 'attributes)))

    (define (set-guile! guile)
      (let ((cell (assq 'guile info)))
        (set-cdr! cell guile)))

    (define (commit! fmt . args)
      (let* ((new (apply fs fmt args))
             (ofile (or (opt 'output) file))
             (ext (opt 'backup))
             (new-len (string-length new))
             (orig-size (stat:size (stat file)))
             (bye (look 'header-size))
             (buf (make-string (+ new-len (- orig-size bye)))))
        (substring-move! new 0 new-len buf 0)
        (call-with-input-file file
          (lambda (port)
            (seek port bye SEEK_SET)
            (read-string!/pud buf port new-len (string-length buf))))
        (and ext
             (file-exists? ofile)
             (rename-file ofile (fs "~A~A" ofile ext))
             (opt 'verbose)
             (fso "wrote backup file: ~A~A~%" ofile ext))
        (call-with-output-file ofile
          (string-out-proc buf))
        (and (opt 'verbose)
             (fso "wrote ~A output file: ~A~%"
                  (or (opt 'style) (look 'idiom)) ofile))))

    (define (check-guile-for-sh-wrapper-variants!)
      (cond ((and (opt 'GUILE-guile) (opt 'installed))
             (set-guile! (fs "${GUILE-~A}" (bindir-guile))))
            ((and (opt 'GUILE-guile) (opt 'guile))
             => (lambda (guile)
                  (set-guile! (fs "${GUILE-~A}" guile))))
            ((opt 'guile)
             => set-guile!)
            ((or (opt 'GUILE-guile) (eq? 'G-g (look 'guile)))
             (set-guile! "${GUILE-guile}"))
            ((not (look 'guile))
             (set-guile! (bindir-guile)))))

    (define (check-guile-for-direct-variants!)
      (cond
       ((opt 'guile)
        => set-guile!)
       ((opt 'installed)
        (set-guile! (bindir-guile)))
       ((or (eq? 'G-g (look 'guile))
            (not (let ((s (look 'guile)))
                   (and s (string? s)
                        (let ((len (string-length s)))
                          (and (< 9 len)
                               (string-prefix? "${GUILE-" s)
                               (char=? #\} (string-ref s (1- len)))
                               (set-cdr! (assq 'guile info)
                                         (substring s 8 (1- len)))))))))
        (decry "cannot infer interpreter, use -i or -g to specify"))))

    (define (formatted-entry-point-maybe)
      (cond ((and (look 'module) (look 'proc))
             (fs "-e \"~A ~A\" " (look 'module) (look 'proc)))
            ((look 'module)
             => (lambda (module)
                  (fs "-e ~S " module)))
            ((look 'proc)
             => (lambda (proc)
                  (fs "-e ~S " proc)))
            ((attr 'entry-point)
             (fs "-e ~S " (cadr (memq 'minus-e (look 'tokens)))))
            (else
             "")))

    (or (attr 'simple)
        (decry "lame parser cannot handle complexity (sorry)"))
    (let* ((idiom (look 'idiom))
           (style (or (opt 'style) idiom)))
      (and (memq idiom '(broken unrecognized))
           (decry "cannot handle ~A (it is unfortunately ~A somehow)"
                  file idiom))
      (case style

        ((old-style-sh-wrapper)
         (check-guile-for-sh-wrapper-variants!)
         (or (look 'module)
             (decry "missing module specification"))
         (let ((proc (look 'proc)))
           (and proc (string? proc)
                (not (string=? "main" proc))
                (decry "old-style-sh-wrapper can only handle proc ‘main’")))
         (commit! "#!/bin/sh~%~A~A~A~%exec ~A -l $0 -c ~S \"$@\"~%!#~%"
                  "main='(module-ref (resolve-module '\\''"
                  (look 'module)
                  ") '\\'main')' # -*-scheme-*-"
                  (look 'guile)
                  "(apply $main (cdr (command-line)))"))

        ((modern-sh-wrapper)
         (check-guile-for-sh-wrapper-variants!)
         (commit! "#!/bin/sh~%exec ~A ~A-s $0 \"$@\" # -*-scheme-*-~%!#~%"
                  (look 'guile)
                  (formatted-entry-point-maybe)))

        ((direct-with-meta-switch)
         (and (opt 'G-g) (decry "incompatible option: --GUILE-guile"))
         (check-guile-for-direct-variants!)
         (commit! "#!~A \\~%~A-s~%!#~%"
                  (or (look 'guile)
                      (bindir-guile))
                  (formatted-entry-point-maybe)))

        ((direct-with-minus-s)
         (cond
          ((opt 'G-g)
           (decry "incompatible option: --GUILE-guile"))
          ((attr 'entry-point)
           (decry "direct-with-minus-s loses entry point information")))
         (check-guile-for-direct-variants!)
         (commit! "#!~A -s~%!#~%"
                  (or (look 'guile)
                      (bindir-guile))))

        (else
         (decry "unsupported style: ~A" style))))))

(define (edit-script-header/qop qop)
  (let ((options '()))

    (define (chk! key)
      (qop key (lambda (val)
                 (set! options (acons key val options)))))

    (FE '(GUILE-guile guile output verbose style backup installed)
        chk!)
    (cond ((assq 'style options)
           => (lambda (cell)
                (let ((sym (string->symbol (cdr cell))))
                  (or (memq sym '(old-style-sh-wrapper
                                  modern-sh-wrapper
                                  direct-with-meta-switch
                                  direct-with-minus-s))
                      (decry "invalid style: ~S" sym))
                  (set-cdr! cell sym)))))
    (and (assq 'installed options)
         (assq 'guile options)
         (decry "-g is incompatible with -i"))
    (FE (qop '())
        (if (qop 'no-act)
            (lambda (file)
              (fso "file: ~A~%" file)
              (FE (parse-script-header file)
                  (lambda (x)
                    (if (list? (cdr x))
                        (FE (cdr x) (lambda (elem)
                                      (fso "~A: ~S~%" (car x) elem)))
                        (fso "~A: ~S~%" (car x) (cdr x))))))
            (lambda (file)
              (edit-script-header options file)))))
  #t)

(define (main args)
  (check-hv args '((package . "ttn-do")
                   (version . "2.0")
                   (help . commentary)))
  (edit-script-header/qop
   (qop<-args
    args '((guile       (single-char #\g) (value optional))
           (GUILE-guile (single-char #\G))
           (no-act      (single-char #\n))
           (output      (single-char #\o) (value #t))
           (style       (single-char #\s) (value #t))
           (verbose     (single-char #\v))
           (backup      (single-char #\b) (value #t))
           (installed   (single-char #\i))))))

;;; edit-script-header ends here
