#!/bin/sh
exec ${GUILE-guile} -e '(ttn-do make-boot-set)' -s $0 "$@" # -*- scheme -*-
!#
;;; make-boot-set --- for fun and profit!

;; Copyright (C) 2001, 2003-2007, 2009, 2010, 2012, 2021 Thien-Thi Nguyen
;;
;; This file is part of ttn-do, released under the terms of the
;; GNU General Public License as published by the Free Software
;; Foundation; either version 3, or (at your option) any later
;; version.  There is NO WARRANTY.  See file COPYING for details.

;;; Commentary:

;; Usage: make-boot-set DIR
;;
;; Write sequence of floppies with the Debian boot set in DIR.
;; A possible money-making scheme, in the right clueless environment.

;;; Code:

(define-module (ttn-do make-boot-set)
  #:export (main)
  #:use-module ((ttn-do zzz banalities) #:select (check-hv
                                                  decry))
  #:use-module ((ttn-do zzz 0gx setvbuf-arg) #:select (*non-buffered*))
  #:use-module ((ice-9 rdelim) #:select (read-line))
  #:use-module ((ttn-do zzz personally) #:select (FE fs fso))
  #:use-module ((ttn-do zzz filesystem) #:select (dir-exists?))
  #:use-module ((ttn-do zzz subprocess) #:select (shell-command->list
                                                  sysfmt)))

(define VERSION "2.1")
;; 2.1 -- Guile 2.2 slog
;; 2.0 -- rewrite

(define (prompt s . args)
  (apply fso (string-append s "~%  (Type RET to continue, q to quit.)  ")
         args)
  (let ((line (read-line)))
    (newline)
    (and (equal? "q" line)
         (exit #t))))

(define (write-image filename)
  (fso "START Writing image: ~A~%" filename)
  (prompt "Place fresh floppy into drive.")
  (let ((status (sysfmt "dd if='~A' of=/dev/fd0"
                        filename)))
    (fso "=> ~A~%" status)
    (case status
      ((0) (fso "OK Done image: ~A~%" filename))
      (else (decry "Unhandled status: ~S" status)))))

(define (make-boot-set dir)
  (prompt "make-boot-set ~A" VERSION)
  (FE (shell-command->list (fs "find ~A -name '*.bin' -print"
                               dir))
      write-image))

(define (main args)
  (check-hv args `((package . "ttn-do")
                   (version . ,VERSION)
                   (help . commentary)))
  (or (pair? (cdr args))
      (decry "missing arg (try --help)"))
  (let ((dir (cadr args)))
    (or (dir-exists? dir)
        (decry "no such directory: ~A" dir))
    (setvbuf (current-input-port) *non-buffered*)
    (setvbuf (current-output-port) *non-buffered*)
    (make-boot-set dir)))

;;; make-boot-set ends here
