#!/bin/sh
exec strace -f -e open,openat "$@" 2>&1 | sed /o.such.file/d
