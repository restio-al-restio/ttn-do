#!/bin/sh
exec ${GUILE-guile} -e '(ttn-do xlsatoms)' -s $0 "$@" # -*- scheme -*-
!#
;;; xlsatoms

;; Copyright (C) 2007, 2009, 2010, 2011, 2017 Thien-Thi Nguyen
;;
;; This file is part of ttn-do, released under the terms of the
;; GNU General Public License as published by the Free Software
;; Foundation; either version 3, or (at your option) any later
;; version.  There is NO WARRANTY.  See file COPYING for details.

;;; Commentary:

;; Usage: xlsatoms
;;
;; Display atoms from the X server, one per line.

;;; Code:

(define-module (ttn-do xlsatoms)
  #:export (main)
  #:use-module ((ttn-do zzz banalities) #:select (check-hv))
  #:use-module ((ttn-do zzz personally) #:select (fso))
  #:use-module ((ttn-do zzz x-umbrages) #:select (connection
                                                  atom-manager)))

(define (main args)
  (check-hv args '((package . "ttn-do")
                   (version . "1.1")
                   ;; 1.1  -- output atom name more cleanly
                   ;; 1.0  -- initial release
                   (help . commentary)))
  (let* ((conn (connection))
         (amgr (atom-manager conn)))
    (let loop ((atom 1))
      (and=> (amgr #:symbolic atom)
             (lambda (name)
               (fso "~A\t~A\n" atom (symbol->string name))
               (loop (1+ atom)))))
    (conn #:bye)))

;;; xlsatoms ends here
