;;; (ttn-do zzz gap-buffer)

;; Copyright (C) 2002-2005, 2007-2013, 2017, 2020, 2021 Thien-Thi Nguyen
;;
;; This file is part of ttn-do, released under the terms of the
;; GNU General Public License as published by the Free Software
;; Foundation; either version 3, or (at your option) any later
;; version.  There is NO WARRANTY.  See file COPYING for details.

;;; Code:

(define-module (ttn-do zzz gap-buffer)
  #:export (filename:
            gb?
            make-gap-buffer
            gb-toggle-read-only
            gb-point
            gb-point-min
            gb-point-max
            gb-char-after
            gb-char-before
            gb-bolp gb-eolp gb-bobp gb-eobp
            gb-insert-string!
            gb-insert-char!
            gb-insert
            gb-delete-char!
            gb-delete-region
            gb-erase!
            gb-goto-char
            gb-forward-char gb-backward-char
            gb-forward-line gb-beginning-of-line gb-end-of-line
            md md!
            gb-match-string
            gb-looking-at
            gb-match-beginning gb-match-end
            gb-search-forward gb-search-backward
            gb-re-search-forward
            gb-replace-match
            gb->port!
            gb->string
            gb->substring
            gb->lines
            make-gap-buffer-port
            gb-insert-file-contents)
  #:use-module ((srfi srfi-8) #:select (receive))
  #:use-module ((srfi srfi-13) #:select (string-concatenate-reverse
                                         substring/shared))
  #:autoload (ice-9 rdelim) (read-delimited)
  #:autoload (ice-9 regex) (string-match
                            match:start
                            match:end)
  #:autoload (ttn-do zzz 0gx string-io) (read-string!/pud
                                         write-string/pud)
  #:autoload (ttn-do zzz banalities) (check-hv)
  #:autoload (ttn-do zzz filesystem) (expand-file-name-substituting-env-vars)
  #:use-module ((ttn-do zzz personally) #:select (FE)))

;; Return the (string) filename associated with @var{object}, or @code{#f}.
;; This is an object property so you can @code{set!} it; that's what
;; @code{find-file} and @code{find-file-read-only} do on the buffer
;; objects they return to effect @dfn{visiting}.
;;
;;-category: object property
;;-args: (1 0 0 object)
;;
(define filename: (make-object-property))

;;; Support

(define (seek+read-string!/pud buf port rd-offset len wr-offset)
  (seek port rd-offset SEEK_SET)
  (read-string!/pud buf port wr-offset (min (+ wr-offset len)
                                            (string-length buf))))


;;; Gap buffer

;; A gap buffer is a structure that models a string but allows relatively
;; efficient insertion of text somewhere in the middle.  The insertion
;; location is called `point' with minimum value 1, and a maximum value the
;; length of the string (which is not fixed).
;;
;; Specifically, we allocate a continuous buffer of characters that is
;; composed of the BEFORE, the GAP and the AFTER (reading L->R), like so:
;;
;;                          +--- POINT
;;                          v
;;    +--------------------+--------------------+--------------------+
;;    |       BEFORE       |        GAP         |       AFTER        |
;;    +--------------------+--------------------+--------------------+
;;
;;     <----- bef-sz ----->|<----- gap-sz ----->|<----- aft-sz ----->
;;
;;     <-------------------|       usr-sz       |------------------->
;;
;;     <-------------------------- all-sz -------------------------->
;;
;; This diagram also shows how the different sizes are computed, and the
;; location of POINT.  Note that the user-visible buffer size `usr-sz' does
;; NOT include the GAP, while the allocation `all-sz' DOES.
;;
;; The consequence of this arrangement is that "moving point" is simply a
;; matter of kicking characters across the GAP, while insertion can be viewed
;; as filling up the gap, increasing `bef-sz' and decreasing `gap-sz'.  When
;; `gap-sz' falls below some threshold, we reallocate with a larger `all-sz'.
;;
;; In the implementation, we actually keep track of the AFTER start offset
;; `aft-ofs' since it is used more often than `gap-sz'.  In fact, most of the
;; variables in the diagram are for conceptualization only.
;;
;; A gap buffer port is a "soft port" that wraps a gap buffer.
;;
;; (The term and concept of "gap buffer" are borrowed from Emacs.  We will
;; gladly return them when libemacs.so is available. ;-)
;;
;; A gap-buffer object has the following printed representation:
;;
;; #<gap-buffer GAP-SIZE/ALLOC-SIZE:POINT-MIN:POINT:POINT-MAX>
;;
;; with all fields (GAP-SIZE, ALLOC-SIZE, MINT-MIN, POINT, and POINT-MAX)
;; integers, and everything else as shown here.

(define gap-buffer
  ;; TODO: Use structs directly.
  (make-record-type 'gap-buffer
                    '(s                 ; the buffer, a string
                      gap-ofs           ; GAP starts, aka (1- point)
                      aft-ofs           ; AFTER starts
                      ro)               ; read-only; insert/delete => error
                    (lambda (obj port)
                      (display "#<gap-buffer " port)
                      (display (- (aft-ofs: obj) (gap-ofs: obj)) port)
                      (display #\/ port)
                      (display (string-length (s: obj)) port)
                      (display #\: port)
                      (display (gb-point-min obj) port)
                      (display #\: port)
                      (display (gb-point obj) port)
                      (display #\: port)
                      (display (gb-point-max obj) port)
                      (display ">" port))))

;; Return #t iff @var{object} is a gap buffer object.
;;
(define (gb? object)
  ((record-predicate gap-buffer) object))

(define s:       (record-accessor gap-buffer 's))
(define gap-ofs: (record-accessor gap-buffer 'gap-ofs))
(define aft-ofs: (record-accessor gap-buffer 'aft-ofs))
(define ro:      (record-accessor gap-buffer 'ro))

(define s!       (record-modifier gap-buffer 's))
(define gap-ofs! (record-modifier gap-buffer 'gap-ofs))
(define aft-ofs! (record-modifier gap-buffer 'aft-ofs))
(define ro!      (record-modifier gap-buffer 'ro))

(define (check-read-only gb)
  (and (ro: gb) (error "buffer read-only:" gb)))

;; todo: expose
(define default-chunk-size 1024)
(define default-realloc-threshold 32)

(define (round-up n)
  (* default-chunk-size (+ 1 (quotient n default-chunk-size))))

(define new (record-constructor gap-buffer))

(define (realloc gb inc)
  (let* ((old-s   (s: gb))
         (all-sz  (string-length old-s))
         (new-sz  (+ all-sz inc))
         (gap-ofs (gap-ofs: gb))
         (aft-ofs (aft-ofs: gb))
         (new-s   (make-string new-sz))
         (new-aft-ofs (+ aft-ofs inc)))
    (substring-move! old-s 0 gap-ofs new-s 0)
    (substring-move! old-s aft-ofs all-sz new-s new-aft-ofs)
    (s! gb new-s)
    (aft-ofs! gb new-aft-ofs)))

(define (insert-prep gb len . precisely?)
  (let* ((gap-ofs (gap-ofs: gb))
         (aft-ofs (aft-ofs: gb))
         (slack (- (- aft-ofs gap-ofs) len)))
    (and (< slack default-realloc-threshold)
         (realloc gb ((if (or (null? precisely?)
                              (not (car precisely?)))
                          round-up
                          identity)
                      (- slack))))
    gap-ofs))

(define unibyte-port
  (cond-expand
   ;; sorry, gotta trudge
   (guile-2
    (lambda (port)
      ;; Theoretically, if the port encoding is ISO-8859-1,
      ;; we could manage things, but let's leave that for the
      ;; possible future.  (Kick the can down the road...)
      #f))
   ;; root of all evil, sigh
   (else
    (lambda (port)
      (false-if-exception (seek port 0 SEEK_CUR))))))

(define (drain-port port)               ; => STRING
  (let ((got (read-delimited "" port)))
    (if (string? got)
        got
        "")))

(define (insert-from-port gb port precisely?)

  (define (fast cur)
    (let* ((end (seek port 0 SEEK_END))
           (len (- end cur)))
      (values len (lambda (s wr-offset)
                    (seek+read-string!/pud
                     s port cur len wr-offset)))))

  (define (slow)
    (let* ((sin (drain-port port))
           (len (string-length sin)))
      (values len (lambda (s wr-offset)
                    (substring-move! sin 0 len s wr-offset)))))

  (check-read-only gb)
  (receive (len move!) (cond ((unibyte-port port)
                              => fast)
                             (else (slow)))
    (let ((gap-ofs (insert-prep gb len precisely?)))
      (move! (s: gb) gap-ofs)
      (gap-ofs! gb (+ gap-ofs len)))))

;; Return a new gap buffer.  Optional arg @var{init} is either a port
;; to read from; a string, used to initialize the buffer contents;
;; or an integer specifying the memory allocation (in bytes) requested.
;; Point is left at the maximum position.
;;
;;-args: (- 1 0)
;;
(define (make-gap-buffer . init)
  (let ((x (if (null? init)
               0
               (car init))))
    (cond ((integer? x)
           (new (make-string x) 0 x #f))
          ((port? x)
           (let ((gb (new "" 0 0 #f)))
             (insert-from-port gb x #t)
             gb))
          ((string? x)
           (let* ((len (string-length x))
                  (alloc (round-up len))
                  (s (make-string alloc)))
             (substring-move! x 0 len s 0)
             (new s len alloc #f)))
          (else (error "bad init type")))))

;; Change whether @var{gb} is read-only.
;; With @var{arg}, set read-only iff @var{arg} is positive.
;;
;;-args: (- 1 0)
;;
(define (gb-toggle-read-only gb . arg)
  (let* ((cur (ro: gb))
         (now (cond ((null? arg) (not cur))
                    ((car arg) => (lambda (v)
                                    (and v (number? v) (positive? v)))))))
    ;; compact on transition from rw to ro
    (and (not cur) now
         (let ((gap (gap-ofs: gb))
               (aft (aft-ofs: gb)))
           (or (= gap aft)
               ;; present approach: realloc (make new string);
               ;; todo: set high-water mark and avoid realloc (re-use string)
               (let* ((s   (s: gb))
                      (len (string-length s))
                      (new (make-string (+ gap (- len aft)))))
                 (substring-move! s 0 gap new 0)
                 (substring-move! s aft len new gap)
                 (s! gb new)
                 (aft-ofs! gb gap)))))
    (ro! gb now)
    now))

;; Return the position of point in @var{gb}.
;; This is an integer starting with 1 (one).
;;
(define (gb-point gb)
  (1+ (gap-ofs: gb)))

;; Return the minimum position possible for point in @var{gb}.
;; At this time, this value is always 1 (one).
;;
(define (gb-point-min gb) 1)            ; no narrowing (for now)

;; Return the maximum position possible for point in @var{gb}.
;; This value can be changed by inserting text into the buffer,
;; and is limited by Guile's string implementation.
;;
(define (gb-point-max gb)
  (1+ (- (string-length (s: gb)) (- (aft-ofs: gb) (gap-ofs: gb)))))

;; Return char after @var{pos}, or #f if there is no char there.
;; If @var{pos} is not specified, it defaults to point.
;;
(define (gb-char-after gb . pos)
  (let* ((s (s: gb))
         (gap (gap-ofs: gb))
         (aft (aft-ofs: gb))
         (idx (if (null? pos)
                  gap
                  (1- (car pos)))))
    (and (<= (gb-point-min gb) (1+ idx))
         (begin
           (or (< idx gap)
               (set! idx (+ idx (- aft gap))))
           (and (< idx (string-length s))
                (string-ref s idx))))))

;; Return char before @var{pos}, or #f if there is no char there.
;; If @var{pos} is not specified, it defaults to point.
;;
(define (gb-char-before gb . pos)
  (gb-char-after gb (1- (if (null? pos)
                            (gb-point gb)
                            (car pos)))))

(define (nl? s i)
  (char=? #\newline (string-ref s i)))

;; Return #t if point in @var{gb} is at the beginning of a line.
;;
(define (gb-bolp gb)
  (or (= (gb-point gb) (gb-point-min gb))
      (nl? (s: gb) (1- (gap-ofs: gb)))))

;; Return #t if point in @var{gb} is at the end of a line.
;;
(define (gb-eolp gb)
  (or (= (gb-point gb) (gb-point-max gb))
      (nl? (s: gb) (aft-ofs: gb))))

;; Return #t if point is at the beginning of @var{gb}.
;;
(define (gb-bobp gb)
  (= 1 (gb-point gb)))

;; Return #t if point is at the end of @var{gb}.
;;
(define (gb-eobp gb)
  (= (gb-point-max gb) (gb-point gb)))

;; Insert into @var{gb} a @var{string}, moving point forward as well as
;; increasing the value that would be returned by @code{gb-point-max}.
;;
(define (gb-insert-string! gb string)
  (check-read-only gb)
  (let* ((len (string-length string))
         (gap-ofs (insert-prep gb len)))
    (substring-move! string 0 len (s: gb) gap-ofs)
    (gap-ofs! gb (+ gap-ofs len))))

;; Insert into @var{gb} a single @var{char}, moving point forward as well as
;; increasing the value that would be returned by @code{gb-point-max}.
;;
(define (gb-insert-char! gb char)
  (check-read-only gb)
  (let ((gap-ofs (insert-prep gb 1)))
    (string-set! (s: gb) gap-ofs char)
    (gap-ofs! gb (+ gap-ofs 1))))

;; Insert the arguments at point.
;; If an arg is a gap-buffer, insert its contents.
;; If an arg is a pair, insert a string made by applying @code{write} to it.
;; If an arg is a number, insert the result of @code{number->string}.
;; Other types accepted: char, string, symbol.
;; Point moves forward to end up after the inserted text.
;;
(define (gb-insert gb . args)
  (let ((ins! (lambda (s) (gb-insert-string! gb s))))
    (FE args
        (lambda (x)
          (cond ((gb? x) (ins! (gb->string x)))
                ((char? x) (gb-insert-char! gb x))
                ((string? x) (ins! x))
                ((symbol? x) (ins! (symbol->string x)))
                ((number? x) (ins! (number->string x)))
                ((pair? x) (ins! (with-output-to-string
                                   (lambda () (write x)))))
                ((port? x) (insert-from-port gb x #f))
                (else (error "wrong type arg")))))))

;; In @var{gb}, delete @var{count} characters from point, forward if
;; @var{count} is positive, backward if @var{count} is negative.  (If
;; @var{count} is zero, do nothing.)  Deleting backwards moves point
;; backwards.  Deleting forwards or backwards decreases the value that would
;; be returned by @code{gb-point-max}.
;;
(define (gb-delete-char! gb count)
  (check-read-only gb)
  (cond ((< count 0)                    ; backwards
         (gap-ofs! gb (max 0 (+ (gap-ofs: gb) count))))
        ((> count 0)                    ; forwards
         (aft-ofs! gb (min (string-length (s: gb)) (+ (aft-ofs: gb) count))))
        ((zero? count)                    ; do nothing
         #t)))

;; Delete text between @var{beg} and @var{end}.
;;
(define (gb-delete-region gb beg end)
  (check-read-only gb)
  (let ((b (min beg end))
        (p (gb-point gb))
        (e (max beg end)))
    (gb-goto-char gb b)
    (gb-delete-char! gb (- e b))
    (or (<= b p e)                      ; region included point => stay put
        (gb-forward-char
         gb (- p (if (< p b)
                     b                  ; was point region => move backward
                     e))))))            ; was region point => move forward

;; Completely erase @var{gb}.  Point is left at the minimum position possible
;; (which happens to be also the maximum position possible since the buffer
;; is empty).
;;
(define (gb-erase! gb)
  (check-read-only gb)
  (gap-ofs! gb 0)
  (aft-ofs! gb (string-length (s: gb))))

(define subs-move-left!
  (cond-expand (guile-2 substring-move!)
               (else substring-move-left!)))

(define subs-move-right!
  (cond-expand (guile-2 substring-move!)
               (else substring-move-right!)))

(define (point++n! gb n s gap-ofs aft-ofs) ; n>0; warning: reckless
  (subs-move-left! s aft-ofs (+ aft-ofs n) s gap-ofs)
  (gap-ofs! gb (+ gap-ofs n))
  (aft-ofs! gb (+ aft-ofs n)))

(define (point+-n! gb n s gap-ofs aft-ofs) ; n<0; warning: reckless
  (subs-move-right! s (+ gap-ofs n) gap-ofs s (+ aft-ofs n))
  (gap-ofs! gb (+ gap-ofs n))
  (aft-ofs! gb (+ aft-ofs n)))

;; In @var{gb}, move point to @var{new-point} and return it.  If
;; @var{new-point} is outside the minimum and maximum positions possible, it
;; is adjusted to the the nearest boundary (however, the return value is
;; @var{new-point} unchanged).
;;
(define (gb-goto-char gb new-point)
  (let ((pmax (gb-point-max gb)))
    (or (and (< new-point 1)    (gb-goto-char gb 1))
        (and (> new-point pmax) (gb-goto-char gb pmax))
        (let ((delta (- new-point (gb-point gb))))
          (or (zero? delta)
              ((if (< delta 0)
                   point+-n!
                   point++n!)
               gb delta (s: gb) (gap-ofs: gb) (aft-ofs: gb))))))
  new-point)

;; In gap-buffer @var{gb}, move point forward @var{n} characters.
;;
(define (gb-forward-char gb n)
  (or (zero? n)
      ((if (> n 0) point++n! point+-n!)
       gb n (s: gb) (gap-ofs: gb) (aft-ofs: gb))))

;; In gap-buffer @var{gb}, move point backward @var{n} characters.
;;
(define (gb-backward-char gb n)
  (gb-forward-char gb (- n)))

(define (forward-line-internal gb n)    ; => (REMAINING . AT-LEAST-ONE?)
  (let ((s (s: gb))
        (gap (gap-ofs: gb))
        (aft (aft-ofs: gb)))
    (if (positive? n)
        (let loop ((n n) (start aft) (at-least-one? #f))
          (cond ((zero? n)
                 (point++n! gb (- start aft) s gap aft)
                 (cons 0 at-least-one?))
                ((string-index s #\newline start)
                 => (lambda (nl)
                      (loop (1- n) (1+ nl) #t)))
                (else
                 (cons
                  (let ((pmax (gb-point-max gb)))
                    ;; "With positive N, a non-empty line at the end counts as
                    ;;  one line successfully moved (for the return value)."
                    (cond ((= pmax (gb-point gb)) n)
                          (else (gb-goto-char gb pmax)
                                (if at-least-one?
                                    n
                                    (1- n)))))
                  at-least-one?))))
        (let loop ((n n) (end gap))
          (cond ((= 1 n)
                 (point+-n! gb (- (1+ end) gap) s gap aft)
                 (cons 0 #f))
                ((string-rindex s #\newline 0 end)
                 => (lambda (nl)
                      (loop (1+ n) nl)))
                (else
                 (gb-goto-char gb (gb-point-min gb))
                 (cons n #f)))))))

;; In gap-buffer @var{gb}, move point @var{n} lines forward (backward if
;; @var{n} is negative).  Precisely, if point is on line @code{I}, move to the
;; start of line @code{I + N}.  If there isn't room, go as far as possible (no
;; error).  Return the count of lines left to move.  If moving forward, that
;; is @var{n} - number of lines moved; if backward, @var{n} + number moved.
;; With positive @var{n}, a non-empty line at the end counts as one line
;; successfully moved (for the return value).
;;
(define (gb-forward-line gb . n)
  (car (forward-line-internal gb (if (null? n) 1 (car n)))))

;; In gap-buffer @var{gb}, move point to beginning of current line.
;; With argument @var{n} not #f or 1, move forward @var{n} - 1 lines first.
;; If point reaches the beginning or end of buffer, it stops there.
;;
(define (gb-beginning-of-line gb . n)
  (forward-line-internal gb (1- (if (null? n) 1 (car n))))
  #f)

;; In gap-buffer @var{gb}, move point to end of current line.
;; With argument @var{n} not #f or 1, move forward @var{n} - 1 lines first.
;; If point reaches the beginning or end of buffer, it stops there.
;;
(define (gb-end-of-line gb . n)
  (let* ((n (if (null? n) 1 (car n)))
         (p-before (gb-point gb))
         (move-info (forward-line-internal gb n))
         (at-least-one? (cdr move-info))
         (p (gb-point gb)))
    (or (if (positive? n)
            (and (= p (gb-point-max gb))
                 (if at-least-one?
                     (< 1 n)
                     (let ((s (s: gb))
                           (gap (gap-ofs: gb)))
                       (or (= 1 n)
                           (not (string-rindex s #\newline
                                               (- gap (- p p-before))
                                               gap))))))
            (= p (gb-point-min gb)))
        (gb-forward-char gb -1)))
  #f)

;;; search (and replace)

;; "life is a search for truth; there is no truth." -- the ancients
;;
;; match data form
;;  emacs               guile                           mogrify
;;  (BEG-0 END-0 ...)   #(STRING (BEG-0 . END-0) ...)   ((BEG-0 . END-0) ...)

(define (guile-md->md guile-md ofs)
  (map (lambda (cell)
         (cons (+ ofs (car cell))
               (+ ofs (cdr cell))))
       (cdr (vector->list guile-md))))

(define --match-data (make-object-property))

;; Return the match-data of gap-buffer @var{gb}.
;;
(define (md  gb)         (--match-data gb))

;; Set the match-data of gap-buffer @var{gb} to @var{m}.
;; This should be a list of pairs of the form:
;;
;; @example
;; ((BEG-0 . END-0) ...)
;; @end example
;;
(define (md! gb m) (set! (--match-data gb) m))

(define (md2! gb a b)
  (md! gb (list (cons a b))))

;; Return string of text matched by last search.
;; @var{n} specifies which parenthesized expression in the last regexp.
;; Value is #f if @var{n}th pair didn't match, or there were less than
;; @var{n} pairs.  Zero means the entire text matched by the whole regexp
;; or whole string.
;;
;;-note: arg ‘string’ purposefully left undocumented. --ttn
;;-args: (2 0 0)
;;
(define (gb-match-string gb n . string)
  (let ((mn (list-ref (md gb) n)))
    (if (null? string)
        (bsub gb (1- (car mn)) (1- (cdr mn)))
        (substring (car string) (car mn) (cdr mn)))))

;; Return #t if text after point matches regular expression @var{re-str}.
;; This function modifies the match data that @code{gb-match-beginning},
;; @code{gb-match-end} and @code{gb-match-data} access; save and restore
;; the match data if you want to preserve them.
;;
(define (gb-looking-at gb re-str)
  (let* ((s (s: gb))
         (start (aft-ofs: gb))
         (gm (if (char=? #\^ (string-ref re-str 0))
                 (string-match re-str s start regexp/newline)
                 (string-match (string-append "^" re-str) s start))))
    (cond ((and gm (= start (car (vector-ref gm 1))))
           (md! gb (guile-md->md gm (1+ (- (gap-ofs: gb) start))))
           #t)
          (else #f))))

;; Return position of start of text matched by last search.
;; @var{subexp}, a number, specifies which parenthesized expression
;; in the last regexp.  Value is #f if @var{subexp}th pair didn't match,
;; or there were less than @var{subexp} pairs.  Zero means the entire text
;; matched by the whole regexp.
;;
;;-args: (- 1 0)
;;
(define (gb-match-beginning gb . n)
  (car (list-ref (md gb) (if (null? n) 0 (car n)))))

;; Return position of end of text matched by last search.
;; @var{subexp}, a number, specifies which parenthesized expression in the
;; last regexp.  Value is nil if @var{subexp}th pair didn't match, or there
;; were less than @var{subexp} pairs.  Zero means the entire text matched by
;; the whole regexp.
;;
;;-args: (- 1 0)
;;
(define (gb-match-end gb . n)
  (cdr (list-ref (md gb) (if (null? n) 0 (car n)))))

(define (parse-search-args gb edge args what)
  (let* ((bound (if (or (null? args)
                        (eq? #f (car args)))
                    edge
                    (car args)))
         (fail (cond ((or (< (length args) 2) (eq? #f (cadr args)))
                      (lambda () (error "search failed:" what)))
                     ((eq? #t (cadr args))
                      (lambda () #f))
                     (else
                      (lambda () (gb-goto-char gb bound) #f))))
         (reps (if (< (length args) 3) 1 (caddr args))))
    (values bound fail reps)))

;; Search forward from point for @var{string}.
;; Set point to the end of the occurrence found, and return point.
;; An optional second argument bounds the search; it is a buffer position.
;; The match found must not extend after that position.  #f is equivalent
;;   to (point-max).
;; Optional third argument, if #t, means if fail just return #f (no error).
;;   If not #f and not #t, move to limit of search and return #f.
;; Optional fourth argument is repeat count--search for successive occurrences.
;;
;;-todo: use string-index more efficiently
;;-args: (- 3 0 bound noerror count)
;;
(define (gb-search-forward gb string . args)
  (let* ((p (gb-point gb))
         (pmax (gb-point-max gb)))
    (receive (bound fail reps) (parse-search-args gb pmax args string)
      (if (string-null? string)
          p
          (let* ((len (string-length string))
                 (len-1 (1- len))
                 (s (s: gb))
                 (start (aft-ofs: gb))
                 (fc (string-ref string 0))               ; first char
                 (lc (string-ref string (1- len))))       ; last char

            (define (next from)
              (string-index s fc from))

            (let loop ((alook (next start)))
              (let ((look (and alook (+ p (- alook start)))))
                (cond
                 ;; no match / done
                 ((or (not look)
                      (not (< look bound))
                      (not (< (+ look len-1) bound)))
                  (fail))
                 ;; match / done if `reps' satisfied otherwise keep going
                 ((and (char=? (string-ref s (+ alook len-1)) lc)
                       (string=? (substring s alook (+ alook len)) string))
                  (set! reps (1- reps))
                  (if (zero? reps)
                      (let ((rest-point (+ look len)))
                        (md2! gb look rest-point)
                        (gb-goto-char gb rest-point)
                        rest-point)     ; retval
                      (loop (next (+ alook len)))))
                 ;; no match / keep going
                 (else (loop (next (1+ alook))))))))))))

;; Search backward from point for @var{string}.
;; Set point to the beginning of the occurrence found, and return point.
;; An optional second argument bounds the search; it is a buffer position.
;; The match found must not extend before that position.
;; Optional third argument, if t, means if fail just return nil (no error).
;;  If not nil and not t, position at limit of search and return nil.
;; Optional fourth argument is repeat count--search for successive occurrences.
;;
;;-args: (- 3 0 bound noerror repeat)
;;
(define (gb-search-backward gb string . args)
  (let* ((p (gb-point gb))
         (pmin (gb-point-min gb)))
    (receive (bound fail reps) (parse-search-args gb pmin args string)
      (if (string-null? string)
          p
          (let* ((len (string-length string))
                 (len-1 (1- len))
                 (s (s: gb))
                 (fc (string-ref string 0))               ; first char
                 (lc (string-ref string (1- len))))       ; last char

            (define (next to)
              (string-rindex s lc 0 to))

            (let loop ((blook (next (gap-ofs: gb))))
              (let ((look (and blook (+ pmin blook))))
                (cond
                 ;; no match / done
                 ((or (not look)
                      (not (>= look bound))
                      (not (>= (- look len-1) bound)))
                  (fail))
                 ;; match / done if `reps' satisfied otherwise keep going
                 ((and (char=? (string-ref s (- blook len-1)) fc)
                       (string=? (substring s (- blook len-1) (1+ blook))
                                 string))
                  (set! reps (1- reps))
                  (if (zero? reps)
                      (let ((rest-point (1+ (- look len))))
                        (md2! gb rest-point (1+ look))
                        (gb-goto-char gb rest-point)
                        rest-point)     ; retval
                      (loop (next (- blook len)))))
                 ;; no match / keep going
                 (else (loop (next (1- blook))))))))))))

;; Search forward from point for regular expression @var{regexp}.
;; Set point to the end of the occurrence found, and return point.
;; An optional second argument bounds the search; it is a buffer position.
;; The match found must not extend after that position.
;; Optional third argument, if #t, means if fail just return #f (no error).
;;   If not #f and not #t, move to limit of search and return #f.
;; Optional fourth argument is repeat count--search for successive occurrences.
;;
;; @var{regexp} may be a string, or compiled regular expression made with
;; @code{make-regexp}, in which case, it is the caller's decision whether or
;; not to include the flag @code{regexp/newline} (normally used when
;; @var{regexp} is a string to compile it internally).
;;
;;-args: (- 3 0 bound noerror repeat)
;;
(define (gb-re-search-forward gb regexp . args)
  (let* ((p (gb-point gb))
         (pmax (gb-point-max gb)))
    (receive (bound fail reps) (parse-search-args gb pmax args regexp)
      (if (and (string? regexp) (string-null? regexp))
          p
          (let* ((s (s: gb))
                 (start (aft-ofs: gb))
                 (rx (if (regexp? regexp)
                         regexp
                         (make-regexp regexp regexp/newline))))

            (define (next from flag)
              (regexp-exec rx s from flag)) ; ugh

            (let loop ((amatch (next start (if (= p 1) 0 regexp/notbol)))
                       (reps (1- reps)))
              (cond
               ;; no match / done
               ((not amatch) (fail))
               ;; match / done if `reps' satisfied
               ((zero? reps)
                (md! gb (guile-md->md amatch (1+ (- (gap-ofs: gb) start))))
                (let ((rest-point (gb-match-end gb)))
                  (gb-goto-char gb rest-point)
                  rest-point))          ; retval
               ;; match / not done
               (else (loop (next (match:end amatch) regexp/notbol)
                           (1- reps))))))))))

(define (replacement-text gb newtext)
  (define (err!) (error "invalid use of `\\' in replacement text"))
  (let* ((last-char (1- (string-length newtext)))
         (c-zero-as-num (char->integer #\0)))
    (define (nrep n) (gb-match-string gb n))
    (define (crep c) (nrep (- (char->integer c) c-zero-as-num)))
    (let loop ((start 0) (acc '()))
      (cond ((string-index newtext #\\ start)
             => (lambda (cut)
                  (and (= cut last-char) (err!))
                  (let ((pre (substring newtext start cut))
                        (c2 (string-ref newtext (1+ cut))))
                    (case c2
                      ((#\&) (loop (+ 2 cut) (cons* (nrep 0) pre acc)))
                      ((#\\) (loop (+ 2 cut) (cons* "\\" pre acc)))
                      ((#\0 #\1 #\2 #\3 #\4 #\5 #\6 #\7 #\8 #\9)
                       (loop (+ 2 cut) (cons* (crep c2) pre acc)))
                      (else (err!))))))
            (else
             (string-concatenate-reverse
              acc (substring newtext start)))))))

;; Replace text matched by last search with @var{newtext}.
;; The second arg is optional and ignored (for now -- in the
;; future it may specify case handling a la Emacs).
;;
;; If third arg @var{literal} is non-#f, insert @var{newtext} literally.
;; Otherwise treat @code{\} as special:
;; @example
;;   `\&' in NEWTEXT means substitute original matched text.
;;   `\N' means substitute what matched the Nth `(...)'.
;;        If Nth parens didn't match, substitute nothing.
;;   `\\' means insert one `\'.
;; @end example
;;
;; Leave point at end of replacement text.
;;
;;-args: (- 2 0 IGNORED literal)
;;
(define (gb-replace-match gb newtext . args)
  (check-read-only gb)
  (let* ((m (md gb)) (buhbye (list-ref m 0))
         (rtxt (if (or (and (< 1 (length args)) (cadr args))
                       (not (string-index newtext #\\))) ; optimize
                   newtext              ; literal
                   (replacement-text gb newtext))))
    (gb-goto-char gb (cdr buhbye))
    (gb-delete-char! gb (- (car buhbye) (cdr buhbye)))
    (gb-insert-string! gb rtxt)))

;;; i/o and bulk transforms

;; Send the contents of @var{gb} to the output @var{port}.
;; Optional args @var{beg} and @var{end} specify a region to send.
;; Point does not move.
;;
;; As a special case, if @var{port} is @code{#t}, send to
;; the current output port.
;;
;;-args: (- 2 0 beg end)
;;
(define (gb->port! gb port . opts)
  (let* ((port (if (eq? #t port)
                   (current-output-port)
                   port))
         (s   (s: gb))
         (len (string-length s))
         (gap (gap-ofs: gb))
         (aft (aft-ofs: gb)))
    (define (chunk->port! beg end)
      (cond-expand
       (guile-2
        (display (substring/shared s beg end) port))
       (else
        (write-string/pud s port beg end))))
    (cond ((null? opts)
           (chunk->port! 0 gap)
           (chunk->port! aft len))
          (else
           (let* ((beg (1- (car opts)))
                  (end (if (null? (cdr opts))
                           len
                           (let ((v (1- (cadr opts))))
                             (cond ((< gap v)
                                    (+ v (- aft gap)))
                                   (else
                                    ;; adjust first chunk end point
                                    (set! gap v)
                                    #f))))))
             (chunk->port! beg gap)
             (and end (chunk->port! aft end)))))))

;; Return a new string representing the text of @var{gb}.
;; Point does not move.
;;
(define (gb->string gb)
  (let* ((s   (s: gb))
         (len (string-length s))
         (gap (gap-ofs: gb))
         (aft (aft-ofs: gb))
         (rv  (make-string (+ gap (- len aft)))))
    (substring-move! s 0 gap rv 0)
    (substring-move! s aft len rv gap)
    rv))

(define (bsub gb x y)                   ; x and y are 0-based
  (let ((s (s: gb))
        (gap (gap-ofs: gb)))
    (if (< y gap)
        (substring s x y)
        (let ((aft (aft-ofs: gb)))
          (if (< gap x)
              (substring s (+ (- x gap) aft) (+ (- y gap) aft))
              (string-append (substring s x gap)
                             (substring s aft (+ (- y gap) aft))))))))

;; Return the region of @var{gb} from @var{start} to @var{end} as a string.
;;
(define (gb->substring gb start end)
  (or (<= start end) ((lambda (new-end new-start)
                        (set! start new-start)
                        (set! end new-end))
                      start end))
  (or (<= (gb-point-min gb) start)
      (error "arg out of range:" start))
  (or (<= end (gb-point-max gb))
      (error "arg out of range:" end))
  (bsub gb (1- start) (1- end)))

;; Return a list of strings representing the lines of text of @var{gb}.
;; Newlines are automatically removed.  A buffer with N newlines results
;; in a list of length N+1.  Point does not move.
;;
(define (gb->lines gb)
  (let ((s (s: gb))
        (rv '()))
    (define (add! line) (set! rv (cons line rv)))
    (define (splice! line) (set-car! rv (string-append line (car rv))))
    (define (->lines! check-splice? end beg)
      (let loop ((rpos end) (just-found? #f))
        (define (sub lpos)
          (substring s lpos rpos))
        (cond ((string-rindex s #\newline beg rpos)
               => (lambda (i)
                    ((cond ((and check-splice? (not (null? rv)))
                            (set! check-splice? #f)
                            (if (= (1- end) i)
                                identity
                                splice!))
                           (else add!))
                     (sub (1+ i)))
                    (loop i #t)))
              ((and (= beg rpos) check-splice? (not just-found?)))
              (else ((if check-splice?
                         splice!
                         add!)
                     (sub beg))))))
    ;; do it!
    (->lines! #f (string-length s) (aft-ofs: gb))
    (->lines! #t (gap-ofs: gb) 0)
    rv))

;; Return a "soft port" on @var{gb} that supports the write-character,
;; write-string and read-character operations (flush-output and close-port
;; are not supported).  All operations move point forward.  Additionally,
;; writing operations increase the value that would be returned by
;; @code{gb-point-max}.
;;
(define (make-gap-buffer-port gb)
  (or (gb? gb)
      (error "not a gap-buffer:" gb))
  (make-soft-port
   (vector
    (lambda (c) (gb-insert-char! gb c))
    (lambda (s) (gb-insert-string! gb s))
    #f
    (lambda () (let ((gap-ofs (gap-ofs: gb))
                     (aft-ofs (aft-ofs: gb))
                     (s       (s: gb)))
                 (if (= aft-ofs (string-length s))
                     #f
                     (let ((c (string-ref s aft-ofs)))
                       (string-set! s gap-ofs c)
                       (gap-ofs! gb (1+ gap-ofs))
                       (aft-ofs! gb (1+ aft-ofs))
                       c))))
    #f)
   "rw"))

;; Into gap-buffer @var{gb}, insert contents of file @var{filename} after point.
;; Return list of absolute file name and number of characters inserted.
;; If 3rd arg @var{visit} is non-#f, the buffer's visited filename is set.
;; If visiting and the file does not exist, visiting is completed before
;; the error is signaled.
;;
;; Optional 4th and 5th args @var{beg} and @var{end} specify what portion
;; of the file to insert.  These arguments count bytes in the file, not
;; characters in the buffer.  If @var{visit} is non-#f, @var{beg} and
;; @var{end} must be #f.
;;
;; If optional 6th arg @var{replace} is non-#f, replace the current
;; buffer contents with the file contents.  When @var{replace} is
;; non-#f, the second return value is the number of characters that
;; replace previous buffer contents.
;;
;;-args: (- 4 0 visit beg end replace)
;;
(define (gb-insert-file-contents gb filename . opts)
  ;; TODO: Use ‘define*’.

  (define (pop?! def . ok)
    (if (null? opts)
        def
        (let ((val (car opts)))
          (set! opts (cdr opts))
          (if (not val)
              def
              ((if (null? ok)
                   identity
                   (car ok))
               val)))))

  (define (visit-partial-error)
    (error "Attempt to visit less than an entire file"))

  (define (check-file-position-proc visit? avail)
    (lambda (n)
      (and n (begin
               ;; Emacs sez: "If VISIT is non-nil, BEG and END must be nil."
               (and visit? (visit-partial-error))
               (or (integer? n) (error "File position not an integer:" n))
               (or (<= 0 n avail) (error "File position out of range:" n))
               n))))

  (check-read-only gb)
  (set! filename (expand-file-name-substituting-env-vars filename))
  (let* ((visit? (pop?! #f))
         ;; Emacs sez: "If visiting and the file does not exist, visiting
         ;;             is completed before the error is signaled."
         (port (begin (and visit? (set! (filename: gb) filename))
                      (open-input-file filename)))
         (avail (stat:size (stat port)))
         (check-file-pos (check-file-position-proc visit? avail))
         (beg (pop?!     0 check-file-pos))
         (end (pop?! avail check-file-pos))
         (replace? (and (pair? opts) (car opts)))
         (num (- end beg)))
    (and replace? (gb-erase! gb))
    ;; ‘num’ counts bytes, but when we are reading UTF-8, it's possible that a
    ;; character requires more than one byte to represent.  In that case, the
    ;; actual number of characters read will be less than ‘num’, so it's OK to
    ;; prep w/ at least that much.  (Spaciousness is OK.)
    (insert-prep gb num)
    (aft-ofs!
     gb (let ((aft-ofs (aft-ofs: gb)))
          (if (unibyte-port port)
              (let ((new-aft (- aft-ofs num)))
                (seek+read-string!/pud
                 (s: gb) port beg num new-aft)
                new-aft)
              (let* ((sin (if (or visit? (and (zero? beg)
                                              (= avail end)))
                              (drain-port port)
                              (let loop ((cur (seek port beg SEEK_SET))
                                         (acc '()))
                                (if (<= end cur)
                                    (string-concatenate-reverse acc)
                                    (let ((c (read-char port)))
                                      (if (eof-object? c)
                                          (loop end acc)
                                          (loop (seek port 0 SEEK_CUR)
                                                (cons (string c)
                                                      acc))))))))
                     (len (string-length sin))
                     (new-aft (- aft-ofs len)))
                (substring-move! sin 0 len (s: gb) new-aft)
                ;; ‘num’ counts bytes, but we want count of characters.
                (set! num len)
                new-aft))))
    (close-port port)
    ;; Return a list (rather than two values) to be Emacs compatible.
    (list filename num)))

;;; (ttn-do zzz gap-buffer) ends here
