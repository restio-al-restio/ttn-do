;;; ciabattone.scm

;; Copyright (C) 2011, 2012, 2014, 2020 Thien-Thi Nguyen
;;
;; This file is part of ttn-do, released under the terms of the
;; GNU General Public License as published by the Free Software
;; Foundation; either version 3, or (at your option) any later
;; version.  There is NO WARRANTY.  See file COPYING for details.

;;; Code:

(define-module (ttn-do zzz ciabattone)
  #:export (cluster-mangler
            custom-dump
            custom-restore)
  #:use-module ((database postgres) #:select (pg-connectdb
                                              pg-connection?
                                              pg-finish))
  #:use-module ((database postgres-qcons) #:select (sql-quote))
  #:use-module ((srfi srfi-2) #:select (and-let*))
  #:use-module ((ice-9 optargs) #:select (lambda*
                                          define*
                                          let-optional*
                                          let-keywords*))
  #:use-module ((ttn-do mogrify) #:select (find-file
                                           editing-buffer))
  #:use-module ((ttn-do rm-rf) #:select (rm-rf!))
  #:use-module ((ttn-do zzz personally) #:select (fs fso fse))
  #:use-module ((ttn-do zzz filesystem) #:select (directory-vicinity
                                                  dir-exists?))
  #:use-module ((ttn-do zzz subprocess) #:select (call-process
                                                  sysfmt)))

;; Return a procedure @var{p} that manages the cluster at/under
;; @var{root}, an absolute directory name.
;; @var{p} accepts a single arg, @var{command}, a symbol.
;; All commands return non-@code{#f} if successful.
;; Optional arg @var{tweaks} is for command @code{make-cluster},
;; described below.
;; Recognized commands (keywords ok, too):
;;
;; @table @code
;; @item cluster-exists?
;; Return non-@code{#f} if the cluster (directory structure) exists.
;;
;; @item make-cluster
;; If the directory @var{root} does not exist, create the cluster
;; with encoding @code{UNICODE} via external command @t{initdb}
;; under @var{root} and modify two files therein:
;;
;; @table @file
;; @item pg_hba.conf
;; Set the contents to @code{local all @var{login} trust}, where
;; @var{login} is for the current user, per @code{(getpwuid (getuid))}.
;;
;; @item postgresql.conf
;; Set option @code{listen_addresses} (if present) to nothing,
;; and the option @code{unix_socket_directory} to @var{root}.
;; If @var{tweaks} is specified, it should be a list of @dfn{spec}
;; forms @code{(@var{option} . @var{value})} where @var{option} is
;; a symbol and @var{value} is a string, to set additionally.
;; Normally, if @var{option} is not found in the file, it is ignored.
;; You can force it to be added at the end, however, by using the
;; form @code{(#t @var{spec})}, that is, consing a @code{#t} onto
;; a normal spec form.
;; @end table
;;
;; The overall effect of these changes is to disable network access
;; and allow only (albeit further unrestricted) Unix-domain access.
;;
;; @item daemon-running?
;; Return non-@code{#f} if the daemon is up.
;;
;; @item daemon-up
;; First, make sure the cluster exists (via @code{make-cluster}).
;; If creation fails signal ``make-cluster FAILED'' error.
;; Then, if the daemon is not up, start it (like @t{pg_ctl start}),
;; specifying @file{@var{root}/log} as the log file, and attempt
;; (up to five times) to connect to database @code{template1}.
;; If all attempts fail, display a message to the current error
;; port and return @code{#f}.
;;
;; @item daemon-down
;; If the daemon is up, bring it down (like @t{pg_ctl stop}).
;;
;; @item drop-cluster
;; First, make sure the daemon is down.  If the daemon
;; cannot be taken down, signal ``daemon-down FAILED'' error.
;; Then, delete @var{root} and all files under it.
;;
;; @item tweaker
;; This command does not do anything; instead, it returns a
;; procedure @var{p} that takes one arg, @var{tweaks}, a list of
;; spec forms as described in @code{make-cluster}, above.
;; Procedure @var{p} actually does the work of modifying
;; @file{postgresql.conf} (and saving it).
;; Why have two ways of doing the same thing?
;; Good question.
;; @end table
;;
;; Other commands signal ``bad command'' error.  Argument @var{who}
;; is either @code{#f} to specify silent operation, or a short string
;; to be used for certain progress messages.
;;
;;-args: (- 1 0 tweaks)
;;
(define (cluster-mangler who root . options)

  (define (validate-tweaks ls)
    (define (valid? pair)
      (and (symbol? (car pair))
           (string? (cdr pair))))
    (or (and (list? ls)
             (and-map pair? ls)
             (and-map (lambda (x)
                        (or (valid? x)
                            (and (eq? #t (car x))
                                 (valid? (cdr x)))))
                      ls)
             ls)
        (error "bad tweaks:" ls)))

  ;; First do some sanity checks.
  (and (or (not (string? root))
           (string-null? root))
       (error "invalid directory:" root))
  (or (char=? #\/ (string-ref root 0))
      (error "not absolute:" root))
  (or (not (file-exists? root))
      (dir-exists? root)
      (error "not a directory:" root))
  ;; OK, do it!
  (let ((spew (if who
                  (lambda args
                    (fso "~A: " who)
                    (apply fso args)
                    (newline))
                  (lambda ignored
                    #f)))
        (tweaks (if (pair? options)
                    ;; yes, validate
                    (validate-tweaks (car options))
                    ;; no, life goes on
                    '()))
        (mum (if who "" "1>/dev/null 2>&1 "))
        (cfile (directory-vicinity root)))

    (define (sysok? program . args)
      (zero? (call-process
              program #:args args
              #:outp (and who (current-output-port)))))

    (define (cluster-exists?)
      (dir-exists? root))

    (define (daemon-running?)
      (and-let* ((pidfile (cfile "postmaster.pid"))
                 ((file-exists? pidfile))
                 (pid (editing-buffer #t
                        (insert-file-contents pidfile #f 0 15)
                        (end-of-line)
                        (buffer-substring (point-min) (point)))))
        (or (sysok? "ps" "ww" pid)
            (begin (spew "pid ~A stale, deleting ~A" pid pidfile)
                   (delete-file pidfile)
                   #f))))

    (define (find-cfile filename)
      (find-file (cfile filename)))

    (define (tweak-configuration init? tweaks)
      (editing-buffer (find-cfile "postgresql.conf")
        (define (replace-value x)
          (let ((p (point)))
            (end-of-line)
            (delete-region p (point)))
          (insert " = " (sql-quote x))
          (beginning-of-line)
          (and (looking-at "#+")
               (replace-match "")))
        (cond (init?
               ;; This is for PostgreSQL 8.x and later.
               (and (search-forward "listen_addresses" #f #t)
                    (replace-value ""))
               ;; The classic.  PostgreSQL 9.3 and later
               ;; use ‘unix_socket_directories’ hence the rx.
               (re-search-forward "unix_socket_dir[a-z]+")
               (replace-value root)))
        ;; Handle tweaks.
        (let loop ((ls tweaks))
          (or (null? ls)
              (let* ((spec (car ls))
                     (force? (and (eq? #t (car spec))
                                  (pair? (cdr spec))
                                  (begin (set! spec (cdr spec))
                                         #t)))
                     (opt (symbol->string (car spec)))
                     (val (cdr spec))
                     (hit? (begin
                             (goto-char (point-min))
                             (search-forward opt))))
                (cond (hit?
                       (replace-value val))
                      (force?
                       (goto-char (point-max))
                       (insert "\n" opt " = "
                               (sql-quote val)
                               "\n")))
                (loop (cdr ls)))))
        ;; OK, done.
        (save-buffer)
        #t))

    (define (make-cluster)
      (or (cluster-exists?)
          (and (sysok? "initdb"
                       "-E" "UNICODE"
                       "-D" root)
               (editing-buffer (find-cfile "pg_hba.conf")
                 (erase-buffer)
                 (insert "local"
                         "  all"
                         "  " (passwd:name (getpwuid (getuid)))
                         "  trust"
                         "\n")
                 (save-buffer)
                 #t)
               (tweak-configuration #t tweaks))))

    (define (daemon-up)

      (define (connection)
        (pg-connectdb (fs "host=~A dbname=template1" root)))

      (define (all-done conn)
        ;; This awaits useful return value from ‘pg-finish’.
        (pg-finish conn)
        (string=? "#<PG-CONN:->" (object->string conn)))

      (or (cluster-exists?)
          (make-cluster)
          (error "make-cluster FAILED"))
      (or (daemon-running?)
          (and (sysok? "pg_ctl" "start"
                       "-D" root
                       "-l" (cfile "log"))
               (let loop ((n 15))
                 (let ((conn (false-if-exception ; lame
                              (connection))))
                   (cond ((pg-connection? conn)
                          (spew "connected!")
                          (all-done conn))
                         ((zero? n)
                          (fse "~AERROR: cannot connect!"
                               (if who
                                   (fs "~A: " who)
                                   ""))
                          #f)
                         (else
                          (spew "(no connection yet)...")
                          (sleep 1)
                          (loop (1- n)))))))))

    (define (daemon-down)
      (or (not (daemon-running?))
          (sysok? "pg_ctl" "stop"
                  "-D" root)))

    (define (drop-cluster)
      (or (not (daemon-running?))
          (daemon-down)
          (error "daemon-down FAILED"))
      (cond ((not (cluster-exists?)))
            (else (rm-rf! who root)
                  #t)))

    ;; rv
    (lambda (command)
      (case (if (keyword? command)
                (keyword->symbol command)
                command)
        ((cluster-exists?) (cluster-exists?))
        ((make-cluster) (make-cluster))
        ((daemon-running?) (daemon-running?))
        ((daemon-up) (daemon-up))
        ((daemon-down) (daemon-down))
        ((drop-cluster) (drop-cluster))
        ((tweaker) (lambda (tweaks)
                     (tweak-configuration #f (validate-tweaks tweaks))))
        (else (error "bad command:" command))))))

(define (sysok? s . args)
  (zero? (apply sysfmt s args)))

(define (maybe-minus-h host)
  (if host
      (fs " -h ~S" host)
      ""))

;; Dump database @var{db-name} to @var{outfile} in ``custom''
;; format with maximum compression (via @samp{pg_dump}).
;; Return @code{#t} if successful, @code{#f} otherwise.
;; Keyword args:
;;
;; @table @var
;; @item host
;; Specify also @samp{-h @var{host}} to @samp{pg_dump}.
;;
;; @item listing
;; After successfully generating @var{outfile}, also write
;; a listing (via @samp{pg_restore -l}) to @var{listing} (a filename).
;; @end table
;;
(define* (custom-dump db-name outfile #:key host listing)
  (and (sysok? "pg_dump~A -F c -Z 9 ~A > ~A"
               (maybe-minus-h host)
               db-name outfile)
       (or (not listing)
           (sysok? "pg_restore -l ~A > ~A"
                   outfile listing))))

;; Restore database @var{db-name} (via @samp{pg_restore})
;; from @var{infile}, which should be in ``custom'' format.
;; Return @code{#t} if successful, @code{#f} otherwise.
;; Keyword args:
;;
;; @table @var
;; @item host
;; Specify also @samp{-h @var{host}} to @samp{pg_restore}.
;;
;; @item listing
;; Specify also @samp{-L @var{listing}} to @samp{pg_restore}.
;;
;; @item create?
;; If non-@code{#f}, specify also @samp{-C} to @samp{pg_restore}.
;; @end table
;;
(define* (custom-restore db-name infile #:key host listing create?)
  (sysok? "pg_restore~A~A~A -d ~A ~A"
          (maybe-minus-h host)
          (if listing (fs " -L ~S" listing) "")
          (if create? " -C" "")
          db-name infile))

;;; ciabattone.scm ends here
