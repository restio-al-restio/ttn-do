;;; (ttn-do zzz 0gx string-io)

;; Copyright (C) 2017 Thien-Thi Nguyen
;;
;; This file is part of ttn-do, released under the terms of the
;; GNU General Public License as published by the Free Software
;; Foundation; either version 3, or (at your option) any later
;; version.  There is NO WARRANTY.  See file COPYING for details.

;;; Commentary:

;; "/pud" stands for "partial, until done".  The "partial" describes
;; the string access -- the range [START,END).  The "until done"
;; describes the control aspect, on short read/write, update START
;; and loop.
;;
;; This module is ‘(... 0gx ...)’ and thus undocumented.  Probably it
;; could be moved into Guile-BAUX proper, after the slightly-unsmooth
;; ‘len’ handling is resolved (e.g., see ‘seek+read-string!/pud’),
;; and once Guile 2.x considerations are, uh, considered.  :-/

;;; Code:

(define-module (ttn-do zzz 0gx string-io)
  #:export (read-string!/pud
            string-in-proc
            write-string/pud
            string-out-proc)
  #:use-module ((ice-9 rw) #:select (read-string!/partial
                                     write-string/partial)))

;; Call @code{read-string!/partial} on @var{str}, @var{port/fd}
;; @var{start}, @var{end}, repeatedly until ``done''.
;; Note that this presumes @code{read-string!/partial}
;; always returns a numeric value.
;;
(define (read-string!/pud str port/fd start end)
  (let loop ((pos start))
    (and (< pos end)
         (loop (+ pos (read-string!/partial
                       str port/fd pos end))))))

;; Return a procedure that takes arg @var{port/fd}.
;; This procedure fills the first @var{len} characters of string
;; @var{str} from @var{port/fd} and returns @var{str}.
;; If @var{len} is unspecified, fill the entire @var{str}.
;;
;;-args: (- 1 0)
;;
(define (string-in-proc str . len)      ; => (lambda (port/fd) ...)
  (set! len (if (pair? len)
                (car len)
                (string-length str)))
  (lambda (port/fd)                     ; => STR
    (read-string!/pud str port/fd 0 len)
    str))

;; Call @code{write-string/partial} on @var{str}, @var{port/fd}
;; @var{start}, @var{end}, repeated until ``done''.
;; Note that this presumes @code{write-string/partial}
;; always returns a numeric value.
;;
(define (write-string/pud str port/fd start end)
  (let loop ((pos start))
    (and (< pos end)
         (loop (+ pos (write-string/partial
                       str port/fd pos end))))))

;; Return a procedure that takes arg @var{port/fd}.
;; This procedure writes the first @var{len} characters of string
;; @var{str} to @var{port/fd} and returns @var{str}.
;; If @var{len} is unspecified, write the entire @var{str}.
;;
;;-args: (- 1 0)
;;
(define (string-out-proc str . len)     ; => (lambda (port/fd) ...)
  (set! len (if (pair? len)
                (car len)
                (string-length str)))
  (lambda (port/fd)                     ; => STR
    (write-string/pud str port/fd 0 len)
    str))

;;; (ttn-do zzz 0gx string-io) ends here
