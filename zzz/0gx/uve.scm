;;; (ttn-do zzz 0gx uve)

;; Copyright (C) 2013, 2019 Thien-Thi Nguyen
;;
;; This file is part of ttn-do, released under the terms of the
;; GNU General Public License as published by the Free Software
;; Foundation; either version 3, or (at your option) any later
;; version.  There is NO WARRANTY.  See file COPYING for details.

;;; Commentary:

;;; Code:

(define-module (ttn-do zzz 0gx uve)
  #:export (make-uve
            uve-length
            uve-set!
            uve-ref
            uve-type
            HOST-ENDIANNESS
            read-network-uve-proc))

(cond-expand
 (guile-2                               ; FIXME: integrate
  (use-modules
   (rnrs bytevectors)
   (rnrs io ports)))
 (else #f))

(define make-uve
  (cond-expand
   (guile-2
    make-typed-array)
   (else
    (if (defined? 'make-typed-array)
        make-typed-array
        (lambda (tag fill len)
          (make-uniform-vector
           len (case tag
                 ((u8) #\nul)
                 ((s8) #\a)             ; is this right?
                 ((s16
                   ;; NB: This is a kludge; client code MUST
                   ;;     compensate on vref via range shifting:
                   ;;     (if (negative? VALUE)
                   ;;         (+ 65536 VALUE)
                   ;;         VALUE)
                   u16) 's)
                 ((u32) 1)              ; assumes 32-bit host!
                 ((s32) -1)             ; likewise
                 ((s64) 'l)             ; likewise
                 (else (error "(ttn-do zzz 0gx uve) unsupported tag:" tag)))
           fill))))))

(define uve-length
  (cond-expand (guile-2 array-length)
               (else uniform-vector-length)))

(define uve-set!
  (cond-expand (guile-2 (lambda (uve idx val)
                          (array-set! uve val idx)))
               (else uniform-vector-set!)))

(define uve-ref
  (cond-expand (guile-2 array-ref)
               (else uniform-vector-ref)))

(define uve-type
  (cond-expand
   (guile-2
    array-type)
   (else
    (if (defined? 'array-type)
        array-type
        (lambda (uve)
          (case (array-prototype uve)
            ((#\nul) 'u8)
            ((#\a)   'a)
            ((s)     's16)
            ((1)     'u32)
            ((-1)    's32)
            ((l)     's64)
            (else (error "(ttn-do zzz 0gx uve) unsupported prototype:"
                         (array-prototype uve)))))))))

(define HOST-ENDIANNESS
  (cond-expand (guile-2 (native-endianness))
               (else (if (= 1 (ntohs 1)) 'big 'little))))

(define (read-network-uve-proc type)
  (let* ((uve (make-uve type 0 1))
         (size (case type
                 ((u32 s32) 4)
                 ((u16 s16) 2))))

    (define fill!
      (cond-expand

       (guile-2 get-bytevector-n!)

       (else (lambda (port uve . ignored)
               (uniform-vector-read! uve port)))))

    (define ref
      (cond-expand

       (guile-2 (case type
                  ((u32 u16) bytevector-uint-ref)
                  ((s32 s16) bytevector-sint-ref)))

       (else (let ((swizzle (case size
                              ((4) ntohl)
                              ((2) ntohs))))
               (lambda (uve idx . ignored)
                 (swizzle (uve-ref uve idx)))))))

    ;; retval
    (lambda (port)
      (fill! port uve 0 size)
      (ref uve 0 (endianness big) size))))

;;; (ttn-do zzz 0gx uve) ends here
