;;; record.eaab (from xcb-proto-1.0/src/record.xml)

;; Copyright (C) 2010 Thien-Thi Nguyen
;; Copyright (C) 2005 Jeremy Kolb.
;; All Rights Reserved.
;; 
;; Permission is hereby granted, free of charge, to any person ob/Sintaining a copy
;; of this software and associated documentation files (the "Software"), to deal
;; in the Software without restriction, including without limitation the rights
;; to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
;; copies of the Software, and to permit persons to whom the Software is
;; furnished to do so, subject to the following conditions:
;; 
;; The above copyright notice and this permission notice shall be included in all
;; copies or substantial portions of the Software.
;; 
;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
;; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
;; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
;; AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
;; ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
;; WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
;; 
;; Except as contained in this notice, the names of the authors or their
;; institutions shall not be used in advertising or otherwise to promote the
;; sale, use or other dealings in this Software without prior written
;; authorization from the authors.

(xcb ((header . record)
      (extension-xname . RECORD)
      (extension-name . Record)
      (major-version . 1)
      (minor-version . 13))
     (xidtype ((name . CONTEXT)))
     (struct
       ((name . Range8))
       (field ((type . CARD8) (name . first)))
       (field ((type . CARD8) (name . last))))
     (struct
       ((name . Range16))
       (field ((type . CARD16) (name . first)))
       (field ((type . CARD16) (name . last))))
     (struct
       ((name . ExtRange))
       (field ((type . Range8) (name . major)))
       (field ((type . Range16) (name . minor))))
     (struct
       ((name . Range))
       (field ((type . Range8) (name . core-requests)))
       (field ((type . Range8) (name . core-replies)))
       (field ((type . ExtRange) (name . ext-requests)))
       (field ((type . ExtRange) (name . ext-replies)))
       (field ((type . Range8) (name . delivered-events)))
       (field ((type . Range8) (name . device-events)))
       (field ((type . Range8) (name . errors)))
       (field ((type . BOOL) (name . client-started)))
       (field ((type . BOOL) (name . client-died))))
     (typedef
       ((oldname . CARD8) (newname . ElementHeader)))
     (enum ((name . HType))
           (item ((name . FromServerTime)) (value () 1))
           (item ((name . FromClientTime)) (value () 2))
           (item ((name . FromClientSequence)) (value () 4)))
     (typedef
       ((oldname . CARD32) (newname . ClientSpec)))
     (enum ((name . CS))
           (item ((name . CurrentClients)) (value () 1))
           (item ((name . FutureClients)) (value () 2))
           (item ((name . AllClients)) (value () 3)))
     (struct
       ((name . ClientInfo))
       (field ((type . ClientSpec) (name . client-resource)))
       (field ((type . CARD32) (name . num-ranges)))
       (list ((type . Range) (name . ranges))
             (fieldref () num-ranges)))
     (error ((name . BadContext) (number . 0))
            (field ((type . CARD32) (name . invalid-record))))
     (request
       ((name . QueryVersion) (opcode . 0))
       (field ((type . CARD16) (name . major-version)))
       (field ((type . CARD16) (name . minor-version)))
       (reply ()
              (pad ((bytes . 1)))
              (field ((type . CARD16) (name . major-version)))
              (field ((type . CARD16) (name . minor-version)))))
     (request
       ((name . CreateContext) (opcode . 1))
       (field ((type . record:CONTEXT) (name . context)))
       (field ((type . ElementHeader) (name . element-header)))
       (pad ((bytes . 3)))
       (field ((type . CARD32) (name . num-client-specs)))
       (field ((type . CARD32) (name . num-ranges)))
       (list ((type . ClientSpec) (name . client-specs))
             (fieldref () num-client-specs))
       (list ((type . Range) (name . ranges))
             (fieldref () num-ranges)))
     (request
       ((name . RegisterClients) (opcode . 2))
       (field ((type . record:CONTEXT) (name . context)))
       (field ((type . ElementHeader) (name . element-header)))
       (pad ((bytes . 3)))
       (field ((type . CARD32) (name . num-client-specs)))
       (field ((type . CARD32) (name . num-ranges)))
       (list ((type . ClientSpec) (name . client-specs))
             (fieldref () num-client-specs))
       (list ((type . Range) (name . ranges))
             (fieldref () num-ranges)))
     (request
       ((name . UnregisterClients) (opcode . 3))
       (field ((type . record:CONTEXT) (name . context)))
       (field ((type . CARD32) (name . num-client-specs)))
       (list ((type . ClientSpec) (name . client-specs))
             (fieldref () num-client-specs)))
     (request
       ((name . GetContext) (opcode . 4))
       (field ((type . record:CONTEXT) (name . context)))
       (reply ()
              (field ((type . BOOL) (name . enabled)))
              (field ((type . ElementHeader) (name . element-header)))
              (pad ((bytes . 3)))
              (field ((type . CARD32)
                      (name . num-intercepted-clients)))
              (pad ((bytes . 16)))
              (list ((type . ClientInfo)
                     (name . intercepted-clients))
                    (fieldref () num-intercepted-clients))))
     (request
       ((name . EnableContext) (opcode . 5))
       (field ((type . record:CONTEXT) (name . context)))
       (reply ()
              (field ((type . CARD8) (name . category)))
              (field ((type . ElementHeader) (name . element-header)))
              (field ((type . BOOL) (name . client-swapped)))
              (pad ((bytes . 2)))
              (field ((type . CARD32) (name . xid-base)))
              (field ((type . CARD32) (name . server-time)))
              (field ((type . CARD32) (name . rec-sequence-num)))
              (pad ((bytes . 8)))
              (list ((type . BYTE) (name . data)))))
     (request
       ((name . DisableContext) (opcode . 6))
       (field ((type . record:CONTEXT) (name . context))))
     (request
       ((name . FreeContext) (opcode . 7))
       (field ((type . record:CONTEXT) (name . context)))))

;;; record.eaab ends here
