;;; txtoutline.scm

;; Copyright (C) 2002, 2007, 2008, 2010-2013, 2020 Thien-Thi Nguyen
;;
;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this package.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; The proc ‘txtoutline-reader’ was previously in (ttn-do todo)
;; as ‘make-text-outline-reader’, which was itself a reformulation
;; of ‘(scripts read-text-outline) make-text-outline-reader’ from
;; (Unofficial) Guile 1.4.x.

;;; Code:

(define-module (ttn-do zzz txtoutline)
  #:export (txtoutline-reader)
  #:use-module ((srfi srfi-13) #:select (string-prefix?
                                         string-suffix?
                                         string-trim-right))
  #:use-module ((ice-9 regex) #:select (match:suffix
                                        match:substring))
  #:use-module ((ttn-do mogrify) #:select (editing-buffer))
  #:use-module ((ttn-do zzz personally) #:select (FE)))

(define put set-object-property!)
(define get object-property)

;; Return a proc that reads from @var{port} and returns a list of trees.
;; Scanning recognizes an outline format where each heading is zero or
;; more pairs of leading spaces followed by @samp{-} (hyphen).
;; Something like:
;;
;; @example
;; - a                  0
;;   - b                1
;;     - c              2
;;   - d                1
;; - e                  0
;;   - f                1
;;     - g              2
;;   - h                1
;; @end example
;;
;; In this example the levels are shown to the right.  The result for
;; such a file would be the form:
;;
;;   (("a" ("b" "c") "d") ("e" ("f" "g") "h"))
;;
;; Basically, anything at the beginning of a list is a parent, and the
;; remaining elements of that list are its children.  @strong{NOTE}:
;; Outlines that ``skip'' levels signal an error.  In other words, this
;; will fail:
;;
;; @example
;; - a               0
;;   - b             1
;;       - c         3       <-- skipped 2 -- error!
;;   - d             1
;; @end example
;;
;; @var{re} is a regular expression (string) that is used to identify a
;; header line of the outline, as opposed to a whitespace line or
;; intervening text.  @var{re} must begin w/ a sub-expression to match
;; the @dfn{level prefix} of the line.  You can use
;; @code{level-submatch-number} in @var{specs} (explained below) to
;; specify a number other than 1, the default.
;;
;; Normally, the level of the line is taken directly as the length of
;; its level prefix.  This often results in adjacent levels not mapping
;; to adjacent numbers, which confuses the tree-building portion of the
;; program, which expects top-level to be 0, first sub-level to be 1,
;; etc.  You can use @code{level-substring-divisor} or
;; @code{compute-level} in @var{specs} to specify a constant scaling
;; factor or specify a completely alternative procedure, respectively.
;;
;; @var{specs} is an alist which may contain the following key/value pairs:
;;
;; @example
;; - level-submatch-number @var{number}
;; - level-substring-divisor @var{number}
;; - compute-level @var{proc}
;; - body-submatch-number @var{number}
;; - extra-fields ((@var{field-1} . @var{submatch-1})
;;                 (@var{field-2} . @var{submatch-2}) ...)
;; - more-lines? @var{bool}
;; @end example
;;
;; The @var{proc} value associated with key @code{compute-level} should
;; take a Scheme match structure (as returned by @code{regexp-exec}) and
;; return a number, the normalized level for that line.  If this is
;; specified, it takes precedence over other level-computation methods.
;;
;; Use @code{body-submatch-number} if @var{re} specifies the whole body,
;; or if you want to make use of the extra fields parsing.  The
;; @code{extra-fields} value is a sub-alist, whose keys name additional
;; fields that are to be recognized.  These fields along with
;; @code{level} are set as object properties of the final string (the
;; @dfn{body}) that is consed into the tree.  If a field name ends in
;; @samp{?} (question mark) the field value is set to be @code{#t} if
;; there is a match and the result is not an empty string, and @code{#f}
;; otherwise.
;;
;; Normally, only header lines are considered.  Use @code{more-lines?}
;; with a non-@code{#f} value to specify that non-header lines be
;; included as well.  The structure of the output will be the same,
;; however headers that are not immediately followed by another will
;; result in strings with embedded newline characters and all trailing
;; whitespace (as per SRFI 13 ‘string-trim-right’) removed.
;;
(define (txtoutline-reader re specs)

  (define (check key)
    (assq-ref specs key))

  (define (msub n)
    (lambda (m)
      (match:substring m n)))

  (define (??-predicates pair)
    (cons (car pair)
          (if (string-suffix? "?" (symbol->string (car pair)))
              (lambda (m)
                (not (string-null? (match:substring m (cdr pair)))))
              (msub (cdr pair)))))

  (let* ((rx (make-regexp (if (string-prefix? "^" re)
                              re
                              (string-append "^" re))))
         (level-substring (msub (or (check 'level-submatch-number) 1)))
         (one? (not (check 'more-lines?))))

    (define extract-level
      (cond ((check 'compute-level)
             => (lambda (proc)
                  (lambda (m)
                    (proc m))))
            ((check 'level-substring-divisor)
             => (lambda (n)
                  (lambda (m)
                    (/ (string-length (level-substring m))
                       n))))
            (else
             (lambda (m)
               (string-length (level-substring m))))))

    (define extract-body
      (cond ((check 'body-submatch-number)
             => msub)
            (else
             (lambda (m) (match:suffix m)))))

    (define misc-props!
      (cond ((check 'extra-fields)
             => (lambda (alist)
                  (let ((new (map ??-predicates alist)))
                    (lambda (obj m)
                      (FE new (lambda (pair)
                                (put obj (car pair)
                                     ((cdr pair) m))))))))
            (else
             (lambda (obj m)
               (and (or obj m) #t)))))

    (define (parse-line line)
      (and=> (regexp-exec rx line)
             (lambda (m)
               (let ((level (extract-level m))
                     (body  (extract-body m)))
                 (put body 'level level)
                 (misc-props! body m)
                 body))))

    ;; retval
    (lambda (port)
      (let* ((buf (editing-buffer port
                    (goto-char (point-min))
                    (current-buffer)))
             (all '(start))
             (pchain (list)))           ; parents chain

        (let loop ((prev-level -1)      ; How this relates to the first input
                                        ; level determines whether or not we
                                        ; start in "sibling" or "child" mode.
                                        ; In the end, ‘start’ is ignored and
                                        ; it's much easier to ignore parents
                                        ; than siblings (sometimes).  This is
                                        ; not to encourage ignorance, however.
                   (tp all))

          (define line
            (editing-buffer buf
              (and (< (point) (point-max))
                   (let ((p (point)))
                     (forward-line 1)
                     (or one? (let another ()
                                (cond ((eobp))
                                      ((looking-at re))
                                      (else (forward-line 1)
                                            (another)))))
                     (string-trim-right (buffer-substring p (point)))))))

          (cond ((not line))
                ((parse-line line)
                 => (lambda (w)
                      (let* ((words (list w))
                             (level (get w 'level))
                             (diff (- level prev-level)))
                        (cond

                         ;; sibling
                         ((zero? diff)
                          ;; Just extend the chain.
                          (set-cdr! tp words))

                         ;; child
                         ((positive? diff)
                          (or (= 1 diff)
                              (error "unhandled diff not 1:" diff line))
                          ;; Parent may be contacted by uncle later (kids
                          ;; these days!) so save its level.
                          (put tp 'level prev-level)
                          (set! pchain (cons tp pchain))
                          ;; "Push down" car into hierarchy.
                          (set-car! tp (cons (car tp) words)))

                         ;; uncle
                         ((negative? diff)
                          ;; Prune back to where levels match.
                          (do ((p pchain (cdr p)))
                              ((= level (get (car p) 'level))
                               (set! pchain p)))
                          ;; Resume at this level.
                          (set-cdr! (car pchain) words)
                          (set! pchain (cdr pchain))))

                        (loop level words))))
                (else (loop prev-level tp))))
        (set! all (car all))
        (if (eq? 'start all)
            '()                         ; wasteland
            (cdr all))))))

;;; txtoutline.scm ends here
