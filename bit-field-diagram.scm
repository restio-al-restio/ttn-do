#!/bin/sh
exec ${GUILE-guile} -e '(ttn-do bit-field-diagram)' -s $0 "$@" # -*- scheme -*-
!#
;;; bit-field-diagram --- Display bit-field diagrams in different ways

;; Copyright (C) 2002-2004, 2007, 2011, 2019-2021 Thien-Thi Nguyen
;;
;; This file is part of ttn-do, released under the terms of the
;; GNU General Public License as published by the Free Software
;; Foundation; either version 3, or (at your option) any later
;; version.  There is NO WARRANTY.  See file COPYING for details.

;;; Commentary:

;; Usage: bit-field-diagram [--file FILE | BITSPEC]
;;
;; Display a bit field diagram representing BITSPEC.  Option `--file'
;; means read the bitspec from FILE.  A BITSPEC is a Scheme list of
;; the form:
;;
;;   (CONFIGURATION* [FIELD-SPEC...])
;;
;; CONFIGURATION is an optionally-omitted list with car #:config
;; followed by CONFIG-SPEC forms, described below.  FIELD-SPEC is a
;; list (NAME WIDTH [BITS]), where NAME is a symbol or string, WIDTH
;; an integer, and BITS an optional string to populate the diagram.
;; Each character in BITS is displayed (left to right) in its
;; corresponding field position.
;;
;; For example:
;; $ ttn-do bit-field-diagram "((hello 5) (there 10))"
;;  hello     there
;; +---------+-------------------+
;; | | | | | | | | | | | | | | | |
;; +---------+-------------------+
;;
;; Specifying CONFIG-SPEC forms, each a keyword/value pair, changes
;; the appearance of the output.  Currently supported:
;;
;;   #:columns-per-bit       2       this includes a bar "|"
;;   #:suppress-sep-bars     #f      omit internal "|"
;;   #:field-name-justify    #:left  can also be #:right
;;   #:field-name-placement  #:over  can also be #:internal or #:under
;;   #:height                1       lines for the body of the field
;;   #:group                 #f      number of bits in a group, or #f
;;   #:merge-group-borders?  #f      merge internal borders
;;
;; When #:merge-group-borders? is set, #:field-name-placement must be
;; #:internal, otherwise an error is signalled.  A #:field-name-placement
;; of #:internal automatically sets #:suppress-sep-bars.  You must set
;; #:suppress-sep-bars when specifying field BITS.  When #:group is
;; specified, an error is signalled if the fields do not align to it.
;;
;; Here is a more complicated example.  This form is placed in a file:
;;
;;   ((#:config (#:columns-per-bit . 3)
;;              (#:field-name-justify . #:right)
;;              (#:field-name-placement . #:internal)
;;              (#:height . 3)
;;              (#:group . 16)
;;              (#:merge-group-borders? . #t))
;;    (hello 6)
;;    (there 10)
;;    (how 2)
;;    (are 3)
;;    (you 8)
;;    (doing 3)
;;    ("well i hope?" 11))
;;
;; The result is:
;;
;;   +-----------------+-----------------------------+
;;   |            hello|                        there|
;;   |                 |                             |
;;   |                 |                             |
;;   +-----+--------+--+--------------------+--------+
;;   |  how|     are|                    you|   doing|
;;   |     |        |                       |        |
;;   |     |        |                       |        |
;;   +-----+--------+-----------------+-----+--------+
;;   |                    well i hope?|
;;   |                                |
;;   |                                |
;;   +--------------------------------+
;;
;;
;; TODO: Support #:group-justify, #:border-style, #:bit-numbering,
;;               #:endian, per-group #:prefix and #:suffix, etc. etc.

;;; Code:

(define-module (ttn-do bit-field-diagram)
  #:export (main bit-field-diagram)
  #:use-module ((ttn-do zzz banalities) #:select (check-hv
                                                  decry
                                                  qop<-args))
  #:use-module ((ttn-do zzz personally) #:select (FE whatever))
  #:use-module ((ice-9 format) #:select (format)))

(define name:  car)
(define width: cadr)
(define (bits: x)
  (if (null? (cddr x))
      ""
      (caddr x)))

(define *default-config*
  '((#:columns-per-bit . 2)
    (#:suppress-sep-bars . #f)
    (#:field-name-justify . #:left)
    (#:field-name-placement . #:over)
    (#:height . 1)
    (#:group . #f)
    (#:merge-group-borders? . #f)))

(define (default-config)
  (map copy-tree *default-config*))

(define (bf-description<-port/spec port/spec)
  (let ((cfg (default-config))
        (raw (if (port? port/spec)
                 (read port/spec)
                 port/spec)))
    (cond ((and (pair? raw) (pair? (car raw))
                (eq? #:config (caar raw))
                (cdar raw))
           => (lambda (overrides)
                (set! raw (cdr raw))
                (FE overrides
                    (lambda (pair)
                      (assq-set! cfg (car pair) (cdr pair)))))))
    (cons cfg raw)))

(define (merge-widths ls-1 ls-2)
  (define (as-cols ls)
    (let loop ((ls ls) (acc (list 0)))
      (if (null? ls)
          (cdr (reverse! acc))
          (loop (cdr ls) (cons (+ (car ls) (car acc)) acc)))))
  (let loop ((ls (sort (append! (as-cols ls-1) (as-cols ls-2)) <))
             (prev-head 0)
             (acc '()))
    (if (null? ls)
        (reverse! acc)                  ; rv
        (let* ((head (car ls))
               (tail (cdr ls))
               (skip? (and (pair? tail) (= head (car tail)))))
          (loop tail
                (if skip? prev-head head)
                (if skip? acc (cons (- head prev-head) acc)))))))

;; Format a bit-field diagram to @var{out}, normally a port.  As a
;; special case, if @var{out} is @code{#f}, construct and return a new
;; string.  @var{port/spec} is either a port where a specification can
;; be @code{read}, or a specification (structured expression) itself.
;;
(define (bit-field-diagram out port/spec)

  (define (fo s . args)
    (apply format out s args))

  (define (return)
    (whatever (flush-all-ports)))

  (let* ((bf-description (bf-description<-port/spec port/spec))
         (config (car bf-description))
         (C      (lambda (key) (assq-ref config key)))
         (fspecs (cdr bf-description))
         (cols/b (C #:columns-per-bit))
         (name-placement (C #:field-name-placement))
         (name-delim (if (eq? #:internal name-placement) "|" " "))
         (field-name-format (string-append
                             name-delim "~:{~V"
                             (case (C #:field-name-justify)
                               ((#:left) "")
                               ((#:right) "@")
                               (else ""))
                             "A" name-delim "~}~%"))
         (groups #f)
         (merge-borders? (C #:merge-group-borders?)))

    (define (>>plus-dash-1 widths)
      (fo "+~:{~V,,,'-A+~}~%"
          (map (lambda (w)
                 (list (1- (* cols/b w)) ""))
               widths)))

    ;; sanity checks
    (and merge-borders?
         (not (eq? #:internal name-placement))
         (decry "incompatible: ~A and ~A ~A"
                #:merge-group-borders?
                #:field-name-placement
                name-placement))
    ;; check grouping
    (cond ((C #:group)
           => (lambda (n)
                (let loop ((ls fspecs) (partial 0) (acc '()))
                  (if (null? ls)
                      (or (null? acc)
                          (set! groups (cons (reverse! acc) groups)))
                      (let* ((head (car ls))
                             (one (width: head))
                             (so-far (+ one partial)))
                        (cond ((< so-far n)
                               (loop (cdr ls) so-far (cons head acc)))
                              ((= so-far n)
                               (set! groups (cons (reverse! (cons head acc))
                                                  (or groups '())))
                               (loop (cdr ls) 0 '()))
                              (else
                               (decry "cannot group: ~S" head)))))))))
    ;; do it!
    (or out (begin (set! out (open-output-string))
                   (set! return (lambda ()
                                  (let ((rv (get-output-string out)))
                                    (close-port out)
                                    rv)))))
    (let loop ((ls (if groups (reverse! groups) (list fspecs)))
               (prev-widths #f))
      (or (null? ls)
          (let* ((group (car ls))
                 (spread (map (lambda (pair)
                                (list (1- (* cols/b (width: pair)))
                                      (name: pair)))
                              group))
                 (widths (map width: group))
                 (bits (map bits: group)))

            (define (>>space-space-1)
              (fo "|~:{~VA|~}~%"
                  (map (lambda (w l)
                         (or (string-null? l)
                             (= w (string-length l))
                             (set! l (make-string w (string-ref l 0))))
                         (list (1- (* cols/b w))
                               (format #f "~{~A~^ ~}"
                                       (string->list l))))
                       widths bits)))

            (define (>>bar-space-1)
              (fo "|~{~A|~}~%"
                  (make-list (apply + widths)
                             (make-string (1- cols/b) #\space))))

            (and (eq? #:over name-placement)
                 (fo field-name-format spread))
            (>>plus-dash-1 (if (and prev-widths merge-borders?)
                               (merge-widths prev-widths widths)
                               widths))
            (let* ((int? (eq? #:internal name-placement))
                   (style (if (or int? (C #:suppress-sep-bars))
                              >>space-space-1
                              >>bar-space-1)))
              (and int? (fo field-name-format spread))
              (let loop ((vpad (- (C #:height) (if int? 1 0))))
                (or (zero? vpad)
                    (begin
                      (style)
                      (loop (1- vpad))))))
            (and (or (not merge-borders?)
                     (null? (cdr ls)))
                 (>>plus-dash-1 widths))
            (and (eq? #:under name-placement)
                 (fo field-name-format spread))
            (loop (cdr ls) widths))))
    (return)))

(define (main args)
  (check-hv args '((package . "ttn-do")
                   (version . "2.2")
                   ;; 2.2  -- Guile 2 slog
                   (help . commentary)))
  (let ((qop (qop<-args args `((file (single-char #\f) (value #t)
                                     (predicate ,file-exists?))))))
    (bit-field-diagram #t (or (qop 'file open-input-file)
                              (open-input-string (car (qop '()))))))
  #t)

;;; bit-field-diagram ends here
