#!/bin/sh
exec ${GUILE-guile} -e '(ttn-do mark-up-news-excerpt)' -s $0 "$@" # -*- scheme -*-
!#
;;; mark-up-news-excerpt

;; Copyright (C) 2011-2013, 2017, 2020, 2021 Thien-Thi Nguyen
;;
;; This file is part of ttn-do, released under the terms of the
;; GNU General Public License as published by the Free Software
;; Foundation; either version 3, or (at your option) any later
;; version.  There is NO WARRANTY.  See file COPYING for details.

;;; Commentary:

;; Usage: mark-up-news-excerpt [options] SPECFILE
;;
;; Process SPCEFILE, which must be in a special format, and
;; display to stdout the XHTML tree of its "Latest release"
;; paragraph and NEWS outline, including headings and body text.
;;
;; The NEWS must be in the format:
;;
;;   PREAMBLE
;;
;;   - VERSION | DATE
;;     - ITEM
;;     ...
;;
;;   - OLDER-VERSION | OLDER-DATE
;;   ...
;;
;; Any PREAMBLE and OLDER-VERSION tree onwards are ignored.
;;
;; The SPECFILE format is line-oriented UTF-8 text, with comments
;; starting with semicolon and going to the end of the line:
;;
;;   ;;; HEADER
;;
;;   * TAG   TEXT
;;   ...
;;
;;   #:KEY  VALUE
;;   ...
;;
;;   ;;; FOOTER
;;
;; Both HEADER and FOOTER are ignored, although it is probably a
;; good idea to put a "Local variables" block in the FOOTER to make
;; sure the file stays UTF-8.
;;
;; The "markup section" begins with an asterisk in column 0 followed
;; by a TAG (e.g., ‘code’), followed by the text that must be marked
;; up.  Between TAG and TEXT must appear one or more space (U+0020)
;; characters.  See module (ttn-do zzz xhtml-tree) for more info.
;;
;; The rest of the file is the "config section", made of alternating
;; Scheme keywords and values.  These keywords are supported:
;;
;; * #:download-dir URI (string)
;;   In the "Latest release" paragraph, VERSION and DATE are
;;   formatted as "VERSION (DATE)".  If this option is given,
;;   that text is hyperlinked to URI.
;;
;; * #:extract FILENAME (string)
;;   By default the news file to mark up is taken as SPECFILE sans
;;   its extension -- e.g., for SPECFILE "NEWS.mune", use "NEWS".
;;   If this option is given, use FILENAME, instead.
;;
;; If optional arg ‘--xhtml’ (‘-x’ for short) is given, flatten
;; the XHTML tree and display XHTML instead.  Follow each top-level
;; element (i.e., ‘<p>’ and ‘<ul>’) with a newline.
;;
;;
;; Usage: mark-up-news-excerpt -e excerpt-file [-d URL] [-x]
;;
;; Process EXCERPT-FILE, which must be in a special format,
;; and display to stdout the XHTML tree of its "Latest release"
;; paragraph and NEWS outline, including headings and body text.
;;
;; The EXCERPT-FILE format is line-oriented UTF-8 text:
;;
;;   HEADER
;;
;;   * TAG   TEXT
;;   ...
;;
;;   - VERSION | DATE
;;     - ITEM
;;     ...
;;
;;   FOOTER
;;
;; Both HEADER and FOOTER are ignored, although it is probably a
;; good idea to put a "Local variables" block in the FOOTER to make
;; sure the file stays UTF-8.
;;
;; The "markup section" begins with an asterisk in column 0 followed
;; by a TAG (e.g., ‘code’), followed by the text that must be marked
;; up.  Between TAG and TEXT must appear one or more space (U+0020)
;; characters.  See module (ttn-do zzz xhtml-tree) for more info.
;;
;; In the "Latest release" paragraph, VERSION and DATE are formatted
;; as "VERSION (DATE)".  If ‘--download-dir URL’ (‘-d’ for short) is
;; given, that text is hyperlinked to URL.
;;
;; In addition to the explicit markup, the ITEMs are automatically
;; processed to mark ‘foo’ and "bar" with ‘~code’.  In the first
;; case, the left (U+2018) and right (U+2019) single-quotes are
;; placed outside the ‘~code’, while in the second case, the double
;; quotes (U+0022) are placed inside the ‘~code’.
;;
;; If optional arg ‘--xhtml’ (‘-x’ for short) is given, flatten
;; the XHTML tree and display XHTML instead.  Follow each top-level
;; element (i.e., ‘<p>’ and ‘<ul>’) with a newline.

;;; Code:

(define-module (ttn-do mark-up-news-excerpt)
  #:export (mark-up-news-excerpt
            tree<-news-excerpt
            main)
  #:use-module ((ttn-do zzz banalities) #:select (check-hv
                                                  decry
                                                  qop<-args))
  #:use-module ((ttn-do zzz personally) #:select (FE fs fse make-fso
                                                     forms<-port))
  #:use-module ((ttn-do zzz filesystem) #:select (filename-stem))
  #:use-module ((ttn-do zzz txtoutline) #:select (txtoutline-reader))
  #:use-module ((ttn-do zzz publishing) #:select (html-data<-file
                                                  w/o-<&>
                                                  flatten-to
                                                  flatten))
  #:use-module ((srfi srfi-11) #:select (let-values))
  #:use-module ((srfi srfi-13) #:select (string-trim-right))
  #:use-module ((ttn-do zzz 0gx temporary-file) #:select (unique-i/o-file-port
                                                          unlink-port-filename))
  #:use-module ((ttn-do mogrify) #:select (filename:
                                           editing-buffer))
  #:use-module ((ice-9 regex) #:select (match:prefix
                                        match:suffix
                                        match:substring))
  #:autoload (ice-9 pretty-print) (pretty-print))

(define URI-RX (make-regexp "<(https*:[^<>]+)>"))

(define read-text-outline-silently
  (txtoutline-reader
   "(([ ][ ])*)- *"
   '((level-substring-divisor . 2)
     (more-lines? . #t))))

(define (markup-gt a b)
  (let ((len-a (string-length (car a)))
        (len-b (string-length (car b))))
    (or (> len-a len-b)
        (and (= len-a len-b)
             (string>? (car a) (car b))))))

(define (current-line buf)
  (editing-buffer buf
    (let ((p (point)))
      (goto-char (point-min))
      (let loop ((n 1))
        (if (search-forward "\n" p 0)
            (loop (1+ n))
            (if (bolp)
                (1- n)
                n))))))

(define (bad-markup buf reason)
  (fse "~A:~A: bad markup: ~A~%"
       (filename: buf)
       (current-line buf)
       reason)
  (exit #f))

(define (read-markup buf)
  (editing-buffer buf
    (define (badness s . args)
      (bad-markup buf (apply fs s args)))
    (forward-char -1)
    (let loop ((alist '()))
      (if (eolp)
          ;; rv
          (sort! alist markup-gt)
          (let* ((m (cond ((char=? #\* (char-after))
                           (forward-char 1)
                           (read (buffer-port)))
                          (else
                           (forward-char 1)
                           (badness "missing bol ‘*’"))))
                 (p (if (looking-at " +")
                        (match-end 0)
                        (badness "missing space after ‘~A’" m))))
            (and (char=? #\newline (char-after p))
                 (badness "missing text for ‘~A’" m))
            (forward-line 1)
            (loop (acons (buffer-substring p (1- (point)))
                         (symbol-append '~ m)
                         alist)))))))

(define (grok excerpt opts)
  (let* ((download-dir (and (pair? opts)
                            (car opts)))
         (buf (editing-buffer #t
                (insert-file-contents excerpt)
                (current-buffer)))
         (markup (editing-buffer buf
                   (if (search-forward "\n*" #f #t)
                       (read-markup buf)
                       '())))
         (tree (car (editing-buffer buf
                      (let ((p (point)))
                        (cond ((re-search-forward "^#" #f #t)
                               (forward-char -1)
                               (let ((plist (forms<-port (buffer-port))))
                                 (define (bye q)
                                   (let ((adj (if (re-search-forward
                                                   "^- " #f (and (< (point) q)
                                                                 1))
                                                  2
                                                  0)))
                                     (delete-region (- (point) adj) q)))
                                 (define (pget key)
                                   (and=> (memq key plist)
                                          cadr))
                                 (delete-region p (point-max))
                                 ;; Add a newline for the sake of regexp operator "^".
                                 (insert #\newline)
                                 (insert-file-contents
                                  (or (pget #:extract)
                                      (filename-stem excerpt)))
                                 (goto-char p)
                                 (bye p)
                                 (bye (point-max))
                                 (goto-char p)
                                 (or download-dir
                                     (set! download-dir
                                           (pget #:download-dir)))))
                              ((re-search-forward "^;+ Local variables:" #f #t)
                               (beginning-of-line)
                               (delete-region (point) (point-max))
                               (goto-char p))))
                      (read-text-outline-silently (buffer-port))))))
    (values markup
            tree
            download-dir)))

(define (string-w/o-naked-<&> s)
  (flatten-to #f (w/o-<&> s)))

;; Return an expression (unevaluated tree) made from marking up
;; @var{excerpt-file} (a string).  The tree includes elements to
;; produce a "Latest release:" paragraph (including a hyperlink to
;; @var{download-dir}, if specified) followed by the outline of news
;; items.  Evaluation of this tree requires access to XHTML procs
;; (@pxref{zzz xhtml-tree}).
;;
;;-args: (- 1 0 download-dir)
;;
(define (mark-up-news-excerpt excerpt-file . opts)
  (let-values (((markup tree download-dir) (grok excerpt-file opts)))

    (define (decorate s)
      (editing-buffer (string-w/o-naked-<&> s)

        (define (look literal)
          (goto-char (point-min))
          (search-forward literal #f #t))

        (define (ret markup)
          (cons* 'list (buffer-substring
                        (point-min)
                        (match-beginning 0))
                 (list markup (match-string 0))
                 (list (decorate (buffer-substring
                                  (match-end 0)
                                  (point-max))))))

        (cond ((or-map (lambda (pair)
                         (and (look (car pair))
                              (cdr pair)))
                       markup)
               => ret)
              ((begin (goto-char (point-min))
                      (re-search-forward "\"[^\"]*\"" #f #t))
               (ret '~code))
              (else
               (buffer-string)))))

    (define (pretty s)
      (let ((rv '()))
        (define (push! x)
          (set! rv (cons x rv)))
        (editing-buffer s
          (goto-char (point-min))
          (while (re-search-forward "‘([^‘’]+)’" #f #t)
            (let ((sub-b (match-beginning 1))
                  (sub-e (match-end 1)))
              (push! (decorate (buffer-substring (point-min) sub-b)))
              (push! (list '~code (decorate (buffer-substring sub-b sub-e))))
              (delete-region (point-min) sub-e)))
          (push! (decorate (buffer-string))))
        (cons 'list (reverse! rv))))

    (define (proper s)
      (cond ((regexp-exec URI-RX s)
             => (lambda (m)
                  (list 'list
                        (pretty (match:prefix m))
                        `(~a 'href ,(match:substring m 1)
                             ,@(w/o-<&> (match:substring m 0)))
                        (proper (match:suffix m)))))
            (else
             (pretty s))))

    (define (bodacious s)
      (if (not (string-index s #\newline))
          (proper s)
          (let ((indent #f)
                (rv '()))

            (define (push! x)
              (set! rv (cons x rv)))

            (define (p s)
              (cons '~p (cdr (proper s))))

            (define (pre s)
              (editing-buffer s
                (goto-char (point-min))
                (while (< (point) (point-max))
                  (and (looking-at " ")
                       (delete-char indent))
                  (forward-line 1))
                (list '~pre (string-w/o-naked-<&> (buffer-string)))))

            (editing-buffer s

              (define (this-indent)
                (if (looking-at " +")
                    (string-length (match-string 0))
                    0))

              (goto-char (point-min))
              (search-forward "\n\n")
              (push! (p (buffer-substring (point-min)
                                          (match-beginning 0))))
              (delete-region (point-min) (point))
              (set! indent (this-indent))
              (while (< (point) (point-max))
                (let ((same? (= indent (this-indent)))
                      (part (buffer-substring
                             (point-min)
                             (or (search-forward "\n\n" #f 1)
                                 (point-max)))))
                  (push! ((if same?
                              p
                              pre)
                          (string-trim-right part)))
                  (delete-region (point-min) (point)))))
            (cons 'list (reverse! rv)))))

    (define (munge x)
      (cond ((pair? x) (list '~li (bodacious (car x))
                             (cons '~ul (map munge (cdr x)))))
            ((string? x) (list '~li (bodacious x)))
            (else (list '~pre x))))

    ;; rv
    `(list (~p 'class "button"
               "Latest release: "
               ;; for debugging
               ;; (strftime "%F %T" (localtime (current-time))) " "
               ,(let ((s (editing-buffer (car tree)
                           (goto-char (point-min))
                           (search-forward "| ")
                           (delete-region (match-beginning 0) (match-end 0))
                           (insert "(")
                           (goto-char (point-max))
                           (insert ")")
                           (buffer-string))))
                  (if download-dir
                      `(~a 'href ,download-dir ,s)
                      s)))
           ;; We ‘cons’ a dummy head for the sake of ‘munge’ first,
           ;; and fix up (discard cruft) w/ ‘caddr’ afterwards.
           ,(caddr (munge (cons "" (cdr tree)))))))

;; Return a tree made from evaluating:
;;
;; @example
;; (mark-up-news-excerpt @var{filename} @var{download-dir})
;; @end example
;;
;; in the ``current module''.
;;
(define (tree<-news-excerpt filename download-dir)
  (let* ((port (unique-i/o-file-port (fs "~AT" filename)))
         (fso (make-fso port)))
    (fso "~S~%~S~%"
         '(use-modules (ttn-do zzz xhtml-tree))
         (mark-up-news-excerpt filename download-dir))
    (seek port 0 SEEK_SET)
    (let ((rv (html-data<-file (port-filename port))))
      (unlink-port-filename port)
      rv)))

(define (main/qop qop)

  (define (sexp exfn ddir)
    (pretty-print (mark-up-news-excerpt exfn ddir)))

  (define (xhtml exfn ddir)
    (FE (tree<-news-excerpt exfn ddir)
        (lambda (form)
          (flatten form)
          (newline))))

  ((if (qop 'xhtml)
       xhtml
       sexp)
   (or (false-if-exception (car (qop '())))
       (qop 'excerpt-file)
       (decry "Missing args (try --help)"))
   (qop 'download-dir))
  #t)

(define (main args)
  (check-hv args '((package . "ttn-do")
                   (version . "1.7")
                   ;; 1.7  -- bugfix: handle <&> in ~pre
                   ;; 1.6  -- use class "button" for "Latest release" para
                   ;; 1.5  -- don't slog for Guile 2
                   ;; 1.4  -- slog for Guile 2
                   ;; 1.3  -- bugfix: do ‘<’, ‘&’, ‘>’ substitutions
                   ;;         mark up "<http...>" as anchor
                   ;;         add SPECFILE usage
                   ;;         new option: --xhtml
                   ;; 1.2  -- new proc; also process body
                   ;; 1.1  -- bugfix: nest sub-‘~ul’ properly in ‘~li’
                   (help . commentary)))
  (main/qop
   (qop<-args
    args '((xhtml (single-char #\x))
           (excerpt-file (single-char #\e) (value #t))
           (download-dir (single-char #\d) (value #t))))))

;;; mark-up-news-excerpt ends here
