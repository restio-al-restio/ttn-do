/* linux-gnu.c

   Copyright (C) 2007-2013, 2017, 2019-2021 Thien-Thi Nguyen

   This is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this package.  If not, see <https://www.gnu.org/licenses/>.  */

#include "config.h"

#include <stdbool.h>
/* Citing getsid(2) here:
   To get the prototype under glibc, define both _XOPEN_SOURCE and
   _XOPEN_SOURCE_EXTENDED, or use "#define _XOPEN_SOURCE n" for some
   integer n larger than or equal to 500.  */
#define _XOPEN_SOURCE 500
#include <unistd.h>             /* for getsid */
#include <sys/select.h>         /* for fd_set (POSIX.1-2001) */
#include <stdlib.h>
#include <sys/sendfile.h>
#include <sys/uio.h>
#include "gi.h"

#define PROB(x)  (0 > (x))
#define UNBOUNDP(x)  (SCM_EQ_P (x, SCM_UNDEFINED))


PRIMPROC
(sl_getsid, "getsid", 1, 0, 0,
 (SCM pid),
 doc: /***********
Return the session id associated with @var{pid}.  */)
{
  return NUM_INT (getsid (C_INT (pid)));
}

PRIMPROC
(sl_gethostname, "gethostname", 0, 0, 0,
 (void),
 doc: /***********
Return the host name as a string,
or @code{#f} if there were problems.  */)
{
  char buf[256];

  if (PROB (gethostname (buf, 256)))
    RETURN_FALSE ();
  return STRING (buf);
}


PRIMPROC
(sl_sendfile, "sendfile", 4, 0, 0,
 (SCM outfd, SCM infd, SCM offset, SCM count),
 doc: /***********
Call sendfile(2) with args @var{outfd}, @var{infd},
@var{offset} and @var{count}, all non-negative integers.
As a special case, @var{offset} may also be @code{#f},
which means start to read from the ``current'' file offset,
and update it afterwards.
Return the number of bytes actually written.  */)
{
#define FUNC_NAME s_sl_sendfile

  int rv, coutfd, cinfd;
  off_t coffset; int offset_specified_p;
  size_t ccount;

  VALIDATE_INUM_COPY (1, outfd, coutfd);
  VALIDATE_INUM_COPY (2, infd, cinfd);
  if ((offset_specified_p = NOT_FALSEP (offset)))
    SCM_VALIDATE_ULONG_COPY (3, offset, coffset);
  SCM_VALIDATE_ULONG_COPY (4, count, ccount);

  if (PROB (rv = sendfile (coutfd, cinfd, (offset_specified_p
                                           ? &coffset
                                           : NULL),
                           ccount)))
    SYSTEM_ERROR ();

  return NUM_LONG (rv);

#undef FUNC_NAME
}

PRIMPROC
(sl_sendfile_slash_never_fewer, "sendfile/never-fewer", 4, 0, 0,
 (SCM outfd, SCM infd, SCM offset, SCM count),
 doc: /***********
Call sendfile(2) with args @var{outfd}, @var{infd},
@var{offset} and @var{count}, all non-negative integers.
Loop until all bytes are sent (or error).  */)
{
#define FUNC_NAME s_sl_sendfile_slash_never_fewer

  int rv, coutfd, cinfd;
  off_t coffset;
  size_t ccount;

  VALIDATE_INUM_COPY (1, outfd, coutfd);
  VALIDATE_INUM_COPY (2, infd, cinfd);
  SCM_VALIDATE_ULONG_COPY (3, offset, coffset);
  SCM_VALIDATE_ULONG_COPY (4, count, ccount);

  for (; ccount; ccount -= rv)
    {
      if (PROB (rv = sendfile (coutfd, cinfd, &coffset, ccount)))
        SYSTEM_ERROR ();
      if (! rv)
        break;
    }

  RETURN_UNSPECIFIED ();

#undef FUNC_NAME
}


static unsigned long int sl_iovec_tag;

typedef struct
{
  SCM     obj;                          /* string or uvec */
  void   *stage;                        /* string transfer space (sigh) */
  size_t  len;
  size_t  inc;
  size_t  ofs;
} sl_details_t;

typedef struct
{
  struct iovec  *v;                     /* array of ‘struct iovec’ */
  unsigned int  svalid;                 /* count of struct iovect */
  size_t        sstart;                 /* index into v */
  sl_details_t  *det;
  char          *str;
} sl_iovec_t;

#define UNPACK_IOVEC(smob)                      \
  ((sl_iovec_t *) SMOBDATA (smob))

#define VALIDATE_IOVEC_COPY(pos, smob, cvar)  do                        \
    {                                                                   \
      ASSERT (smob, SCM_SMOB_PREDICATE (sl_iovec_tag, smob), pos);      \
      cvar = UNPACK_IOVEC (smob);                                       \
    }                                                                   \
  while (0)

static SCM
sl_mark_iovec (SCM smob)
{
  sl_iovec_t *s = UNPACK_IOVEC (smob);

  if (s->svalid)
    {
      size_t i = s->svalid;

      while (--i)
        scm_gc_mark (s->det[i].obj);
      return s->det[0].obj;
    }
  return SCM_BOOL_F;
}

#define VIOV_ALLOC(x)  ((x) * sizeof (struct iovec))
#define DET_ALLOC(x)   ((x) * sizeof (sl_details_t))

#define iovec_name "iovec"
#define viov_name  "viov"
#define det_name   "det"
#define str_name   "str"

#define GCFREE_MAYBE(p,nick)  do                \
    {                                           \
      if (p)                                    \
        GCFREE (p, nick);                       \
      p = NULL;                                 \
    }                                           \
  while (0)

static void
clear_details (sl_iovec_t *s)
{
  size_t i;

  if (s->det)
    for (i = 0; i < s->svalid; i++)
      s->det[i].obj = SCM_EOL;
}

#define FREE_EVERYTHING(s)  do                  \
    {                                           \
      GCFREE_MAYBE (s->str, str_name);          \
      clear_details (s);                        \
      GCFREE_MAYBE (s->det, det_name);          \
      GCFREE_MAYBE (s->v, viov_name);           \
      GCFREE (s, iovec_name);                   \
    }                                           \
  while (0)

static size_t
sl_free_iovec (SCM smob)
{
  sl_iovec_t *s = UNPACK_IOVEC (smob);
  size_t rv = GI_LEVEL_1_8
    ? 0
    : (sizeof (sl_iovec_t)
       + VIOV_ALLOC (s->svalid)
       + DET_ALLOC (s->svalid));

  FREE_EVERYTHING (s);
  return rv;
}

static int
sl_print_iovec (SCM smob, SCM port, scm_print_state *pstate)
{
  sl_iovec_t *s = UNPACK_IOVEC (smob);

  scm_puts     ("#<iovec ",                            port);
  scm_intprint                         (s->sstart, 10, port);
  scm_putc     ('/',                                   port);
  scm_intprint                         (s->svalid, 10, port);
  scm_putc     (' ',                                   port);
  scm_intprint             ((unsigned long int) s, 16, port);
  scm_putc     ('>',                                   port);

  return 1;
}

SCM_SYMBOL (sym_s, "s");
SCM_SYMBOL (sym_l, "l");
SCM_SYMBOL (sym_a, "a");
SCM_SYMBOL (sym_u8, "u8");
SCM_SYMBOL (sym_u16, "u16");
SCM_SYMBOL (sym_s16, "s16");
SCM_SYMBOL (sym_u32, "u32");
SCM_SYMBOL (sym_s32, "s32");
SCM_SYMBOL (sym_s64, "s64");

#if GI_LEVEL_1_8

#define uve_type   scm_array_type

#else  /* !GI_LEVEL_1_8 */

static SCM
uve_type (SCM uve)
{
  SCM proto = scm_array_prototype (uve);

#ifdef NEED_ARRAY_PROTOTYPE_KLUDGE
  if (SCM_UNDEFINED == proto)
    switch (SCM_TYP7 (uve))
      {
      case scm_tc7_svect: proto = sym_s; break;
      case scm_tc7_llvect: proto = sym_l; break;
      }
#endif  /* NEED_ARRAY_PROTOTYPE_KLUDGE */

  return
    (EQ (proto, SCM_MAKE_CHAR ('a'))
     ? sym_a
     : (EQ (proto, SCM_MAKE_CHAR ('\0'))
        ? sym_u8
        : (EQ (proto, sym_s)
           ? sym_s16
           : (EQ (proto, SCM_MAKINUM (1))
              ? sym_u32
              : (EQ (proto, SCM_MAKINUM (-1))
                 ? sym_s32
                 : (EQ (proto, sym_l)
                    ? sym_s64
                    : SCM_BOOL_F))))));
}

#endif /* !GI_LEVEL_1_8 */

#if GI_LEVEL_2_0
static size_t
acceptable_uvec (SCM obj)
{
#define UVETYPEP(type)  (NOT_FALSEP (scm_ ## type ## vector_p (obj)))

  if (UVETYPEP (s8)  || UVETYPEP (u8))  return 1;
  if (UVETYPEP (s16) || UVETYPEP (u16)) return 2;
  if (UVETYPEP (s32) || UVETYPEP (u32)) return 4;
  if (UVETYPEP (s64) || UVETYPEP (u64)) return 8;
  return 0;

#undef UVETYPEP
}
#endif  /* GI_LEVEL_2_0 */

static bool
try_uvec (SCM obj, sl_details_t *d)
{
  d->obj = obj;
  d->stage = NULL;
  d->ofs = 0;                           /* for now */

  if (STRINGP (obj))
    {
      d->len = C_ULONG (scm_string_length (obj));
      d->inc = 1;
      d->stage = (void *)1;
      return true;
    }

#if GI_LEVEL_2_0
  if ((d->inc = acceptable_uvec (obj)))
    {
      d->len = scm_c_array_length (obj);
      return true;
    }
#else  /* !GI_LEVEL_2_0 */
  if (C_BOOL (scm_uniform_vector_p (obj)))
    {
      SCM vectag = uve_type (obj);
      size_t wordsize =
        ((EQ (vectag, sym_a)
          || EQ (vectag, sym_u8))
         ? 1
         : ((EQ (vectag, sym_u16)
             ||EQ (vectag, sym_s16))
            ? 2
            : ((EQ (vectag, sym_u32)
                || EQ (vectag, sym_s32))
               ? 4
               : (EQ (vectag, sym_s64)
                  ? 8
                  : 0))));

      if (wordsize)
        {
          d->len = UNIFORM_VECTOR_LEN (obj);
          d->inc = wordsize;
          return true;
        }
    }
#endif  /* !GI_LEVEL_2_0 */

  return false;
}

PRIMPROC
(sl_iovec, "iovec", 1, 0, 0,
 (SCM sequence),
 doc: /***********
Return an iovec derived from @var{sequence}, a list or vector.
This object is suitable for passing to @code{readv} and @code{writev}.
Each element of @var{sequence} must be either a string;
or a uniform vector:

@example
@r{Guile 2 and later: w/ type}
  s8, u8, s16, u16, s32, u32, s64, u64

@r{prior to Guile 2: w/ prototype}
  #\a   @r{(string)}
  #\nul @r{(byte vector)}
  s     @r{(short int)}
  -1    @r{(long int)}
  1     @r{(unsigned long int)}
  l     @r{(long long int)}
@end example

or one of the following forms,
where @var{uve} is a suitable uniform vector previously described:

@table @code
@item (@var{uve} @var{start})
Select elements of @var{uve} from index @var{start}
to the end of @var{uve}.

@item (@var{uve} @var{start} @var{stop})
Select elements of @var{uve} from index @var{start}
up to but not including index @var{stop}.

@item (@var{uve} @var{start} . @var{count})
Select @var{count} elements of @var{uve} from index @var{start}.
@end table

The iovec object keeps track of the @dfn{current element},
initially, the first.  After the i/o operation that uses it
finishes (and returns @code{#f}), this state is reset so that
subsequent calls proceed from the beginning.  */)
{
#define FUNC_NAME s_sl_iovec
  int i, len;
  sl_iovec_t *s;
  size_t strings_len = 0;

  if (NULLP (sequence) || PAIRP (sequence))
    sequence = scm_vector (sequence);
  len = VECTOR_LEN (sequence);

  s = GCMALLOC (sizeof (sl_iovec_t), iovec_name);

  s->svalid = len;
  s->sstart = 0;
  s->str = NULL;

  /* Initialize the viov (if necessary).  */
  if (! len)
    {
      s->v = NULL;
      s->det = NULL;
    }
  else
    {
      const char *why = NULL;

#define BAD(reason)  do                         \
        {                                       \
          why = reason;                         \
          goto bad;                             \
        }                                       \
      while (0)

      s->v = GCMALLOC (VIOV_ALLOC (len), viov_name);
      s->det = GCMALLOC (DET_ALLOC (len), det_name);

      for (i = 0; i < len; i++)
        {
          SCM elem = VECTOR_REF (sequence, i);
          SCM uvec = PAIRP (elem) ? CAR (elem) : elem;
          SCM spec = PAIRP (elem) ? CDR (elem) : SCM_EOL;
          sl_details_t *d = &s->det[i];

          if (! try_uvec (uvec, d))
            BAD ("not a string or uvec");

          if (PAIRP (spec))
            {
              SCM start = CAR (spec);
              SCM rest = CDR (spec);

              if (! NUMBERP (start))
                BAD ("START not a number");
              d->ofs = C_ULONG (start);
              if (d->len < d->ofs)
                BAD ("START out of range");
              d->len -= d->ofs;
              if (NULLP (rest))
                /* Do nothing -- done.  */
                ;
              else if (PAIRP (rest))
                {
                  SCM end = CAR (rest);
                  size_t stop;

                  if (PAIRP (CDR (rest)))
                    BAD ("junk after END");
                  if (! NUMBERP (end))
                    BAD ("END not a number");
                  stop = C_ULONG (end);
                  if (stop < d->ofs)
                    BAD ("END less than START");
                  if (d->ofs + d->len < stop)
                    BAD ("END out of range");
                  d->len = stop - d->ofs;
                }
              else if (NUMBERP (rest))
                {
                  size_t count = C_ULONG (rest);

                  if (d->len < count)
                    BAD ("COUNT out of range");
                  d->len = count;
                }
              else
                BAD ("invalid END or COUNT");
            }
          else if (! NULLP (spec))
            {
              BAD ("invalid START");
            bad:
              FREE_EVERYTHING (s);
              ERROR ("bad iovec element spec at sequence index ~A: ~A",
                     NUM_INT (i), STRING (why));
            }

          /* OK, we know what bits are relevant now.  */
#if !GI_LEVEL_1_8
          s->v[i].iov_base = SCM_WRITABLE_VELTS (uvec) + d->inc * d->ofs;
#endif
          s->v[i].iov_len = d->inc * d->len;
          if (d->stage)
            strings_len += d->len;
        }
#undef BAD

      /* Now handle any strings.  */
      if (strings_len)
        {
          size_t so_far = 0;

          s->str = GCMALLOC (strings_len, str_name);
          for (i = 0; i < len; i++)
            {
              sl_details_t *d = &s->det[i];

              if (d->stage)
                {
                  d->stage = s->str + so_far;
                  so_far += d->len;
                }
            }
        }
    }

  SCM_RETURN_NEWSMOB (sl_iovec_tag, s);
#undef FUNC_NAME
}

/* Scatter/gather function type, ie. readv(2) or writev(2).  */
typedef int (* sg_fn_t) (int, struct iovec *, int);

#define SG_RD  ((sg_fn_t) readv)
#define SG_WR  ((sg_fn_t) writev)


#if GI_LEVEL_1_8
typedef struct
{
  size_t        i;
  sg_fn_t       op;
  int           cfdes;
  size_t        ccount;
  struct iovec *base;
  struct iovec *cur;
  sl_details_t *det;
  char         *str;
  int           rv;
} sl_wind_closure_t;

static void
wind (sl_wind_closure_t *wc)
{
  if (wc->i < wc->ccount)
    {
      sl_details_t *d = wc->det++;
      struct iovec *iov = wc->cur++;
      scm_t_array_handle h;

      /* Set up.  */
      if (d->stage)
        {
          iov->iov_base = d->stage;
          /* (outbound) Copy the string to the staging area.
             FIXME: This loses for multibyte encodings.  */
          if (SG_WR == wc->op)
            scm_to_locale_stringbuf (d->obj, d->stage, d->len);
        }
      else
        {
          scm_array_get_handle (d->obj, &h);
          iov->iov_base
            = scm_array_handle_uniform_writable_elements (&h)
            + d->ofs * d->inc;
        }

      /* Recurse.  */
      wc->i++;
      wind (wc);

      /* Tear down.  */
      if (d->stage)
        {
          /* (inbound) Copy the staging area to the string.
             FIXME: This loses for multibyte encodings.  */
          if (SG_RD == wc->op)
            {
              SCM fresh = BSTRING (d->stage, d->len);
              SCM zero = NUM_INT (0);

              scm_string_copy_x (d->obj, zero, fresh, zero,
                                 scm_string_length (fresh));
            }
        }
      else
        scm_array_handle_release (&h);
    }
  else
    {
      wc->rv = wc->op (wc->cfdes, wc->base, wc->ccount);
    }
}
#endif  /* GI_LEVEL_1_8 */

static SCM
sl_rdwrv (const char *FUNC_NAME, sg_fn_t sg_op,
          SCM fdes, SCM iovec, SCM count)
{
  sl_iovec_t *s;
  struct iovec *base;
  int cfdes, done, move;
  size_t ccount;

  VALIDATE_INUM_COPY (1, fdes, cfdes);
  VALIDATE_IOVEC_COPY (2, iovec, s);
  if (UNBOUNDP (count))
    ccount = s->svalid - s->sstart;
  else
    VALIDATE_INUM_COPY (3, count, ccount);

  if (! s->svalid)
    RETURN_FALSE ();

  if (s->svalid == s->sstart)
    {
      /* Reset for the next time.  */
      s->sstart = 0;
      RETURN_FALSE ();
    }

  if (s->svalid < s->sstart + ccount)
    ccount = s->svalid - s->sstart;

  base = s->v + s->sstart;

#if GI_LEVEL_1_8
  {
    sl_wind_closure_t wc =
      {
        .i = 0,
        .op = sg_op,
        .cfdes = cfdes,
        .ccount = ccount,
        .base = base,
        .cur = base,
        .det = s->det + s->sstart,
        .str = NULL,
        .rv = -1
      };
    size_t i;

    if (s->str)
      for (i = 0; i < s->sstart; i++)
        {
          sl_details_t *d = s->det + i;

          if (d->stage)
            wc.str = d->stage + d->len;
        }

    wind (&wc);
    move = done = wc.rv;
  }
#else
  move = done = sg_op (cfdes, base, ccount);
#endif

  if (PROB (move))
    SYSTEM_ERROR ();
  if (base->iov_len > move)
    {
      base->iov_len -= move;
      base->iov_base += move;
    }
  else
    while (move)
      if (move >= base->iov_len)
        {
          move -= base->iov_len;
          base++;
          s->sstart++;
        }

  return NUM_INT (done);
}

PRIMPROC
(sl_readv, "readv", 2, 1, 0,
 (SCM fdes, SCM iovec, SCM count),
 doc: /***********
Read from @var{fdes} data specified by @var{iovec} using readv(2).
Return number of bytes successfully read, or @code{#f}
if there is no more space to read into.
If the underlying system call returns -1, signal error.
Update the starting position of @var{iovec} by side-effect.
Optional arg @var{count} specifies how many elements of
@var{iovec} to process (default is ``remaining count'').  */)
{
  return sl_rdwrv (s_sl_readv, SG_RD, fdes, iovec, count);
}

PRIMPROC
(sl_writev, "writev", 2, 1, 0,
 (SCM fdes, SCM iovec, SCM count),
 doc: /***********
Write to @var{fdes} data specified by @var{iovec} using writev(2).
Return number of bytes successfully written, or @code{#f}
if there is nothing more to be written.
If the underlying system call returns -1, signal error.
Update the starting position of @var{iovec} by side-effect.
Optional arg @var{count} specifies how many elements of
@var{iovec} to process (default is ``remaining count'').  */)
{
  return sl_rdwrv (s_sl_writev, SG_WR, fdes, iovec, count);
}


static
void
init_module (void)
{
  sl_iovec_tag = scm_make_smob_type ("iovec", sizeof (sl_iovec_t));
  scm_set_smob_mark  (sl_iovec_tag, sl_mark_iovec);
  scm_set_smob_free  (sl_iovec_tag, sl_free_iovec);
  scm_set_smob_print (sl_iovec_tag, sl_print_iovec);

#include "linux-gnu.x"
}

MOD_INIT_LINK_THUNK ("ttn-do zz sys linux-gnu"
                     ,ttn_do_zz_sys_linux_gnu
                     ,init_module)

/* linux-gnu.c ends here */
