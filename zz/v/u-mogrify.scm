;;; u-mogrify.scm

;; Copyright (C) 2020 Thien-Thi Nguyen
;;
;; This is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.
;;
;; This software is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this package.  If not, see <https://www.gnu.org/licenses/>.

(use-modules
 ((ttn-do mogrify) #:select (editing-buffer find-file)))

;; TARGET: FALSE -- DO NOT MODIFY THIS OR THE NEXT LINE.
;; TARGET: ‘PINEAPPLE’

(define buf (editing-buffer (find-file (port-filename (current-load-port)))))

(define (curline)
  (editing-buffer buf
    (let* ((p (point))
           (rv (format #f "~A!~A~%"
                       (buffer-substring (begin (beginning-of-line)
                                                (point))
                                         p)
                       (buffer-substring p (begin (end-of-line)
                                                  (point))))))
      (goto-char p)
      rv)))

(editing-buffer buf
  (search-forward "TARGET: " #f #t 2)
  (let* ((p (point))
         (q (begin (end-of-line) (point)))
         (s (buffer-substring p q)))
    (goto-char p)
    (and (equal? "1" (getenv "DEBUG"))
         (format #t "~A" (curline)))
    (or (string=? "‘PINEAPPLE’" s)
        (let ((cep (current-error-port)))
          (format cep "badness: buf=~S~%" buf)
          (format cep (curline))
          (exit #f)))))

(exit #t)

;;; u-mogrify.scm ends here
