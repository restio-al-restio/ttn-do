;;; test-osys.scm                               -*-scheme-*-

;; Copyright (C) 2011-2013, 2019, 2020 Thien-Thi Nguyen
;;
;; This is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.
;;
;; This software is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this package.  If not, see <https://www.gnu.org/licenses/>.

(use-modules
 ((srfi srfi-13) #:select (string-contains))
 ((ice-9 rdelim) #:select (write-line))
 ((ttn-do zzz personally) #:select (FE fs fso))
 ((ttn-do zzz 0gx uve) #:select (make-uve
                                 uve-set!
                                 uve-type))
 ((ttn-do zz sys linux-gnu) #:select ((sendfile . ttn-sendfile)
                                      sendfile/never-fewer
                                      iovec
                                      readv
                                      writev)))

;; existence
(define all-procs (list
                   ttn-sendfile         ; Guile 2.x has ‘sendfile’ -- snif.
                   sendfile/never-fewer
                   iovec
                   readv
                   writev
                   ;; Add procs here.
                   ))

;; recognized as procs
(FE all-procs
    (lambda (x)
      (or (procedure? x)
          (error "not a procedure" x))))

;; iovec spec
(let ((simple "simple"))

  (define (expect-fail how partial)
    (let* ((spec (cons simple partial))
           (hmm (catch 'misc-error (lambda ()
                                     (iovec (list spec))
                                     #f)
                       (lambda (key who fmt e-args ignored)

                         (define (truly! s . args)
                           (error (apply fs "~A~%raw error args: ~S"
                                         (apply fs s args)
                                         (list key who fmt e-args ignored))))

                         (or (string=? "iovec" who)
                             (truly! "unexpected who: ~A" who))
                         (or (string-contains fmt "bad iovec element spec")
                             (truly! "unexpected fmt: ~S" fmt))
                         (or (and (list? e-args)
                                  (= 2 (length e-args))
                                  (number? (car e-args))
                                  (zero? (car e-args))
                                  (string? (cadr e-args)))
                             (truly! "unexpected error args: ~S" e-args))
                         (or (not ignored)
                             (truly! "non-‘#f’ ignored: ~S" ignored))
                         (cadr e-args)))))
      (or hmm (error (fs "spec ‘~S’ did not produce an error!" spec)))
      (or (string=? how hmm)
          (error (fs "expected ~S but got ~S~%\tfor spec: ~S"
                     how hmm spec)))))

  (expect-fail "invalid START" 'symbol)
  (expect-fail "START not a number" '(symbol))
  (expect-fail "END not a number" '(1 symbol))
  (expect-fail "junk after END" '(1 2 symbol))
  (expect-fail "invalid END or COUNT" '(1 . symbol))
  (expect-fail "END less than START" '(1 0))
  (expect-fail "END out of range" '(1 7))
  (expect-fail "COUNT out of range" '(1 . 6)))

;; iovec/writev/readv
(let* ((zonkable "test-osys.temporay")
       (tmp (open-file zonkable "w+"))
       (ten-bytes (cons "some"
                        (map (lambda (s)
                               (let* ((len (string-length s))
                                      (uve (make-uve 'u8 0 len)))
                                 (FE (string->list s)
                                     (iota len)
                                     (lambda (c i)
                                       (uve-set!
                                        uve i (char->integer c))))
                                 uve))
                             '("stuff"
                               "\n"))))
       (stuff (iovec (apply append (make-list 424 ten-bytes)))))

  (define (out!)
    (writev (fileno tmp) stuff (random 42)))

  (define (one!)
    (let loop ((res (out!)) (acc 0) (count 0))
      (if res (let ((new-acc (+ acc res)))
                (display (cond ((zero? res) res)
                               ((zero? (remainder count 42)) ".")
                               (else "")))
                (loop (out!) new-acc (1+ count)))
          (write-line count)))
    (write-line stuff))

  (one!)
  (one!)
  (let* ((size (seek tmp 0 SEEK_END))
         (ok? (= 8480 size)))
    (fso "wrote ~A bytes (~A)~%" size (if ok? "ok" "BADNESS"))
    (or ok? (exit #f)))
  (close-port tmp)
  (delete-file zonkable))

(exit #t)

;;; test-osys.scm ends here
