/* gi.h

   Copyright (C) 2004-2006, 2008, 2009, 2011-2013, 2019-2021 Thien-Thi Nguyen

   This is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3, or (at your option)
   any later version.

   This software is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this package.  If not, see <https://www.gnu.org/licenses/>.  */

#ifndef _GI_H_
#define _GI_H_

#include <libguile.h>
#include "snuggle/level.h"
#include "snuggle/humdrum.h"
#include "snuggle/defsmob.h"
#include "snuggle/modsup.h"

/*
 * backward (sometimes foresight was incomplete)
 */

#ifndef SCM_WRITABLE_VELTS
#define SCM_WRITABLE_VELTS  SCM_VELTS
#endif

/*
 * abstractions
 */

/* Apparently, some Guile version deprecated ‘SCM_VALIDATE_INUM_COPY’ in
   favor of ‘SCM_VALIDATE_NUMBER_COPY’ but that was in turn later deprecated
   in favor of ‘SCM_VALIDATE_DOUBLE_COPY’.  For now, we use ‘GI_LEVEL_2_2’
   but that might not be completely accurate.  Sigh.  */
#if GI_LEVEL_2_2
#define VALIDATE_INUM_COPY  SCM_VALIDATE_DOUBLE_COPY
#else
#define VALIDATE_INUM_COPY  SCM_VALIDATE_INUM_COPY
#endif

#if GI_LEVEL_2_0
#define UNIFORM_VECTOR_LEN(vec)  (scm_c_array_length (vec))
#else  /* !GI_LEVEL_2_0 */
#define UNIFORM_VECTOR_LEN(vec)  (C_ULONG (scm_uniform_vector_length (vec)))
#endif  /* !GI_LEVEL_2_0 */

#define NOT_FALSEP(x)      (SCM_NFALSEP (x))

#define RETURN_FALSE()                        return SCM_BOOL_F
#define RETURN_UNSPECIFIED()                  return SCM_UNSPECIFIED

#define ASSERT(what,expr,msg)  SCM_ASSERT ((expr), what, msg, FUNC_NAME)

#define SMOBDATA(obj)  ((void *) SCM_SMOB_DATA (obj))

#define PCHAIN(...)  (LISTIFY (__VA_ARGS__, SCM_UNDEFINED))

#define ERROR(blurb, ...)  SCM_MISC_ERROR (blurb, PCHAIN (__VA_ARGS__))
#define SYSTEM_ERROR()     SCM_SYSERROR

#define PRIMPROC             GH_DEFPROC

#endif /* _GI_H_ */

/* gi.h ends here */
