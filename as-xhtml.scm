#!/bin/sh
exec ${GUILE-guile} -e '(ttn-do as-xhtml)' -s $0 "$@" # -*-scheme-*-
!#
;;; as-xhtml

;; Copyright (C) 2011, 2021 Thien-Thi Nguyen
;;
;; This file is part of ttn-do, released under the terms of the
;; GNU General Public License as published by the Free Software
;; Foundation; either version 3, or (at your option) any later
;; version.  There is NO WARRANTY.  See file COPYING for details.

;;; Commentary:

;; Usage: as-xhtml [options] INFILE
;;
;; Expand and evaluate INFILE.  Write resulting XHTML to stdout.
;; Options:
;;
;;  -I, --include DIR      -- add DIR to end of search path
;;                            (can be specified multiply)
;;  -o, --output FILE      -- write output to FILE
;;  -p, --pretty-print     -- pretty-print expansion to stderr
;;
;; The search path for ‘(#:include "FILENAME")’ directives
;; is initialized to the directory of INFILE.

;;; Code:

(define-module (ttn-do as-xhtml)
  #:export (main)
  #:use-module ((ttn-do zzz banalities) #:select (check-hv
                                                  decry
                                                  qop<-args))
  #:use-module ((ttn-do zzz 0gx forms-from) #:select (forms<-file))
  #:use-module ((ttn-do pp) #:select (pp))
  #:use-module ((srfi srfi-11) #:select (let-values))
  #:use-module ((ttn-do zzz personally) #:select (FE))
  #:use-module ((ttn-do zzz publishing) #:select (flatten-to)))

(define (expand inc-dirs x)

  (define (include filename)
    (cond ((search-path inc-dirs filename)
           => forms<-file)
          (else
           (decry "could not find file to include: ~A"
                  filename))))

  (let loop ((acc '()) (ls x))
    (if (null? ls)
        ;; rv
        (values (reverse! (cdr acc))    ; defs
                (car acc))              ; tree
        (let ((form (car ls))
              (rest (cdr ls)))
          (cond ((not (pair? form))
                 (decry "bad form: ~S" form))
                ((keyword? (car form))
                 (case (car form)
                   ((#:include)
                    (loop acc (append! (include (cadr form))
                                       rest)))
                   (else
                    (decry "bad directive: ~S" form))))
                (else
                 (loop (cons form acc)
                       rest)))))))

(begin ;;; FIXME: This is all very ugly.

  (and (defined? 'eval2)
       (use-modules ((ice-9 session) #:select (current-module))))

  (define eval-in-current-module
    (if (defined? 'eval2)
        (lambda (x)
          (eval2 x (standard-eval-closure (current-module))))
        (lambda (x)
          (eval x (current-module))))))

(define (main/qop qop)
  (let* ((spew (if (qop 'pretty-print)
                   (lambda (x)
                     (pp x (current-error-port))
                     x)
                   identity))
         (infile (if (null? (qop '()))
                     (decry "no input specified")
                     (car (qop '())))))
    (define (eicm x)
      (eval-in-current-module (spew x)))
    (let-values (((defs tree) (expand (cons (dirname infile)
                                            (or (qop 'include) '()))
                                      (forms<-file infile))))
      (define (out port)
        (eicm '(use-modules (ttn-do zzz xhtml-tree)))
        (FE defs eicm)
        (flatten-to port (eicm tree)))
      (or (qop 'output (lambda (filename)
                         (call-with-output-file filename out)))
          (out (current-output-port)))))
  #t)

(define (main args)
  (check-hv args '((package . "ttn-do")
                   (version . "1.0")
                   ;; 1.0 -- initial
                   (help . commentary)))
  (main/qop
   (qop<-args
    args '((include (single-char #\I) (value #t) (merge-multiple? #t))
           (output (single-char #\o) (value #t))
           (pretty-print (single-char #\p))))))

;;; as-xhtml ends here
