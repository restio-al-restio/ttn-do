#!/usr/bin/make -f
# Copyright (C) 2008, 2010, 2011 Thien-Thi Nguyen
#
# This file is part of ttn-do, released under the terms of the
# GNU General Public License as published by the Free Software
# Foundation; either version 3, or (at your option) any later
# version.  There is NO WARRANTY.  See file COPYING for details.
##
# Usage: gnuvola-wip [help | version | check | copy | uncopy] [OVERRIDE-VARS]
#
# If invoked w/o args, default to command "check".  Note that this program
# does not support `--help' or `--version'; use hyphenless variants, instead.
#
#   help    -- Display this message and exit successfully.
#   version -- Display version and exit successfully.
#   check   -- Test for subdir .git; exit failurefully if not found.
#   copy    -- Create $(pubrepo) if necessary (with $(git) clone),
#              then push the repo there using $(git) prune, gc, push.
#              Lastly, copy the description file.
#   uncopy  -- Delete $(pubrepo).
#
# Some GNUmakefile vars (and their default values) influence operation:
#
#   git     -- git
#   wip     -- ~/build/gnuvola/wip
#   here    -- $(shell pwd)
#   repo    -- $(notdir $(here)).git
#   branch  -- master
#   pubrepo -- $(wip)/$(repo)
#
# You can override them on the command line with VAR=VALUE.
##

zero := $(lastword $(MAKEFILE_LIST))
me   := $(basename $(notdir $(zero)))

here := $(shell pwd)

check:
	@if [ -d .git ] ; then : ; else				\
	  echo $(me): ERROR: missing repo: $(here)/.git/ ;	\
	  false ; fi

help: version
	@echo
	@sed '/^##/,/^##/!d;/^##/d;s/^# //g;s/^#$$//g' $(zero)

version:
	@echo "$(me) (ttn-do) 1.6"

git     := git
wip     := ~/build/gnuvola/wip
repo    := $(notdir $(here)).git
branch  := master
pubrepo := $(wip)/$(repo)

$(pubrepo):
	$(git) prune
	$(git) gc --prune
	$(git) clone -l --bare .git $(pubrepo)

copy: check $(pubrepo)
	$(git) prune
	$(git) gc --prune
	$(git) push -v --tags $(pubrepo) $(branch)
	cp -p .git/description $(pubrepo)/description
	cd $(pubrepo) && $(git) update-server-info

uncopy: check
	test ! -d $(pubrepo) || rm -rf $(pubrepo)

# gnuvola-wip ends here
