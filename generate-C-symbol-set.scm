#!/bin/sh
exec ${GUILE-guile} -e '(ttn-do generate-C-symbol-set)' -s $0 "$@" # -*-scheme-*-
!#
;;; generate-C-symbol-set

;; Copyright (C) 2009, 2010, 2011, 2013, 2021 Thien-Thi Nguyen
;;
;; This file is part of ttn-do, released under the terms of the
;; GNU General Public License as published by the Free Software
;; Foundation; either version 3, or (at your option) any later
;; version.  There is NO WARRANTY.  See file COPYING for details.

;;; Commentary:

;; Usage: generate-C-symbol-set [options] SYMBOL...
;;
;; Options are:
;; -p, --pool-name NAME      -- use NAME instead of ‘symbolpool’
;; -b, --byte-type TYPE      -- use TYPE instead of ‘uint8_t’
;; -g, --global              -- omit "static"
;; -z, --zero                -- produce NUL-terminated entries
;; -n, --numeric             -- produce numeric byte values only
;; -e, --essential           -- omit count
;;
;; Write to stdout a C data structure representing a symbol pool
;; with symbols SYMBOL...  For example:
;;
;;   $ generate-C-symbol-set \
;;     Author Date Id
;;   static const uint8_t symbolpool[16] =
;;   {
;;     3 /* count */,
;;     6,'A','u','t','h','o','r',
;;     4,'D','a','t','e',
;;     2,'I','d'
;;   };
;;
;; A more complicated example, using all the switches:
;;
;;   $ generate-C-symbol-set -b BYTE -p keywords -g -z -n -e \
;;     Author Date Id
;;   const BYTE keywords[18] =
;;   {
;;     /* Author */ 6,65,117,116,104,111,114,0,
;;     /* Date */ 4,68,97,116,101,0,
;;     /* Id */ 2,73,100,0
;;   };
;;
;; Note that with ‘--zero’, the pool (sub)strings are C-compatible.

;;; Code:

(define-module (ttn-do generate-C-symbol-set)
  #:export (generate-C-symbol-set
            main)
  #:use-module ((srfi srfi-13) #:select (string-join))
  #:use-module ((ttn-do zzz banalities) #:select (check-hv
                                                  decry
                                                  qop<-args))
  #:use-module ((ttn-do zzz personally) #:select (fs))
  #:use-module ((ttn-do zzz publishing) #:select (flatten-to :LF)))

(define (alloc name)
  (let ((len (string-length name)))
    (or (< 0 len 256)
        (decry "uncool symbol: ~A" name))
    len))

;; Return a string tree representing the C code for the symbol set
;; @var{ls} (a list of strings), customized by @code{configuration}
;; (an alist).  Recognized @code{configuration} keys (symbols) are:
;; @code{zero}, @code{global}, @code{byte-type} and @code{pool-name}.
;; Values are boolean for @code{zero} and @code{global}, and string
;; for the others.
;;
;; You can use @code{flatten} (@pxref{zzz publishing}), to render
;; this tree.  (This is what the shell command does.)
;;
(define (generate-C-symbol-set configuration ls)
  (define (cfg x)
    (assq-ref configuration x))
  (let ((n (length ls))
        (len (map alloc ls))
        (essential (cfg 'essential))
        (zero (cfg 'zero))
        (numeric (cfg 'numeric)))
    (list
     (fs "~Aconst ~A ~A[~A] = ~%"
         (if (cfg 'global) "" "static ")
         (cfg 'byte-type)
         (cfg 'pool-name)
         (apply + (if essential 0 1)
                (* n (if zero 2 1)) len))
     "{" :LF
     "  " (if essential
              ""
              (fs "~A /* count */,~%  " n))
     (string-join
      (map (lambda (len name)
             (fs "~A~A,~A~A"
                 (if numeric
                     (fs "/* ~A */ " name)
                     "")
                 len
                 (string-join
                  (map (lambda (c)
                         (if (and (not numeric)
                                  (or (char-numeric? c)
                                      (char-alphabetic? c)
                                      (eq? #\- c)))
                             (fs "'~A'" c)
                             (number->string (char->integer c))))
                       (string->list name))
                  ",")
                 (if zero
                     (fs ",~A" (if numeric
                                   0
                                   "'\\0'"))
                     "")))
           len ls)
      (fs ",~%  "))
     :LF
     "};"
     :LF)))

(define (main/qop qop)
  (flatten-to (current-output-port)
              (generate-C-symbol-set
               `((pool-name . ,(or (qop 'pool-name) "symbolpool"))
                 (byte-type . ,(or (qop 'byte-type) "uint8_t"))
                 (global    . ,(qop 'global))
                 (essential . ,(qop 'essential))
                 (numeric   . ,(qop 'numeric))
                 (zero      . ,(qop 'zero)))
               (qop '()))))

(define (main args)
  (check-hv args '((package . "ttn-do")
                   (version . "1.2")
                   ;; 1.2 -- add ‘--numeric’, ‘--essential’
                   ;; 1.1 -- add ‘--global’, ‘--zero’
                   ;; 1.0 -- initial release
                   (help . commentary)))
  (main/qop
   (qop<-args
    args '((pool-name (single-char #\p) (value #t))
           (byte-type (single-char #\b) (value #t))
           (essential (single-char #\e))
           (numeric (single-char #\n))
           (global (single-char #\g))
           (zero (single-char #\z))))))

;;; generate-C-symbol-set ends here
