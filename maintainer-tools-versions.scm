#!/bin/sh
exec ${GUILE-guile} -e '(ttn-do maintainer-tools-versions)' -s $0 "$@" # -*- scheme -*-
!#
;;; maintainer-tools-versions

;; Copyright (C) 2008, 2011, 2019, 2020 Thien-Thi Nguyen
;;
;; This file is part of ttn-do, released under the terms of the
;; GNU General Public License as published by the Free Software
;; Foundation; either version 3, or (at your option) any later
;; version.  There is NO WARRANTY.  See file COPYING for details.

;;; Commentary:

;; Usage: maintainer-tools-versions [ADDITIONAL-PROGRAMS...]
;;
;; Call autoconf, automake, libtool and guile and any specified
;; ADDITIONAL-PROGRAMS with single arg ‘--version’ and collect the
;; first line (respectively) output.
;;
;; Display each line with a "# " (hash space) prefix.

;;; Code:

(define-module (ttn-do maintainer-tools-versions)
  #:export (main)
  #:use-module ((ttn-do zzz banalities) #:select (check-hv))
  #:use-module ((ice-9 popen) #:select (open-input-pipe
                                        close-pipe))
  #:use-module ((ice-9 rdelim) #:select (read-line))
  #:use-module ((ttn-do zzz personally) #:select (FE fs fso)))

(cond-expand
 (guile-2
  (use-modules
   (ice-9 curried-definitions)))
 (else #f))

(define ((display-tool-version prefix) program)
  (let ((p (open-input-pipe (fs "~A --version" program))))
    (display prefix)
    (display (read-line p 'concat))
    (close-pipe p)))

(define (main args)
  (check-hv args `((package . "ttn-do")
                   (version . "1.1")
                   ;; 1.1  -- Guile 2 slog
                   ;; 1.0  -- initial release
                   (help . commentary)))
  (FE `("autoconf"
        "automake"
        "libtool"
        "guile"
        ,@(cdr args))
      (display-tool-version "# ")))

;;; maintainer-tools-versions ends here
