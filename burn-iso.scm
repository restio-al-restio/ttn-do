#!/bin/sh
exec ${GUILE-guile} -e '(ttn-do burn-iso)' -s $0 "$@" # -*-scheme-*-
!#
;;; burn-iso

;; Copyright (C) 2004, 2006, 2007, 2008, 2011, 2013, 2021 Thien-Thi Nguyen
;;
;; This file is part of ttn-do, released under the terms of the
;; GNU General Public License as published by the Free Software
;; Foundation; either version 3, or (at your option) any later
;; version.  There is NO WARRANTY.  See file COPYING for details.

;;; Commentary:

;; Usage: burn-iso [options] ISO
;;
;; Options:
;;  -d, --device DEVICE    -- use DEVICE (default: "/dev/hdc")
;;  -e, --eject            -- eject when done
;;  -s, --speed N          -- quickness multiplier
;;
;; DEVICE is a device filename (default: "/dev/hdc").
;; Look at "wodim --devices" output if unsure.
;; SPEED is a number 4 and up (48 seems to work ok).
;; Omitting SPEED normally means "as fast as possible".
;;
;; This program uses wodim(1) to do the actual work.

;;; Code:

(define-module (ttn-do burn-iso)
  #:export (main)
  #:use-module ((ttn-do zzz banalities) #:select (check-hv
                                                  decry
                                                  qop<-args))
  #:use-module ((ttn-do zzz personally) #:select (fs fso)))

(define (burn-iso/qop qop)
  (let* ((iso (if (null? (qop '()))
                  (decry "missing argument")
                  (car (qop '()))))
         (device (or (qop 'device)
                     "/dev/hdc"))
         (speed-opt (or (qop 'speed (lambda (s)
                                      (fs " speed=~A" s)))
                        ""))
         (command (fs "wodim -force~A dev=~A~A -tao -data ~A"
                      (if (qop 'eject)
                          " -eject"
                          "")
                      device speed-opt iso)))
    (fso "+ ~A~%" command)
    (zero? (system command))))

(define (main args)
  (check-hv args '((package . "ttn-do")
                   (version . "1.6")
                   ;; 1.6  -- add ‘--eject’
                   ;; 1.5  -- support long opts (rewritten in scheme)
                   ;; 1.4  -- handle --help, --version
                   ;; 1.3  -- use ‘wodim -eject’
                   ;; 1.2  -- use eject(1)
                   ;; 1.1  -- use wodim(1)
                   ;; 1.0  -- initial release
                   (help . commentary)))
  (burn-iso/qop
   (qop<-args args '((device (single-char #\d) (value #t))
                     (eject (single-char #\e))
                     (speed (single-char #\s) (value #t))))))

;;; burn-iso ends here
