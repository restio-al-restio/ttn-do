#!/bin/sh
exec ${GUILE-guile} -e '(ttn-do mogrify)' -s $0 "$@" # -*- scheme -*-
!#
;;; mogrify --- programmable text editor

;; Copyright (C) 2002-2005, 2007-2013, 2017, 2020, 2021 Thien-Thi Nguyen
;;
;; This file is part of ttn-do, released under the terms of the
;; GNU General Public License as published by the Free Software
;; Foundation; either version 3, or (at your option) any later
;; version.  There is NO WARRANTY.  See file COPYING for details.

;;; Commentary:

;; Usage: mogrify
;;
;; Do nothing and exit successfully.

;;; Code:

(define-module (ttn-do mogrify)
  #:export-syntax (editing-buffer)
  #:re-export (filename:)
  #:export (main editing-buffer-manager
                 find-file-read-only
                 find-file)
  #:autoload (ttn-do zzz banalities) (check-hv)
  #:use-module (ttn-do zzz gap-buffer))

;; e:

(define current-buffer-procs '())
(define cb-count 0)
(define *cbprocs* '())

(define (register-cbproc! name proc)
  (set! current-buffer-procs (acons name cb-count current-buffer-procs))
  (set! cb-count (1+ cb-count))
  (set! *cbprocs* (cons proc *cbprocs*)))

(define e:ify (symbol-prefix-proc 'e:))

(define-macro (simple-gb->e: . names)
  `(begin
     ,@(append (map (lambda (name)
                      `(begin
                         (define ,(e:ify name) ,(symbol-append 'gb- name))
                         (register-cbproc! ',name ,(e:ify name))))
                    `(,@names)))))

(define-macro (tricky-gb->e: . names)
  `(begin
     ,@(append (map (lambda (gb/name)
                      (let ((name (cdr gb/name)))
                        `(begin
                           (define ,(e:ify name) ,(car gb/name))
                           (register-cbproc! ',name ,(e:ify name)))))
                   `(,@names)))))

;; meta

(simple-gb->e: toggle-read-only)

;; query -- see also search section below

(simple-gb->e: point
               point-min
               point-max
               char-after
               char-before
               bolp eolp
               bobp eobp)

(tricky-gb->e: (gb->string . buffer-string)
               (gb->substring . buffer-substring)
               (gb->lines . buffer-lines)
               (make-gap-buffer-port . buffer-port))

;; traverse

(simple-gb->e: goto-char
               forward-char
               backward-char
               forward-line
               beginning-of-line
               end-of-line)

;; search and replace

(tricky-gb->e: (md . match-data)
               (md! . set-match-data))

(simple-gb->e: match-string
               looking-at
               match-beginning
               match-end
               search-forward
               search-backward
               re-search-forward
               replace-match)

;; common backend for {flush,keep}-lines

(define (fk-do! keep? gb regexp)
  (check-read-only gb)
  (let* ((rx (make-regexp regexp regexp/newline))
         (s (s: gb))
         (len (string-length s))
         ;; Restore point here afterwards (poor man's ‘save-excursion’).
         (orig (gb-point gb)))

    (define (prev-bol beg ofs)
      (cond ((string-rindex s #\newline beg ofs) => 1+)
            (else beg)))

    (define (next-bol ofs)
      (cond ((string-index s #\newline ofs) => 1+)
            (else len)))

    ;; Look for each regexp, taking note if the initial position is not
    ;; bolp.  If ‘keep?’, stash the match line (with fixups); otherwise,
    ;; stash the lines between the start of the search and match line
    ;; (also with fixups).
    ;;
    ;; FOO-bol+1 stands for the beginning of the next line after FOO,
    ;; one of: ‘ss’ (search start), ‘m’ (match).

    (let loop ((aft (aft-ofs: gb)) (flags (if (gb-bolp gb)
                                              0
                                              regexp/notbol)))
      (cond

       ((regexp-exec rx s aft flags)
        => (lambda (m)
             (let* ((ss-bol+1 (and
                               ;; The fixup is only required for
                               ;; initial position not bolp, so
                               ;; encode "not required" here,
                               ;; once, by gating w/ ‘flags’.
                               (positive? flags)
                               (next-bol aft)))
                    (b (match:start m))
                    (bol (prev-bol aft b))
                    (m-bol+1 (next-bol (match:end m)))
                    (gap (gap-ofs: gb)))

               (define (stash! beg end)
                 (substring-move! s beg end s gap)
                 (set! gap (+ gap (- end beg))))

               (cond

                (keep?
                 ;; Fixup: Preserve the initial line
                 ;; even if it does not match.
                 (and ss-bol+1
                      (> b ss-bol+1)
                      (stash! aft ss-bol+1))
                 ;; Save the matched lines.
                 (stash! bol m-bol+1))

                (else
                 ;; Fixup: Zonk the entire initial line
                 ;; if the match occurs in part after point.
                 ;; Adjust orig point accordingly.
                 (and ss-bol+1
                      (< b ss-bol+1)
                      (begin
                        (set! gap (prev-bol 0 gap))
                        (set! orig (1+ gap))))
                 ;; Save the lines prior to match.
                 (stash! aft bol)))

               ;; Update the gap and loop.
               (gap-ofs! gb gap)
               ;; Subsequent searches start at bol (0 for ‘flags’).
               (loop m-bol+1 0))))

       (else
        ;; No more matches.
        (aft-ofs! gb (if keep?
                         ;; Ignore the rest.
                         len
                         ;; Keep the rest.
                         aft)))))

    (gb-goto-char gb orig)))

(define (e:flush-lines gb rx)
  (fk-do! #f gb rx))

(define (e:keep-lines gb rx)
  (fk-do! #t gb rx))

(register-cbproc! 'flush-lines e:flush-lines)
(register-cbproc! 'keep-lines e:keep-lines)

;; munge

(simple-gb->e: insert-file-contents)
(simple-gb->e: insert)

(tricky-gb->e: (gb-delete-char! . delete-char)
               (gb-erase! . erase-buffer))

(simple-gb->e: delete-region)

;; output

(tricky-gb->e: (gb->port! . write-to-port))

(define (e:save-buffer gb)
  (call-with-output-file (or (filename: gb)
                             (error "Buffer not visiting a file"))
    (lambda (port)
      (gb->port! gb port))))

(register-cbproc! 'save-buffer e:save-buffer)

;; close e:

(set! *cbprocs* (list->vector (reverse! *cbprocs*)))

;; dispatch

(define cbexp-specials '())             ; (TAG . (LAMBDA (BUF FORM)))

(define (define-cbexp-special tag expander)
  (set! cbexp-specials (acons tag expander cbexp-specials)))

(define-cbexp-special 'let
  (lambda (box form)
    (let ((expand-bindings
           (lambda (bindings-form)
             (map (lambda (binding)
                    `(,(car binding)
                      ,@(cbproc-app-expand box (cdr binding))))
                  bindings-form))))
      (if (symbol? (cadr form))         ; named let
          `(,(car form) ,(cadr form)
            ,(expand-bindings (caddr form))
            ,@(cbproc-app-expand box (cdddr form)))
          `(,(car form) ,(expand-bindings (cadr form))
            ,@(cbproc-app-expand box (cddr form)))))))

(define-cbexp-special 'let*
  (assq-ref cbexp-specials 'let))

(define-cbexp-special 'letrec
  (assq-ref cbexp-specials 'let))

(define-cbexp-special 'lambda
  (lambda (box form)
    `(lambda ,(cadr form) ,@(cbproc-app-expand box (cddr form)))))

(define-cbexp-special 'define
  (lambda (box form)
    `(define ,(cadr form) ,@(cbproc-app-expand box (cddr form)))))

(define-cbexp-special 'case
  (lambda (box form)
    `(case ,(cbproc-app-expand box (cadr form))
       ,@(map (lambda (case-element)
                `(,(car case-element)
                  ,@(cbproc-app-expand box (cdr case-element))))
              (cddr form)))))

(define-cbexp-special 'current-buffer
  (lambda (box form)
    (car box)))

(define-cbexp-special 'editing-buffer
  (lambda (box form)
    form))

(define (newname<-prefix+idx prefix idx)
  (string->symbol (simple-format #f "~A/~A" prefix idx)))

(define (cbproc-app-expand box form)
  (cond ((pair? form)
         (let ((op (car form))          ; only expand applications
               (handle-rest (lambda ()
                              (cond ((not (list? form))
                                     (cbproc-app-expand box (cdr form)))
                                    ((= 1 (length form))
                                     '())
                                    (else
                                     (map (lambda (elem)
                                            (cbproc-app-expand box elem))
                                          (cdr form)))))))
           (cond ((assq-ref cbexp-specials op)
                  => (lambda (expand) (expand box form)))
                 ((assq-ref current-buffer-procs op)
                  => (lambda (idx)
                       (or (assq-ref (cdr box) op)
                           (set-cdr! box (acons op idx (cdr box))))
                       `(,(newname<-prefix+idx (car box) idx)
                         ,(car box) ,@(handle-rest))))
                 (else (cons (cbproc-app-expand box op) (handle-rest))))))
        (else form)))

;; Consider @var{buffer} the current buffer and execute @var{body}.
;; In @var{body}, applications of one of the following procs are
;; handled specially.
;;
;; @example
;; (insert-file-contents filename [visit? [beg [end [replace?]]]])
;; (toggle-read-only [arg])
;; (search-forward string [bound [noerror [repeat]]])
;; (match-end [n])
;; (match-data)
;; (set-match-data m)
;; (buffer-lines)               ; => (STRING ...)
;; (buffer-substring beg end)
;; (search-backward string [bound [noerror [repeat]]])
;; (match-string n)
;; (forward-char n)
;; (backward-char n)
;; (forward-line [n])
;; (beginning-of-line [n])
;; (end-of-line [n])
;; (re-search-forward regexp [bound [noerror [repeat]]])
;; (match-beginning [n])
;; (match-end [n])
;; (delete-region beg end)
;; (buffer-port)
;; (replace-match newtext)
;; (insert [obj...])            ; gb/char/string/symbol/number/sexp
;; (looking-at regexp)
;; (delete-char n)
;; (erase-buffer)
;; (goto-char pos)
;; (buffer-string)
;; (point-min)
;; (point-max)
;; (char-after [pos])
;; (char-before [pos])
;; (point)
;; (bolp)                       ; beginning/end of line/buffer
;; (eolp)
;; (bobp)
;; (eobp)
;; (write-to-port port [beg [end]])
;; (flush-lines regexp)         ; leaves point at eob
;; (keep-lines regexp)          ; likewise
;; (save-buffer)                ; buffer must be "visiting a file"
;;                              ;  else error (see `find-file' et al)
;; @end example
;;
;; These calls are all translated by @code{editing-buffer} to procedures that
;; take gap-buffer @var{buffer} as initial argument (so that the "first" arg
;; above would actually be passed as the second arg, and so on).
;;
;; If @var{buffer} is #t, create and use a new (empty) gap-buffer.
;; If @var{buffer} is not a gap-buffer object and not #t, it is passed to
;; @code{make-gap-buffer} and the result used for editing.
;;
;; The value is that of the last form in @var{body}, or the gap-buffer
;; object if there are no @var{body} forms.
;;
(define-macro (editing-buffer buffer . body)
  (let* ((bufsym (gensym))
         (symbox (list bufsym))
         (bigbod (cbproc-app-expand symbox body)) ; pushes on symbox cdr
         (prdefs (map (lambda (pair)
                        (let ((op (car pair))
                              (idx (cdr pair)))
                          `(,(newname<-prefix+idx bufsym idx)
                            ,(vector-ref *cbprocs* idx))))
                      (cdr symbox)))
         (v `(let* ((,bufsym (let ((init ,buffer))
                               (cond ((,gb? init) init)
                                     ((eq? #t init) (,make-gap-buffer))
                                     (else (,make-gap-buffer init)))))
                    ,@prdefs)
               ,@(if (null? bigbod)
                     (list bufsym)
                     bigbod))))
    ;;(use-modules ((ttn-do pp) #:select (pp)))
    ;;(pp v (current-error-port))
    v))

;; Return a procedure that encapsulates a new editing buffer
;; (or @var{buffer}, if specified).
;; The returned proc takes a (symbolic) command and arguments (if any),
;; dispatching internally.  For example:
;;
;; @example
;; (define MGR (editing-buffer-manager))
;; (MGR 'insert "foo")
;; (MGR 'write-to-port #t)
;;
;; ===
;;
;; (editing-buffer "foo"
;;   (write-to-port #t))
;; @end example
;;
;; In addition to the commands supported by @code{editing-buffer},
;; there are some special commands:
;;
;; @table @asis
;; @item @var{mgr}
;; Return the internal editing buffer.  Here, @var{mgr} is the
;; manager procedure itself (i.e., @code{(mgr mgr)}).
;;
;; @item @code{#f}
;; Forget the internal editing buffer
;; (possibly exposing it to garbage collection).
;; Subsequent invocations will signal a "no more buffer" error.
;;
;; @item @code{#t}
;; Return the internal editing buffer, forgetting it, as well.
;; Subsequent invocations will signal a "no more buffer" error.
;; @end table
;;
;;-args: (- 1 0 buffer)
;;
(define (editing-buffer-manager . opts)
  (let ((buf (if (pair? opts)
                 (car opts)
                 (editing-buffer ""))))

    (define (mgr command . args)
      (cond ((eq? mgr command)
             buf)
            ((not command)
             (set! buf #f))
            ((not buf)
             (error "no more buffer"))
            ((eq? #t command)
             (let ((rv buf))
               (set! buf #f)
               rv))
            ((assq-ref current-buffer-procs command)
             => (lambda (i)
                  (apply (vector-ref *cbprocs* i) buf args)))
            (else
             (error "bad command:" command))))

    ;; rv
    mgr))


;;; Convenience procedures for file access.

;; Find @var{filename} into a buffer and make it read-only.
;; This causes the buffer to visit the file.
;; Move point to beginning of buffer and return the buffer.
;;
(define (find-file-read-only filename)
  (editing-buffer #t
    (insert-file-contents filename #t)
    (toggle-read-only 1)
    (current-buffer)))

;; Find @var{filename} into a buffer.
;; This causes the buffer to visit the file.
;; If the file is not readable, return #f.
;; If the file is read-only, make the buffer read-only as well.
;; Move point to beginning of buffer and return the buffer.
;;
(define (find-file filename)
  (and (access? filename R_OK)
       (editing-buffer (find-file-read-only filename)
         (and (access? filename W_OK)
              (toggle-read-only -1))
         (current-buffer))))


;;; Usage from the shell

(define (main args)
  (check-hv args '((package . "ttn-do")
                   (version . "1.5")
                   ;; 1.5  -- use (ttn-do zzz gap-buffer)
                   ;; 1.4  -- bugfix: use ‘drain-port’ to avoid EOF
                   ;; 1.3  -- slog for Guile 2
                   ;; 1.2  -- bugfix: handle all ‘delete-region’ cases
                   ;; 1.1  -- fix {flush,keep}-lines
                   ;; 1.0  -- snarfed from Guile 1.4.x (ice-9 editing-buffer)
                   (help . commentary)))
  (exit #t))

;;; mogrify ends here
