#!/bin/sh
exec ${GUILE-guile} -e '(ttn-do ls-tarballs)' -s $0 "$@" # -*- scheme -*-
!#
;; Copyright (C) 2011, 2012, 2017 Thien-Thi Nguyen
;;
;; This file is part of ttn-do, released under the terms of the
;; GNU General Public License as published by the Free Software
;; Foundation; either version 3, or (at your option) any later
;; version.  There is NO WARRANTY.  See file COPYING for details.

;;; Commentary:

;; Usage: ls-tarballs [DIR]
;;
;; List the compressed archives in DIR (or "." if unspecified),
;; grouping by STEM and summarizing version numbers.
;; The output format is:
;;
;;   YYYY-MM-DD STEM     COUNT -- RELEASES
;;
;; where YYYY-MM-DD is the mtime of the latest STEM-*.FTYPE,
;; FTYPE is either "tar.gz", "tar.lz" or "tar.xz",
;; COUNT is count of the tarballs with the same STEM, and
;; RELEASES is a compacted comma-separated list of version numbers
;; (the part of the filename besides STEM- and ".FTYPE").
;;
;; Version numbers are normalized to a list of integers, so that
;; "edb-1.26p1.tar.gz", for example, has internal representation
;; ‘(1 26 1)’, and displayed with dot as separator, e.g., "1.26.1".
;;
;; The space between STEM and COUNT is adjusted for columnar output.

;;; Code:

(define-module (ttn-do ls-tarballs)
  #:export (main)
  #:use-module ((ttn-do zzz banalities) #:select (check-hv))
  #:use-module ((ice-9 regex) #:select (string-match
                                        match:prefix
                                        match:start))
  #:use-module ((ice-9 format) #:select (format))
  #:use-module ((srfi srfi-1) #:select (partition
                                        take
                                        drop))
  #:use-module ((srfi srfi-2) #:select (and-let*))
  #:use-module ((srfi srfi-11) #:select (let-values))
  #:use-module ((srfi srfi-13) #:select (string-tokenize
                                         string-join))
  #:use-module ((srfi srfi-14) #:select (char-set:digit))
  #:use-module ((ttn-do zzz filesystem) #:select (filtered-files-in-vicinity
                                                  extract-stem-proc
                                                  with-cwd)))

(define (fs s . args)
  (apply format #f s args))

(define mtime (let ((m (make-hash-table)))
                (make-procedure-with-setter
                 (lambda (k) (hash-ref m k #f))
                 (lambda (k v) (hash-set! m k v)))))

(define releases (let ((ht (make-hash-table)))
                   (make-procedure-with-setter
                    (lambda (k) (hash-ref ht k '()))
                    (lambda (k v) (hash-set! ht k v)))))

(define (split vers)
  (map string->number (string-tokenize vers char-set:digit)))

(define (packages/last-modified)
  (filtered-files-in-vicinity
   "." (let ((tgz (extract-stem-proc "tar.gz"))
             (tlz (extract-stem-proc "tar.lz"))
             (txz (extract-stem-proc "tar.xz")))
         (lambda (filename)
           (and-let* ((part (or (tgz filename)
                                (tlz filename)
                                (txz filename)))
                      (tail (string-match "-[0-9]" part))
                      (no-v (match:prefix tail))
                      (vers (substring part (1+ (match:start tail 0))))
                      (this (stat:mtime (stat filename))))
             ;; Avoid duplicates (e.g., w/ same-release .tar.gz, .tar.lz).
             ;; TODO: Look for "pushnew"; use it.
             (let ((so-far (releases no-v)))
               (or (member vers so-far)
                   (set! (releases no-v)
                         (cons vers so-far))))
             (cond ((mtime no-v)
                    => (lambda (before)
                         (set! (mtime no-v) (max before this))
                         #f))
                   (else
                    (set! (mtime no-v) this)
                    no-v)))))))

(define (compact ls)

  (define (s<- numbers)
    (string-join (map number->string numbers) "."))

  (define (pretty head tail)
    (fs "~{~A.~}~A" head
        (let ((first (caar tail)))
          (if (null? (cdr tail))
              (caar tail)
              (fs "{~{~A~^,~}}"
                  (let loop ((seq (list (cons first first)))
                             (rest (map car (cdr tail))))
                  (if (null? rest)
                      (map (lambda (x)
                             (case (- (car x) (cdr x))
                               ((0) (car x))
                               ((1) (fs "~A,~A" (cdr x) (car x)))
                               (else (fs "~A-~A" (cdr x) (car x)))))
                           (reverse! seq))
                      (let ((n (car rest)))
                        (if (= (1+ (caar seq)) n)
                            (loop (acons n (cdar seq) (cdr seq))
                                  (cdr rest))
                            (loop (acons n n seq)
                                  (cdr rest)))))))))))

  (define (version< a b)
    (cond ((and (null? a) (pair? b))
           #t)
          ((and (null? b) (pair? a))
           #f)
          (else
           (let ((fa (car a))
                 (fb (car b)))
             (if (= fa fb)
                 (version< (cdr a) (cdr b))
                 (< fa fb))))))

  (define (same-prefix ls len)
    (cond ((take (car ls) len)
           => (lambda (seq)
                (lambda (x)
                  (and (= len (1- (length x)))
                       (equal? seq (take x len))))))
          (else
           (lambda (x) #f))))

  (define (group ls)
    (let loop ((len (if (pair? ls)
                        (1- (length (car ls)))
                        0)))
      (let-values (((same diff) (partition (same-prefix ls len) ls)))
        (if (null? same)
            (loop (1- len))
            (cons (pretty (take (car same) len)
                          (map (lambda (x)
                                 (drop x len))
                               same))
                  (if (null? diff)
                      diff
                      (group diff)))))))

  (group (sort ls version<)))

(define (show-status)
  (format #t "~:{~A ~20A ~2D -- ~{~A~^, ~}~%~}"
          (map (lambda (pkg)
                 (list (strftime "%Y-%m-%d" (localtime (mtime pkg)))
                       pkg
                       (length (releases pkg))
                       (compact (map split (releases pkg)))))
               (sort (packages/last-modified)
                     (lambda (a b)
                       (> (mtime a)
                          (mtime b)))))))

(define (main args)
  (check-hv args '((package . "ttn-do")
                   (version . "1.2")
                   ;; 1.2  -- handle .tar.lz
                   ;; 1.1  -- handle .tar.xz
                   ;; 1.0  -- initial release
                   (help . commentary)))
  (with-cwd (if (null? (cdr args))
                "."
                (cadr args))
    (show-status)))

;;; ls-tarballs ends here
