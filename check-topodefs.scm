#!/bin/sh
exec ${GUILE-guile} -e '(ttn-do check-topodefs)' -s $0 "$@" # -*- scheme -*-
!#
;;; check-topodefs --- flag callsites appearing before defining form

;; Copyright (C) 2007, 2009-2011, 2013, 2020 Thien-Thi Nguyen
;;
;; This file is part of ttn-do, released under the terms of the
;; GNU General Public License as published by the Free Software
;; Foundation; either version 3, or (at your option) any later
;; version.  There is NO WARRANTY.  See file COPYING for details.

;;; Commentary:

;; Usage: check-topodefs [options] FILE...
;;
;; Scan each FILE for definitions and check that "usages" of those
;; definitions (i.e., simple references to the name of the definition)
;; do not occur in forms prior the definition.  When this is not the
;; case, display to stderr:
;;
;; FILENAME:LINE:: (USAGE-TOP-LEVEL) LATER
;;
;; where USAGE-TOP-LEVEL is the name of the form (if deducible) where
;; the definition of LATER is used, and FILENAME:LINE is its location.
;; Options are:
;;
;;   -I, --ignore NAME  -- In addition to ‘define-module’ and
;;                         ‘use-modules’, ignore NAME, as well.
;;
;;   -D, --defkey NAME  -- Consider NAME a definition form, as well.
;;                         The list of standard definition forms are:
;;                           define     define-public,
;;                           define*    define*-public,
;;                           defmacro   defmacro-public,
;;                           defmacro*  defmacro*-public
;;                           define-macro
;;                           define-record-type (SRFI 9)
;;
;;   -v, --verbose      -- Display progress and scan information.
;;                         This includes filename, definition names,
;;                         and "distance" information.
;;
;;   -a, --average      -- Show AVERAGE instead of CLOSEST (see below).
;;
;; Note that both ‘-I’ and ‘-D’ add to their respective lists; there is
;; presently no way to specify that an item be removed.  Also, both ‘-I’
;; and ‘-D’ can be given multiply; their args accumulate.
;;
;; The "distance" information has the following single-line format:
;;
;;   SERIAL  COUNT  CLOSEST  NAME  DIFFS  USED
;;
;; where SERIAL is the serial number of the NAME form, COUNT is
;; how many of its peers are mentioned, DIFFS are the differences
;; (a list of integers) in serial numbers of the USED (list of peers),
;; and CLOSEST is the first element of DIFFS.  When any element of
;; DIFFS is negative, display the topological violation to stderr
;; (described above).
;;
;; If option ‘--average’ is given, instead of CLOSEST, display
;; AVERAGE, an average of all the DIFFS.  This is a figure of
;; merit -- the lower (but not negative!) the better.

;;; Code:

(define-module (ttn-do check-topodefs)
  #:export (main)
  #:use-module ((ice-9 format) #:select (format))
  #:use-module ((srfi srfi-1) #:select (append-map
                                        lset-difference))
  #:use-module ((ttn-do zzz banalities) #:select (check-hv
                                                  qop<-args))
  #:use-module ((ttn-do zzz personally) #:select (FE HFE fso))
  #:use-module ((scripts read-scheme-source)
                #:select (read-scheme-source-silently
                          quoted?)))

(define *standard-ignored* '(define-module use-modules))
(define *standard-def-head* '(define define-public
                               define* define*-public
                               defmacro defmacro-public
                               defmacro* defmacro*-public
                               define-macro
                               define-record-type))

(define (first-symbol-in-form form)
  (cond ((pair? form) (first-symbol-in-form (car form)))
        ((symbol? form) form)
        (else #f)))

(define (check-proc verbose? average? ignore defkeys)

  (define (def? x)
    (memq x defkeys))

  (lambda (filename)
    (let ((serial (make-object-property))
          (children (make-object-property))
          (counter 0)
          (results (read-scheme-source-silently filename))
          (orig #f)
          (new-names '()))

      (define (nn! nn)
        (define (one! nn)
          (set! (serial nn) counter)
          (set! new-names (cons nn new-names)))
        (if (pair? nn)
            (FE nn one!)
            (one! nn))
        (set! counter (1+ counter)))

      (define (multiple ent)            ; one form, many defs
        (case (car ent)
          ((define-record-type)
           (let* ((parent (cadr ent))
                  (details (cddr ent))
                  (ctor (caar details))
                  (pred (cadr details))
                  (more (append-map (lambda (field-form)
                                      (if (pair? field-form)
                                          (cdr field-form)
                                          '()))
                                    (cddr details)))
                  (all (cons* ctor pred more)))
             ;; Give the record type name its own serial number.
             (nn! parent)
             (set! (children parent) all)
             all))
          (else #f)))

      (FE results (lambda (ent)
                    (and (pair? ent)
                         (def? (car ent))
                         (nn! (or (multiple ent)
                                  (first-symbol-in-form (cadr ent)))))))
      (set! new-names (reverse! new-names))
      (set! orig new-names)
      (and verbose? (format #t "checking: ~A -- ~A defs:~{ ~A~}~%"
                            filename (length new-names) new-names))
      (let loop ((last #f) (ls results))
        (define (next!) (loop (car ls) (cdr ls)))
        (cond ((null? ls))
              ((or (not last)
                   (not (pair? (car ls)))
                   (eq? 'quote (caar ls))
                   (memq (caar ls) ignore))
               (next!))
              (else
               (let* ((u (car ls))
                      (alist (quoted? 'following-form-properties last))
                      (name (or (and=> (assq-ref alist 'signature) car)
                                (first-symbol-in-form (cadr u))))
                      (seen (make-hash-table 97)))

                 (define (walk form)
                   (cond ((not (pair? form))
                          (hashq-set! seen form #t))
                         ((eq? 'quote (car form)))
                         ((eq? 'case (car form))
                          (walk (cadr form))
                          (FE (cddr form)
                              (lambda (claws)
                                (walk (cdr claws)))))
                         (else (walk (car form))
                               (walk (cdr form)))))

                 (and (def? (car u))
                      (set! new-names (delq! name new-names)))
                 ;; Hmm, we assume multidef forms only define, never use.
                 ;; Does this assumption hold beyond ‘define-record-type’?
                 (cond ((children name)
                        => (lambda (kids)
                             (set! new-names (lset-difference
                                              eq? new-names kids))))
                       (else
                        (walk u)))
                 (let ((hit '())
                       (bef '())
                       (serial-name (serial name)))
                   (HFE (k v seen)
                     (and (not (eq? k name))
                          (def? (car u))
                          (memq k orig)
                          (let ((diff (- serial-name (serial k))))
                            (set! bef (cons (list diff k) bef))))
                     (and (memq k new-names)
                          (set! hit (cons k hit))))
                   (set! bef (sort bef (lambda (a b)
                                         (< (car a) (car b)))))
                   (and verbose?
                        (not (null? bef))
                        (fso "~A\t~A\t~A\t~A ~S ~S~%"
                             serial-name (length bef)
                             (if average?
                                 (let ((all (map car bef)))
                                   (/ (inexact->exact
                                       (truncate
                                        (/ (* 10.0 (apply + all))
                                           (length all))))
                                      10.0))
                                 (caar bef))
                             name (map car bef) (map cadr bef)))
                   (or (null? hit)
                       (memq name ignore)
                       (let ((line (or (assq-ref alist 'line) 0)))
                         (format (current-error-port)
                                 "~A:~A:: (~A)~{ ~S~}~%"
                                 filename line name (reverse! hit))))))
               (loop #f (cdr ls))))))))

(define (main/qop qop)

  (define (symbolic opt base)
    (append (cond ((qop opt) => (lambda (ls)
                                  (map string->symbol ls)))
                  (else '()))
            base))

  (FE (qop '()) (check-proc (qop 'verbose) (qop 'average)
                            (symbolic 'ignore *standard-ignored*)
                            (symbolic 'defkey *standard-def-head*))))

(define (main args)
  (check-hv args '((package . "ttn-do")
                   (version . "1.2")
                   ;; 1.2  -- several bugfixes
                   ;;         handle ‘define-record-type’
                   ;;         new option ‘--average’
                   ;; 1.1  --
                   ;; 1.0  -- initial release
                   (help . commentary)))
  (main/qop
   (qop<-args
    args
    '((ignore (single-char #\I) (value #t) (merge-multiple? #t))
      (defkey (single-char #\D) (value #t) (merge-multiple? #t))
      (average (single-char #\a))
      (verbose (single-char #\v))))))

;;; check-topodefs ends here
